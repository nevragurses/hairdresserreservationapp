import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesLogin.dart';
import 'package:hairdresser_booking_flutter/admin/ConfirmedHairdressers.dart';
import 'package:hairdresser_booking_flutter/admin/HairdresserRequests.dart';
import 'package:hairdresser_booking_flutter/admin/RejectedHairdressers.dart';
import 'package:hairdresser_booking_flutter/hairdresser/ConfirmedReservations.dart';
import 'package:hairdresser_booking_flutter/hairdresser/reservationRequests.dart';

import 'RejectedReservations.dart';
class  HairdresserPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState()  => _HairdresserState();
}
class _HairdresserState extends State<HairdresserPage> {
  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        Navigator.push(context, MaterialPageRoute(builder: (context) => TabPagesLogin_Second()));

    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Kuaför Ana Sayfa"),
        centerTitle: true,
        actions: [
          Theme(
            data: Theme.of(context).copyWith(
              dividerColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.white),
              textTheme: TextTheme().apply(bodyColor: Colors.white),
            ),
            child: PopupMenuButton<int>(
              color: Colors.indigo,
              onSelected: (item) => onSelected(context, item),
              itemBuilder: (context) => [
                PopupMenuDivider(),
                PopupMenuItem<int>(
                  value: 0,
                  child: Row(
                    children: [
                      Icon(Icons.logout),
                      const SizedBox(width: 8),
                      Text('Çıkış'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          child: Container(
            margin:  const EdgeInsets.only(top: 180),
            child: GridView.count(
              primary: false,
              padding: const EdgeInsets.all(30),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 2,
              children: <Widget>[
                SingleChildScrollView(
                  child: Container(
                      color:  Color(0xFFC5D8FF),
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                            child: Container(
                                child: GestureDetector(
                                  onTap: () =>
                                  {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => HairdresserRequestPage()))
                                  },
                                  child: new Text(
                                    'RANDEVU TALEPLERİ',
                                    style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      color: Colors.redAccent,
                                    ),
                                  ),
                                )
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                              child: Container(
                                child: GestureDetector(
                                    onTap: () =>
                                    {
                                      new CircularProgressIndicator(),
                                      Navigator.push(context, MaterialPageRoute(
                                          builder: (context) => ReservationRequestPage()))
                                    },
                                    child: new  Image.asset(('assets/waiting.png'),height: 100,width: 100,
                                    )
                                ),
                              )
                          ),
                        ],
                      )
                  ),
                ),
                SingleChildScrollView(
                  child: Container(
                      color:  Color(0xFFC5D8FF),
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                            child: Container(
                                child: GestureDetector(
                                  onTap: () =>
                                  {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => ConfirmedReservationsPage())),
                                  },
                                  child: new Text(
                                    'ONAYLANAN RANDEVULAR',
                                    style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      color: Colors.redAccent,
                                    ),
                                  ),
                                )
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                            child: Container(
                                child: GestureDetector(
                                  onTap: () =>
                                  {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => ConfirmedReservationsPage())),

                                  },
                                  child: new  Image.asset(('assets/confirm.png'),height: 100,width: 100,
                                  ),
                                )
                            ),
                          ),
                        ],
                      )
                  ),
                ),
                SingleChildScrollView(
                  child: Container(
                      color:  Color(0xFFC5D8FF),
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                            child: Container(
                                child: GestureDetector(
                                  onTap: () =>
                                  {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => RejectedReservationsPage()))
                                  },
                                  child: new Text(
                                    'REDDEDİLEN RANDEVULAR',
                                    style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      color: Colors.redAccent,
                                    ),
                                  ),
                                )
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                              child: Container(
                                child: GestureDetector(
                                  onTap: () =>
                                  {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => RejectedReservationsPage()))
                                  },
                                  child: new Image.asset(('assets/reject.png'),height: 115,width: 115,
                                  ),
                                ),
                              )
                          ),
                        ],
                      )
                  ),
                ),

              ],

            ),
          ),
        ),
      ),
    );

  }
}