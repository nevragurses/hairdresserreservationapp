import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/admin/ConfirmedHairdressers.dart';
import 'package:hairdresser_booking_flutter/hairdresser/ConfirmedReservations.dart';
import 'package:hairdresser_booking_flutter/model/ReservationModel.dart';

import 'RejectedReservations.dart';
class ReservationRequests extends StatefulWidget {
  @override
  _ReservationRequestsState createState() => _ReservationRequestsState();
}
class _ReservationRequestsState extends State<ReservationRequests> {
  List<Card> cardList = new List();
  bool setStateRan=false;
  _confirm(String id,String day, String time) async {
    final firestoreInstance = FirebaseFirestore.instance;
    await firestoreInstance.collection("Reservations").get().then((querySnapshot) {
      querySnapshot.docs.forEach((value) {
        if (value.get("hairdresser_id").toString()== FirebaseAuth.instance.currentUser.uid
            && value.get("isAccepted").toString() == "waiting"
           && value.get("day") == day && value.get("time") == time )
         {
           final Map<String, String> someMap = {
             "isAccepted": "true",
           };
           value.reference.update(someMap);
           setState(() {});
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Fluttertoast.showToast(
        msg: "Kayıt Onaylandı!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => ConfirmedReservationsPage()));
  }
  _reject(String id , String day, String time) async {
    final firestoreInstance = FirebaseFirestore.instance;
    await firestoreInstance.collection("Reservations").get().then((querySnapshot) {
      querySnapshot.docs.forEach((value) {
        if (value.get("hairdresser_id").toString()== FirebaseAuth.instance.currentUser.uid
            && value.get("isAccepted").toString() == "waiting"
            && value.get("day") == day && value.get("time") == time )
        {
          final Map<String, String> someMap = {
            "isAccepted": "false",
          };
          value.reference.update(someMap);
          setState(() {});
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Fluttertoast.showToast(
        msg: "Kayıt Reddedildi!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => RejectedReservationsPage()));
  }

  Card newsCard(ReservationModel reservationModel){
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
        //borderRadius: BorderRadius.circular(35.0),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),

        title: Text("Gün " +reservationModel.day.toString()),
        subtitle: Text("Saat: " + reservationModel.time.toString() +
            "\n" + "Hizmetler " +  reservationModel.service.toString()

        ),
        isThreeLine: true,
        trailing: Wrap(
          spacing: 12, // space between two icons
          children: <Widget>[
            new IconButton(
              icon: new Icon(Icons.assignment_turned_in),
              onPressed: (){
                _confirm(reservationModel.hairdresser_id , reservationModel.day, reservationModel.time) ;
              },
              highlightColor: Colors.green,
            ),
            new IconButton(
              icon: new Icon(Icons.clear),
              onPressed: (){
                _reject(reservationModel.hairdresser_id,reservationModel.day, reservationModel.time) ;
              },
              highlightColor: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
  Future<List<ReservationModel>> getNewsList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<ReservationModel> _needs = [];

    await firestoreInstance.collection("Reservations").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("hairdresser_id").toString()== FirebaseAuth.instance.currentUser.uid
            && value.get("isAccepted").toString() == "waiting") {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(ReservationModel.fromMap(values));
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });

    return await _needs;
  }

  createCardList()async{
    List<ReservationModel> newsList =await getNewsList();
    for(int i=0;i< newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
    }
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
class ReservationRequestPage extends StatelessWidget { //BUNU ÇAĞIR
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Randevu Talepleri"),
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 200),
                child:ReservationRequests()
            ),
          ),
        ),
      ),
    );
  }
}