class ServiceModel{
  String name;
  String price;

  ServiceModel(
      this.name,
      this.price,
      );

    ServiceModel.fromMap(Map<dynamic, dynamic> data) {
    name = data['name'];
    price = data['price'];
  }
  Map<String, dynamic> toJson() => {
    'name': name,
    'price': price,
  };
}