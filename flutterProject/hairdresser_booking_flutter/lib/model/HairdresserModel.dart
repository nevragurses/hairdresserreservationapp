class HairdresserModel{
  String name;
  String email;
  String phone;
  String companyName;
  String address;
  String hairdresser_id;
  String admin_id;

  HairdresserModel(
      this.name,
      this.email,
      this.phone,
      this.companyName,
      this.address,
      this.hairdresser_id,
      this.admin_id
      );

  HairdresserModel.fromMap(Map<dynamic, dynamic> data) {
    name = data['name'];
    email = data['email'];
    phone= data['phone'];
    companyName = data['companyName'];
    address = data['address'];
    hairdresser_id = data['hairdresser_id'];
    admin_id=data['admin_id'];
  }
  Map<String, dynamic> toJson() => {
    'name': name,
    'email': email,
    'phone': phone,
    'address': address,
  };
}