class UploadModel{
  String admin_id;
  String image_name;

  UploadModel(
      this.admin_id,
      this.image_name,
      );

  UploadModel.fromMap(Map<dynamic, dynamic> data) {
    admin_id = data['admin_id'];
    image_name = data['image_id'];
  }
  Map<String, dynamic> toJson() => {
    'admin_id': admin_id,
    'image_id': image_name,
  };
}