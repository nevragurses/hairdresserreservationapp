class ReservationModel {
  String hairdresser;
  String hairdresser_id;
  String day;
  String time;
  String service;
  String isAccepted;
  String userId;

  ReservationModel(
      this.hairdresser,
      this.hairdresser_id,
      this.day,
      this.time,
      this.service,
      this.isAccepted,
      this.userId
      );

  ReservationModel.fromMap(Map<dynamic, dynamic> data) {
    hairdresser = data['hairdresser'];
    hairdresser_id = data['hairdresser_id'];
    day= data['day'];
    time = data['time'];
    service = data['service'];
    isAccepted= data['isAccepted'];
    userId = data['userId'];

  }
}
