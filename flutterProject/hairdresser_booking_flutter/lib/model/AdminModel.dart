class AdminModel {
  String name;
  String email;
  String phone;
  String companyName;
  String province;
  String district;
  String neighborhood;
  String street;
  String address;
  String admin_id;

  AdminModel(
      this.name,
      this.email,
      this.phone,
      this.companyName,
      this.province,
      this.district,
      this.neighborhood,
      this.street,
      this.address,
      this.admin_id
      );

  AdminModel.fromMap(Map<dynamic, dynamic> data) {
    name = data['name'];
    email = data['email'];
    phone= data['phone'];
    district = data['district'];
    companyName = data['companyName'];
    street = data['street'];
    neighborhood = data['neighborhood'];
    address = data['address'];
    province = data['province'];
    admin_id = data ['admin_id'];
  }
  Map<String, dynamic> toJson() => {
    'name': name,
    'email': email,
    'phone': phone,
    'address': address,
    'province': province,
    'admin_id' :admin_id
  };
}
