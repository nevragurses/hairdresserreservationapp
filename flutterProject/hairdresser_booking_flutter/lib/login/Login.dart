import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesRegister.dart';
import 'package:hairdresser_booking_flutter/admin/AdminPage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/hairdresser/hairdresserPage.dart';
import 'package:hairdresser_booking_flutter/login/Reset_Password.dart';
import 'package:hairdresser_booking_flutter/user/Companies.dart';
import 'package:hairdresser_booking_flutter/user/CompanyList.dart';
class LoginScreen extends StatefulWidget {
  int user;
  LoginScreen(int user){
    this.user=user;
    print("User: $user");
  }
  @override
  _LoginScreenState createState() => _LoginScreenState(user);
}
class _LoginScreenState extends State<LoginScreen> {
  String _email, _password;
  final auth = FirebaseAuth.instance;
  int flag;

  _LoginScreenState(int user){
    print("Constructor: $user");
    this.flag=user;
  }

  _signin(String _email, String _password) async {
      if (_email == null) {
        Fluttertoast.showToast(
            msg: "Lütfen email adresi giriniz.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else if (_password == null) {
        Fluttertoast.showToast(
            msg: "Lütfen şifre giriniz.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else {
        try {
          await auth.signInWithEmailAndPassword(
              email: _email, password: _password);
          if(flag==0) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CompanyListPage()));
          }
          if(flag==1){
            var user = await FirebaseAuth.instance.currentUser;
            var uid = user.uid;
            DocumentReference users = FirebaseFirestore.instance.collection('Hairdressers').doc(uid);
            users.get().then((documentSnapshot) {
                if(documentSnapshot.get("acceptanceCase") =="true"){
                  Fluttertoast.showToast(
                      msg: "Giriş Yapılıyor...",
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 4
                  );
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HairdresserPage()));
                }
                else if(documentSnapshot.get("acceptanceCase") =="waiting"){
                  Fluttertoast.showToast(
                      msg: "Kayıt talebiniz onay sürecindedir...",
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 4
                  );
                }
                else if(documentSnapshot.get("acceptanceCase") =="false"){
                  Fluttertoast.showToast(
                      msg: "Kayıt talebiniz reddedildi...",
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 4
                  );
                }
            }).catchError((onError) {
              print("getCloudFirestoreUsers: ERROR");
              print(onError);
            });
          }
          if(flag==2) {
            var user = await FirebaseAuth.instance.currentUser;
            var uid = user.uid;
            DocumentReference users = FirebaseFirestore.instance.collection('Admins').doc(uid);
            users.get().then((documentSnapshot) {
              if(documentSnapshot.get("acceptanceCase") =="true"){
                Fluttertoast.showToast(
                    msg: "Giriş Yapılıyor...",
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 4
                );
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AdminPage()));
              }
              else if(documentSnapshot.get("acceptanceCase") =="waiting"){
                Fluttertoast.showToast(
                    msg: "Kayıt talebiniz onay sürecindedir...",
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 4
                );
              }
              else if(documentSnapshot.get("acceptanceCase") =="false"){
                Fluttertoast.showToast(
                    msg: "Kayıt talebiniz reddedildi.Giriş yapamazsınız!",
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 4
                );
              }
            }).catchError((onError) {
              print("getCloudFirestoreUsers: ERROR");
              print(onError);
            });
          }
        } on FirebaseAuthException catch (error) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Email adresi geçersiz veya şifre hatalı!"),
          ));
        }
      }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/loginback.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 60),
          child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: 'Email',
                      prefixIcon: Icon(Icons.email),
                  ),

                  onChanged: (value) {
                    setState(() {
                      _email = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(hintText: 'Şifre', prefixIcon: Icon(Icons.lock),),
                  onChanged: (value) {
                    setState(() {
                      _password = value.trim();
                    });
                  },
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: GestureDetector(
                  onTap: () => {
                      Navigator.push(context,MaterialPageRoute(builder: (context) => Reset_Password()))
                    //Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()))
                  },
                  child: Text(
                    "Şifrenizi Mi Unuttunuz?",
                    style: TextStyle(
                        fontSize: 14,
                        color: Color(0xFF6B8BC8)
                    ),
                  ),
                ),
              ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10),
              child: MaterialButton(
                minWidth: 260,
                height: 60,
                onPressed: (){
                    _signin(_email, _password) ;
                  },
                  //Navigator.push(context, MaterialPageRoute(builder: (context)=> SignupPage()));
                color: Color(0xFF6B8BC8),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)
                ),
                child: Text(
                  "Giriş Yap",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 18
                  ),
                ),
              ),
            ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: GestureDetector(
                  onTap: () => {
                    if(flag==0)
                        Navigator.push(context,MaterialPageRoute(builder: (context) => TabPagesRegister()))
                    else if(flag==1)
                      Navigator.push(context,MaterialPageRoute(builder: (context) => TabPagesRegister_Second()))
                    else if(flag==2)
                      Navigator.push(context,MaterialPageRoute(builder: (context) => TabPagesRegister_Third()))

                    //Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()))
                  },
                  child: Text(
                    "Hesabınız Yok Mu? Kayıt Olun",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF6B8BC8)
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      ),
    );
  }
}