import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Reset_Password extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}
class _ResetPasswordState extends State<Reset_Password> {
  String _email;
  final auth = FirebaseAuth.instance;
  _reset(String _email) async {
    if (_email == null) {
      Fluttertoast.showToast(
          msg: "Lütfen email adresi giriniz.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else{
      try {
        await auth.sendPasswordResetEmail(email: _email);
        Fluttertoast.showToast(
            msg: "Şifre sıfırlama linki email adresine gönderildi.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }on FirebaseAuthException catch (error) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Lütfen geçerli bir email adresi giriniz!\n" + error.toString()),
        ));
      }

    }

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/loginback.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,

          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: 'Şifresini sıfırlamak istediğiniz email',
                      prefixIcon: Icon(Icons.email),
                  ),

                  onChanged: (value) {
                    setState(() {
                      _email = value.trim();
                    });
                  },
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10),
                child: MaterialButton(
                  minWidth: 260,
                  height: 60,
                  onPressed: (){
                    _reset(_email) ;
                  },
                  //Navigator.push(context, MaterialPageRoute(builder: (context)=> SignupPage()));
                  color: Color(0xFF6B8BC8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)
                  ),
                  child: Text(
                    "Şifre Sıfırla",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}