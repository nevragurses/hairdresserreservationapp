import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/model/ReservationModel.dart';
class ConfirmedCostumerReservations extends StatefulWidget {
  @override
  _ConfirmedCostumerReservationsState createState() => _ConfirmedCostumerReservationsState();
}
class _ConfirmedCostumerReservationsState extends State<ConfirmedCostumerReservations> {
  List<Card> cardList = new List();
  bool setStateRan=false;
  Card newsCard(ReservationModel reservationModel){
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
        //borderRadius: BorderRadius.circular(35.0),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),

        title: Text("Gün " +reservationModel.day.toString()),
        subtitle: Text("Saat: " + reservationModel.time.toString() +
            "\n" + "Hizmetler " +  reservationModel.service.toString()

        ),
        isThreeLine: true,
      ),
    );
  }
  Future<List<ReservationModel>> getNewsList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<ReservationModel> _needs = [];
    await firestoreInstance.collection("Reservations").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("userId").toString()== FirebaseAuth.instance.currentUser.uid
            && value.get("isAccepted").toString() == "true") {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(ReservationModel.fromMap(values));
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });

    return await _needs;
  }

  createCardList()async{
    List<ReservationModel> newsList =await getNewsList();
    for(int i=0;i< newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
    }
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
class ConfirmedCustomerReservationsPage extends StatelessWidget { //BUNU ÇAĞIR
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Onaylanan Randevular"),
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 200),
                child:ConfirmedCostumerReservations()
            ),
          ),
        ),
      ),
    );
  }
}