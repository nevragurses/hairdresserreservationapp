import 'package:flutter/material.dart';
import 'package:hairdresser_booking_flutter/admin/Service.dart';

class ServiceSelection extends StatefulWidget {
  List<Service> selectedServices;
  List<String> selected;
  ServiceSelection(this.selectedServices){
    this.selectedServices=selectedServices;
    this.selected = new List();
  }
  List<String> getServices(){
    return selected;
  }
  @override
  _ServiceSelectionState createState() => _ServiceSelectionState();
}
class _ServiceSelectionState extends State<ServiceSelection> {
  // ignore: deprecated_member_use
  control(bool service,String name){
    if(service){
      print("hello " + name);
      if(name!=null)
        widget.selected.add(name);
    }
    else{
      print("hello-2 " + name);
      if(name!=null) {
        if (widget.selected.contains(name)) {
          widget.selected.remove(name);
        }
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (ctx, index) {
        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            widget.selectedServices[index].isSelected = !widget.selectedServices[index].isSelected;
            control(widget.selectedServices[index].isSelected,widget.selectedServices[index].movieName);
            setState(() {});
          },
          child: Container(
            color: widget.selectedServices[index].isSelected
                ? Colors.green[100]
                : null,
            child:Row(
              children: <Widget>[
                Checkbox(
                    value: widget.selectedServices[index].isSelected,
                    onChanged: (s) {
                      widget.selectedServices[index].isSelected = !widget.selectedServices[index].isSelected;
                      control(widget.selectedServices[index].isSelected,widget.selectedServices[index].movieName);
                      setState(() {});
                    }),
                Text(widget.selectedServices[index].movieName)
              ],
            ),
          ),
        );
      },
      itemCount: widget.selectedServices.length,
    );
  }
}