import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/pickers/date_picker_widget.dart';
import 'package:hairdresser_booking_flutter/pickers/hairdresser_widget.dart';
import 'package:hairdresser_booking_flutter/pickers/service_widget.dart';
import 'package:hairdresser_booking_flutter/pickers/time_picker_widget.dart';
class DateSelection extends StatefulWidget {
  String userId;
  DateSelection(String user) {
    this.userId = user;
  }
  @override
  _StartPageState createState() => _StartPageState(userId);

}
class _StartPageState extends State<DateSelection> {
  int index = 0;
  String userId;
  String selectedHairdresser;
  String resultServices;
  String selectedDate;
  String selectedTime;
  HaidresserSelectionPicker haidresserSelectionPicker;
  ServiceSelectionPicker serviceSelectionPicker;
  DatePickerWidget datePickerWidget;
  TimePickerWidget timePickerPickerWidget;
  _StartPageState(String user) {
    this.userId = user;
  }
  Future<String> getList() async{
    final firestoreInstance = FirebaseFirestore.instance;
    String hairdresser;
    await firestoreInstance.collection("Hairdressers").get().then((querySnapshot) {
      querySnapshot.docs.forEach((value) {
          if(value.get("name").toString() == selectedHairdresser && value.get("admin_id").toString()==userId){
            hairdresser = value.get("hairdresser_id");
          }
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR" + onError.toString());
      print(onError);
    });
    return hairdresser;
  }

  _sendSelectedDatas() async {
    selectedHairdresser = haidresserSelectionPicker.getHairdresser();
    resultServices= serviceSelectionPicker.getServices();
    selectedDate =  datePickerWidget.getDate();
    selectedTime = timePickerPickerWidget.getSelectedTime();


    if (selectedHairdresser == null) {
      Fluttertoast.showToast(
          msg: "Lütfen kuaför seçiniz!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }

    else if (resultServices == null ) {
      Fluttertoast.showToast(
          msg: "Lütfen hizmet seçiniz! ",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else if (resultServices!= null && resultServices== "null" ) {
      Fluttertoast.showToast(
          msg: "Lütfen hizmet seçiniz! ",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }

    else if (selectedDate == null) {
      Fluttertoast.showToast(
          msg: "Lütfen gün seçiniz! ",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else if (selectedTime == null) {
      Fluttertoast.showToast(
          msg: "Lütfen saat seçiniz! ",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else {
        String id = await getList();
        final Map<String, String> serviceMap = {
          "hairdresser": selectedHairdresser,
          "service": resultServices,
          "day": selectedDate,
          "time": selectedTime,
          "company": userId,
          "hairdresser_id": id,
          "isAccepted": "waiting",
          "userId" : FirebaseAuth.instance.currentUser.uid.toString()
        };
        FirebaseFirestore.instance.collection('Reservations').doc()
            .set(serviceMap);
        Fluttertoast.showToast(
            msg: "Randevu Talebi Başarıyla Gönderildi.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
        print("GÖNDERILENLER " + selectedHairdresser + " " + resultServices +
            " " + selectedDate + " " + selectedTime);
    }
  }
  @override
  Widget build(BuildContext context) => Scaffold(
    body: buildPages(),
    appBar: AppBar(
      backgroundColor:Color(0xFF6B8BC8),
      title: Text("Randevu Alma"),
    ),
  );
  Widget buildPages() {
    haidresserSelectionPicker = new HaidresserSelectionPicker(userId);
    serviceSelectionPicker = new ServiceSelectionPicker(userId);
    datePickerWidget= new DatePickerWidget();
    timePickerPickerWidget= new TimePickerWidget();
    switch (index) {
      case 0:
        return SingleChildScrollView(
             child: SafeArea(
            child: Container(
            decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/back.png"),fit: BoxFit.fill
          )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30),
            child: Column(

              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                haidresserSelectionPicker,
                const SizedBox(height: 24),
                serviceSelectionPicker,
                const SizedBox(height: 24),
                datePickerWidget,
                const SizedBox(height: 24),
                timePickerPickerWidget,
                const SizedBox(height: 24),
                 MaterialButton(
                  minWidth: 260,
                  height: 60,
                  onPressed: (){
                    _sendSelectedDatas();
                  },
                  //Navigator.push(context, MaterialPageRoute(builder: (context)=> SignupPage()));
                  color: Color(0xFF6B8BC8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)
                  ),
                  child: Text(
                    "Randevu Talebi Gönder",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        );
    }
  }
}