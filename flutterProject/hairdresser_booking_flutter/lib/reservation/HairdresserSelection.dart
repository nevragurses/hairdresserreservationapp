import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
class HairdresserSelection extends StatefulWidget {
  List<String> sortFilter;
  String selectedValue;
  HairdresserSelection(this.sortFilter);
  String getSelectedHairdresser(){
    return selectedValue;
  }
  @override
  _SingleSelectionExampleState createState() => _SingleSelectionExampleState();
}
class _SingleSelectionExampleState extends State<HairdresserSelection> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (ctx, index) {
        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            widget.selectedValue = widget.sortFilter[index];
            setState(() {});
          },
          child: Container(
            color: widget.selectedValue == widget.sortFilter[index]
                ? Colors.green[100]
                : null,
            child: Row(
              children: <Widget>[
                Radio(
                    value: widget.sortFilter[index],
                    groupValue: widget.selectedValue,
                    onChanged: (s) {
                      widget.selectedValue = s;
                      setState(() {});
                    }),
                Text(widget.sortFilter[index])
              ],
            ),
          ),
        );
      },
      itemCount: widget.sortFilter.length,
    );
  }
}