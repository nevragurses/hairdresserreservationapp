import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';
class TimeSelection extends StatefulWidget {
  String selectedTime;
  String getTime() {
    return selectedTime;
  }
  @override
  _TimeSelectionState createState() => _TimeSelectionState();
}

class _TimeSelectionState extends State<TimeSelection> {
  String _onItemFocus;
  List<String> data = [
    "9:00", "9:30",  "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:30",
        "14:00", "15:00", "15:30", "16:00", "16:00" ,"17:00","18:00","18:30"
  ];

  Widget _buildItemList(BuildContext context, int index){
    if(index == data.length)
      return Center(
        child: CircularProgressIndicator(),
      );
    return Container(
      width: 150,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MaterialButton(
            color: Color(0xFF6B8BC8),
            minWidth: 200,
            height: 200,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50)
            ),
            child: Text('${data[index]}',style: TextStyle(fontSize: 30.0,color: Colors.black),),
            onPressed: (){
                  widget.selectedTime=data[index];
                  print("HEEELLOOO " + data[index]);
                  //Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()))
                },
            ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Expanded(
                child: ScrollSnapList(
                  itemBuilder: _buildItemList,
                  itemSize: 150,
                  dynamicItemSize: true,
                  onReachEnd: (){
                    print('Done!');
                  },
                  itemCount: data.length,
                )
            ),
          ],
        ),
      ),
    );
  }
}