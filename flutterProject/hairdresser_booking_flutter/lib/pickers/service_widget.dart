import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/pickers/button_widget.dart';
import 'package:hairdresser_booking_flutter/admin/Service.dart';
import 'package:flutter/material.dart';
import 'package:hairdresser_booking_flutter/reservation/ServiceSelection.dart';
class ServiceSelectionPicker extends StatefulWidget {
  String userId;
  String resultServices;
  ServiceSelectionPicker(String user) {
    this.userId = user;
  }
  String getServices() {
    return resultServices.toString();
  }
  @override
  _ServiceWidgetState createState() => _ServiceWidgetState(userId);
}

class _ServiceWidgetState extends State<ServiceSelectionPicker> {
  String userId;
  List<String> resultServicesInner=null;
  _ServiceWidgetState(String user) {
    this.userId = user;
  }
  String getText() {
    if(resultServicesInner==null)
      return 'Hizmet Seçiniz';
    else{
        widget.resultServices=resultServicesInner.toString();
        return resultServicesInner.toString();
    }
  }

  Future<List<Service>> getServiceList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<Service> _needs = [];
    var uid = userId;
    await firestoreInstance.collection("Admins").doc(uid).collection("Services").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        Map<dynamic, dynamic> values = value.data();
        _needs.add(Service(value.get("name")));
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }
  Future<void> sendDatas(BuildContext context) async {
    List<Service> serviceList=await getServiceList();
    ServiceSelection result = new ServiceSelection(serviceList);
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('Hizmet Listesi'),
            actions: <Widget>[
              MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('İptal'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              MaterialButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Onayla'),
                onPressed: () {
                  setState(() {
                    resultServicesInner = result.getServices();
                    print("returned + " + resultServicesInner.toString());
                    //_update(id, updatedPrice);
                    Navigator.pop(context);
                  });
                },
              ),
            ],
            content: Container(
                width: 300,
                height: 400,
                child: result),
          );
        });
  }
  @override
  Widget build(BuildContext context) => ButtonHeaderWidget(
      title: 'Hizmet Seçimi',
      text: getText(),
      onClicked: () => sendDatas(context)//Navigator.push(context,
    //MaterialPageRoute(builder: (context) => TimeSelection()))
  );
}
