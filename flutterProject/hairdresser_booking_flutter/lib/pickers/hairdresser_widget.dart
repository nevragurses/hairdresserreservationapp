import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/pickers/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:hairdresser_booking_flutter/reservation/HairdresserSelection.dart';
class HaidresserSelectionPicker extends StatefulWidget {
  String userId;
  String selectedHairdresser;
  HaidresserSelectionPicker(String user) {
    this.userId = user;
  }
  String getHairdresser() {
    return selectedHairdresser;
  }
  String getText() {
    if(selectedHairdresser==null)
      return 'Kuaför Seçiniz';
    else {
      return selectedHairdresser;
    }
  }
  @override
  _HairdresserWidgetState createState() => _HairdresserWidgetState(userId);
}
class _HairdresserWidgetState extends State<HaidresserSelectionPicker> {
  String userId;
  List<String> _needs = [];
  List<String> resultServices=[];
  _HairdresserWidgetState(String user) {
    this.userId = user;
  }
  Future<List<String>> getHairdressers()async{
    final firestoreInstance = FirebaseFirestore.instance;
    await firestoreInstance.collection("Hairdressers").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("admin_id").toString()==userId && value.get("acceptanceCase" ) == "true" ) {
          _needs.add(value.get("name"));
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }
  Future<void> sendHairdressers(BuildContext context) async {
    List<String> hairdresserList=await getHairdressers();
    HairdresserSelection result = new HairdresserSelection(hairdresserList);
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('Kuaför Listesi'),
            actions: <Widget>[
              MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('İptal'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              MaterialButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Onayla'),
                onPressed: () {
                  setState(() {
                    String resultString = result.getSelectedHairdresser();
                    //_update(id, updatedPrice);
                      widget.selectedHairdresser=resultString;
                      print("returned + " + resultString.toString());
                    Navigator.pop(context);
                  });
                },
              ),
            ],
            content: Container(
                width: 300,
                height: 400,
                child: result),
          );
        });
  }
  @override
  Widget build(BuildContext context) => ButtonHeaderWidget(
      title: 'Kuaför Seçimi',
      text: widget.getText(),
      onClicked: () => sendHairdressers(context)//Navigator.push(context,
   // MaterialPageRoute(builder: (context) => TimeSelection()))
    );
}
