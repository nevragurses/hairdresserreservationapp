import 'package:hairdresser_booking_flutter/pickers/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:hairdresser_booking_flutter/pickers/time_selection.dart';

class TimePickerWidget extends StatefulWidget {
  String selectedTime;
  String getSelectedTime() {
    return selectedTime;
  }
  @override
  _TimePickerWidgetState createState() => _TimePickerWidgetState();
}
class _TimePickerWidgetState extends State<TimePickerWidget> {
  String time;

  String getText() {
    if (time == null) {
      return 'Saat Seçiniz';
    } else {
      return time;
    }
  }
  Future<void> sendDatas(BuildContext context) async {
    TimeSelection result = new TimeSelection();
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('Saat Seçimi'),
            actions: <Widget>[
              MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('İptal'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              MaterialButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Onayla'),
                onPressed: () {
                  setState(() {
                    time = result.getTime();
                    widget.selectedTime=time;
                    Navigator.pop(context);
                  });
                },
              ),
            ],
            content: Container(
                width: 300,
                height: 400,
                child: result),
          );
        });
  }
  @override
  Widget build(BuildContext context) => ButtonHeaderWidget(
        title: 'Saat Seçimi',
        text: getText(),
        onClicked: () => sendDatas(context)//Navigator.push(context,
            //MaterialPageRoute(builder: (context) => TimeSelection()))
        );

}
