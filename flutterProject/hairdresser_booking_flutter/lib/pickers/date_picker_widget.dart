import 'package:hairdresser_booking_flutter/pickers/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class DatePickerWidget extends StatefulWidget {
  String resultDate;
  String getDate() {
    return resultDate;
  }
  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  DateTime date;
  Locale _locale;
  void setLocale(Locale value) {
    setState(() {
      _locale = value;
    });
  }
  String getText() {
    if (date == null) {
      return 'Gün Seçiniz';
    } else {
      widget.resultDate=DateFormat('dd/MM/yyyy').format(date);
      return DateFormat('dd/MM/yyyy').format(date);
      // return '${date.month}/${date.day}/${date.year}';
    }
  }

  @override
  Widget build(BuildContext context) => ButtonHeaderWidget(
        title: 'Gün Seçimi',
        text: getText(),
        onClicked: () { setLocale(Locale.fromSubtags(languageCode: 'tr'));
                pickDate(context); }
      );

  Future pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    setLocale(Locale.fromSubtags(languageCode: 'tr'));
    final newDate = await showDatePicker(
      context: context,
        locale : const Locale("tr","TR"),
      initialDate: date ?? initialDate,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
    );

    if (newDate == null) return;

    setState(() => date = newDate);
  }
}
