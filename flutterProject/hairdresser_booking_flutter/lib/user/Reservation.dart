import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/register/SignUp.dart';
import 'package:hairdresser_booking_flutter/reservation/DateSelection.dart';
import 'package:hairdresser_booking_flutter/reservation/ServiceSelection.dart';
import 'package:hairdresser_booking_flutter/reservation/HairdresserSelection.dart';
import 'package:hairdresser_booking_flutter/admin/Service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class ReservationPage extends StatefulWidget {
  String userId;
  ReservationPage(String user) {
    this.userId = user;
  }
  @override
  State<StatefulWidget> createState()  => _ReservationState(userId);
}
class _ReservationState extends State<ReservationPage> {
  bool setStateRan=false;
  String userId;
  String selectedHairdresser;
  FirebaseStorage storage = FirebaseStorage.instance;
  List<String> _needs = [];
  List<String> resultServices=[];
  String companyName="";
  _ReservationState(String user) {
    this.userId = user;
    getHairdressers();
  }
  Future<List<String>> getHairdressers()async{
    final firestoreInstance = FirebaseFirestore.instance;
    await firestoreInstance.collection("Hairdressers").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("admin_id").toString()==userId ) {
          _needs.add(value.get("name"));
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }
  Future<List<Service>> getServiceList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<Service> _needs = [];
    var uid = userId;
    await firestoreInstance.collection("Admins").doc(uid).collection("Services").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        Map<dynamic, dynamic> values = value.data();
        _needs.add(Service(value.get("name")));
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }
  Future<void> sendHairdressers(BuildContext context) async {
    List<String> hairdresserList=await getHairdressers();
    HairdresserSelection result = new HairdresserSelection(hairdresserList);
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('Kuaför Listesi'),
            actions: <Widget>[
              MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('İptal'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              MaterialButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Onayla'),
                onPressed: () {
                  setState(() {
                    String resultString = result.getSelectedHairdresser();
                    selectedHairdresser=resultString;
                    print("returned + " + resultString.toString());
                    //_update(id, updatedPrice);
                    Navigator.pop(context);
                  });
                },
              ),
            ],
            content: Container(
                width: 300,
                height: 400,
                child: result),
          );
        });
  }
  Future<void> sendDatas(BuildContext context) async {
    List<Service> serviceList=await getServiceList();
    ServiceSelection result = new ServiceSelection(serviceList);
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('Hizmet Listesi'),
            actions: <Widget>[
              MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('İptal'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              MaterialButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Onayla'),
                onPressed: () {
                  setState(() {
                    resultServices = result.getServices();
                    print("returned + " + resultServices.toString());
                    //_update(id, updatedPrice);
                    Navigator.pop(context);
                  });
                },
              ),
            ],
            content: Container(
                width: 300,
                height: 400,
                child: result),
          );
        });
  }
  _sendSelectedDatas() {

    if (selectedHairdresser == null) {
      Fluttertoast.showToast(
          msg: "Lütfen kuaför seçiniz!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else if (resultServices.length == 0) {
      Fluttertoast.showToast(
          msg: "Lütfen hizmet seçiniz! ",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else {
      print("GÖNDERILENLER" + resultServices.toString() + " " + selectedHairdresser);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => DateSelection(userId)));
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 100),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 150),
                      child: GestureDetector(
                        onTap: () => {
                          sendHairdressers(context)
                        },
                        child: Text(
                          "Kuaför Seçiniz",
                          style: TextStyle(
                              fontSize: 20,
                              color: Color(0xFF6B8BC8)
                          ),
                        ),
                      ),
                    ),
                  Container(
                        child: GestureDetector(
                          onTap: () => {
                            sendDatas(context)
                          },
                          child: Text(
                            "Hizmet Seçiniz",
                            style: TextStyle(
                                fontSize: 20,
                                color: Color(0xFF6B8BC8)
                            ),
                          ),
                        ),
                      ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10),
                      child: MaterialButton(
                        minWidth: 260,
                        height: 60,
                        onPressed: (){
                          _sendSelectedDatas();
                        },
                        //Navigator.push(context, MaterialPageRoute(builder: (context)=> SignupPage()));
                        color: Color(0xFF6B8BC8),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)
                        ),
                        child: Text(
                          "Bir Sonraki Adıma Geç",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 18
                          ),
                        ),
                      ),
                    ),
                    ],
                  ),
            ),

        ),
      ),
    );
  }
}
