import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesLogin.dart';
import 'package:hairdresser_booking_flutter/customer/ConfirmedCustomerReservations.dart';
import 'package:hairdresser_booking_flutter/customer/CustomerReservations.dart';
import 'package:hairdresser_booking_flutter/customer/RejectedCustomerReservations.dart';
import 'package:hairdresser_booking_flutter/model/AdminModel.dart';
import 'package:hairdresser_booking_flutter/register/SignUp.dart';
import 'package:hairdresser_booking_flutter/user/OneCompanyPage.dart';
class Companies extends StatefulWidget {
  @override
  _CompaniesState createState() => _CompaniesState();
}
class _CompaniesState extends State<Companies> {
  final firestoreInstance = FirebaseFirestore.instance;
  List<AdminModel> _needs = [];
  List<AdminModel> adminModelList;
  Future getCloudFirestoreUsers() async {
    print("getCloudFirestore");
    firestoreInstance.collection("Admins").get().then((querySnapshot) {
      _needs.clear();
      print("users: results: length: " + querySnapshot.docs.length.toString());
      querySnapshot.docs.map((doc) => ({doc.id, doc.data() }));
      querySnapshot.docs.forEach((value) {
        Map<dynamic, dynamic> values = value.data();
        _needs.add(AdminModel.fromMap(values));
        print(values);
        print("users: results: length: " + querySnapshot.docs.length.toString());
        print(_needs.length);
        print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
  }
  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        Navigator.push(context, MaterialPageRoute(builder: (context) => CustomerReservationsPage()));
        break;
      case 1:
      Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmedCustomerReservationsPage()));
      break;
      case 2:
        Navigator.push(context, MaterialPageRoute(builder: (context) => RejectedCustomerReservationsPage()));
        break;
      case 3:
        Navigator.push(context, MaterialPageRoute(builder: (context) => TabPagesLogin()));

    }
  }
  @override
  Widget build(BuildContext context) {
    getCloudFirestoreUsers();
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Kuaför İşletmeleri"),
        centerTitle: true,
        actions: [
          Theme(
            data: Theme.of(context).copyWith(
              dividerColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.white),
              textTheme: TextTheme().apply(bodyColor: Colors.white),
            ),
            child: PopupMenuButton<int>(
              color: Colors.indigo,
              onSelected: (item) => onSelected(context, item),
              itemBuilder: (context) => [
                PopupMenuItem<int>(
                  value: 0,
                  child: Text('Randevu Talepleri'),
                ),
                PopupMenuItem<int>(
                  value: 1,
                  child: Text('Onaylanan Randevular'),
                ),
                PopupMenuItem<int>(
                  value: 2,
                  child: Text('Reddedilen Randevular'),
                ),
                PopupMenuDivider(),
                PopupMenuItem<int>(
                  value: 3,
                  child: Row(
                    children: [
                      Icon(Icons.logout),
                      const SizedBox(width: 8),
                      Text('Çıkış'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
        body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
      child: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('Admins').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView(
              padding: EdgeInsets.only(top:180),
            children: snapshot.data.docs.map((document) {
              return Card(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
                    borderRadius: BorderRadius.circular(35.0),
                  ),
                child: ListTile(
                  leading:Image.asset(
                    'assets/scissor.jpg',
                    fit: BoxFit.cover,
                  ),
                  title:  Text(
                    document.get("companyName"),
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Color(0xFF6B8BC8),
                      fontSize: 18
                    ),
                  ),
                  subtitle: Text(document.get("address")),
                  isThreeLine: true,
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => OneCompanyPage((document.get("admin_id").toString()))),
                    );
                  },
                ),
              );
            }).toList(),
          );
        },
      ),
    ),
        ),
    );
  }
}