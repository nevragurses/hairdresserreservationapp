import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesLogin.dart';
import 'package:hairdresser_booking_flutter/customer/ConfirmedCustomerReservations.dart';
import 'package:hairdresser_booking_flutter/customer/CustomerReservations.dart';
import 'package:hairdresser_booking_flutter/customer/RejectedCustomerReservations.dart';
import 'package:hairdresser_booking_flutter/model/AdminModel.dart';
import 'OneCompanyPage.dart';
class CompanyList extends StatefulWidget {
  @override
  State<StatefulWidget> createState()  => _CompanyListState();
}
class _CompanyListState extends State<CompanyList> {
  List<Card> cardList = new List();
  bool setStateRan=false;

  Card newsCard(AdminModel news){
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
        borderRadius: BorderRadius.circular(35.0),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),
        title: Text(news.companyName.toString()),
        subtitle: Text(news.address.toString()),
        //trailing: Icon(Icons.more_vert),
        isThreeLine: true,
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => OneCompanyPage(news.admin_id.toString())),
          );
        },
      ),
    );
  }
  Future<List<AdminModel>> getNewsList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<AdminModel> _needs = [];
    await firestoreInstance.collection("Admins").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        Map<dynamic, dynamic> values = value.data();
        if(value.get("acceptanceCase")=="true")
        _needs.add(AdminModel.fromMap(values));
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }

  createCardList()async{
    List<AdminModel> newsList =await getNewsList();
      for(int i=0;i< newsList.length;++i){
        cardList.add(newsCard(newsList[i]));
      }
      setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
void onSelected(BuildContext context, int item) {
  switch (item) {
    case 0:
      Navigator.push(context, MaterialPageRoute(builder: (context) => CustomerReservationsPage()));
      break;
    case 1:
      Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmedCustomerReservationsPage()));
      break;
    case 2:
      Navigator.push(context, MaterialPageRoute(builder: (context) => RejectedCustomerReservationsPage()));
      break;
    case 3:
      Navigator.push(context, MaterialPageRoute(builder: (context) => TabPagesLogin()));

  }
}
class CompanyListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Kuaför İşletmeleri"),
        centerTitle: true,
        actions: [
          Theme(
            data: Theme.of(context).copyWith(
              dividerColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.white),
              textTheme: TextTheme().apply(bodyColor: Colors.white),
            ),
            child: PopupMenuButton<int>(
              color: Colors.indigo,
              onSelected: (item) => onSelected(context, item),
              itemBuilder: (context) => [
                PopupMenuItem<int>(
                  value: 0,
                  child: Text('Randevu Talepleri'),
                ),
                PopupMenuItem<int>(
                  value: 1,
                  child: Text('Onaylanan Randevular'),
                ),
                PopupMenuItem<int>(
                  value: 2,
                  child: Text('Reddedilen Randevular'),
                ),
                PopupMenuDivider(),
                PopupMenuItem<int>(
                  value: 3,
                  child: Row(
                    children: [
                      Icon(Icons.logout),
                      const SizedBox(width: 8),
                      Text('Çıkış'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(vertical: 120),
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 60),
                child:CompanyList()
            ),
          ),
        ),
      ),
    );
  }
}