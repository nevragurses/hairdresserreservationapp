import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/admin/HairdresserList.dart';
import 'package:hairdresser_booking_flutter/reservation/DateSelection.dart';
import 'package:hairdresser_booking_flutter/reservation/ServiceSelection.dart';
import 'package:hairdresser_booking_flutter/admin/Service.dart';
import 'package:hairdresser_booking_flutter/user/GivenCompanyServices.dart';
import 'package:hairdresser_booking_flutter/user/Reservation.dart';
class OneCompanyPage extends StatefulWidget {
  String userId;
  OneCompanyPage(String user) {
    this.userId = user;
  }
  @override
  State<StatefulWidget> createState()  => _OneCompanyPageState(userId);
}
class _OneCompanyPageState extends State<OneCompanyPage> {
  String userId;
  FirebaseStorage storage = FirebaseStorage.instance;
  List<String> _needs = [];
  String companyName="";
  _OneCompanyPageState(String user) {
    this.userId = user;
    _getName();
  }
  Future<List<Service>> getServiceList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<Service> _needs = [];
    var user = await FirebaseAuth.instance.currentUser;
    var uid = userId;
    await firestoreInstance.collection("Admins").doc(uid).collection("Services").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        Map<dynamic, dynamic> values = value.data();
        _needs.add(Service(value.get("name")));
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }
  Future<void> _getName()async{
    DocumentReference users = await FirebaseFirestore.instance.collection("Admins").doc(userId);
    //DocumentReference users = FirebaseFirestore.instance.collection('Admins').doc(id);
    await users.get().then((documentSnapshot) {
      companyName = documentSnapshot.get("companyName").toString();
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
  }
  Future<List<Map<String, dynamic>>> _loadingImages() async {
    List<Map<String, dynamic>> files = [];
    final ListResult result = await storage.ref().list();
    final List<Reference> allFiles = result.items;
    await Future.forEach<Reference>(allFiles, (file) async {
      var uid = userId;
      final firestoreInstance = FirebaseFirestore.instance;
      await firestoreInstance.collection("Admins").doc(uid).collection("Images").get().then((querySnapshot) {
        querySnapshot.docs.forEach((value) {
          _needs.add(value.get("image_name"));
          //print(value.data());
        });
      }).catchError((onError) {
        print("getCloudFirestoreUsers: ERROR");
        print(onError);
      });
      if(_needs.contains(file.name).toString()=="true"){
        final String fileUrl = await file.getDownloadURL();
        final FullMetadata fileMeta = await file.getMetadata();
        files.add({
          "url": fileUrl,
          "path": file.fullPath,
        });
      }
    });
    return files;
  }
  Future<void> sendDatas(BuildContext context) async {
    List<Service> serviceList=await getServiceList();
    ServiceSelection result = new ServiceSelection(serviceList);
      showDialog(
          context: context,
          builder: (ctx) {
            return AlertDialog(
              title: Text('Hizmet Listesi'),
              actions: <Widget>[
                MaterialButton(
                  color: Colors.red,
                  textColor: Colors.white,
                  child: Text('İptal'),
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                ),
                MaterialButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  child: Text('Güncelle'),
                  onPressed: () {
                    setState(() {
                      List<String> resultString = result.getServices();
                      print("returned + " + resultString.toString());
                      //_update(id, updatedPrice);
                      Navigator.pop(context);
                    });
                  },
                ),
              ],
              content: Container(
                  width: 300,
                  height: 400,
                  child: result),
            );
          });
    }
  @override
  Widget build(BuildContext context) {
    _getName();
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Color(0xFF6B8BC8),
        title: Text("İşletme Bilgileri"),
      ),
      body: SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
            Container(
              height: 200,
              child: FutureBuilder(
                future: _loadingImages(),
                builder: (context,
                    AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      scrollDirection: Axis.horizontal,
                      physics: const AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        final image = snapshot.data[index];
                        return Container(
                          margin: EdgeInsets.symmetric(vertical: 20.0),
                          height: 200.0,
                            child: Image.network(image['url'],
                              height: 200,
                              fit: BoxFit.fitWidth,),
                        );
                      },
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
            Container(
              height: (MediaQuery.of(context).size.height),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical:10),
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                      Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                      child: Text(
                        "Hizmetler",
                        style: TextStyle(
                            color: Color(0xFF6B8BC8),
                            fontWeight: FontWeight.w600,
                            fontSize: 18
                        ),
                      ),
                    ),
                    //padding: EdgeInsets.symmetric(vertical: 0),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                        child: GivenCompanyServices(userId)
                    ),
                    Padding(
                    padding: EdgeInsets.symmetric(horizontal: 1, vertical: 0),
                    child: Text(
                      "Kuaförler",
                      style: TextStyle(
                          color: Color(0xFF6B8BC8),
                          fontWeight: FontWeight.w600,
                          fontSize: 18
                      ),
                    ),
                  ),
                  //padding: EdgeInsets.symmetric(vertical: 0),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                      child: HairdresserList(userId)
                  ),
                  Padding(
                        padding: EdgeInsets.symmetric(horizontal: 1, vertical: 5),
                        child: GestureDetector(
                          onTap: () => {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => DateSelection(userId)))
                            //Navigator.push(context,MaterialPageRoute(builder: (context) => MultiSelectionExample()))
                            //Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()))
                          },
                          child: Text(
                            "Randevu Almak İster Misiniz?",
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Color(0xFF6B8BC8),
                                fontWeight: FontWeight.w600,
                                fontSize: 18
                            ),
                          ),
                        ),
                      ),

                ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

