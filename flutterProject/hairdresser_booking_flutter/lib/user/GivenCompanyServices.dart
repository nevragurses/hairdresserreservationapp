import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/admin/HairdresserList.dart';
import 'package:hairdresser_booking_flutter/model/ServiceModel.dart';

class GivenCompanyServices extends StatefulWidget {
  String userId;
  GivenCompanyServices(String user){
    this.userId=user;
  }
  @override
  State<StatefulWidget> createState()  => _GivenCompanyState(userId);
}
class _GivenCompanyState extends State<GivenCompanyServices> {
  List<Card> cardList = new List();
  bool setStateRan=false;
  String userId;
  _GivenCompanyState(String user){
    this.userId=user;
  }
  Card servicesCard(ServiceModel services){
    return Card(
      shadowColor: Color(0xFF6B8BC8),
      // Change this
      child: ListTile(

        leading:Image.asset(
          'assets/ingredients.jpg',
          fit: BoxFit.cover,
        ),
        title: Text("Hizmet: " +services.name.toString(),
            style: TextStyle(
            color: Colors.redAccent,
            fontWeight: FontWeight.w600,
            fontSize: 16
        ),),
        subtitle: Text("Fiyat: " + services.price.toString()  ),
        isThreeLine: true,
      ),
    );
  }
  Future<List<ServiceModel>> getList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<ServiceModel> _needs = [];
    var uid = userId;
    await firestoreInstance.collection("Admins").doc(uid).collection("Services").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        Map<dynamic, dynamic> values = value.data();
        _needs.add(ServiceModel.fromMap(values));
        print("İÇERDE"+ value.data().toString());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }
  createCardList()async{
    List<ServiceModel> serviceList =await getList();
    for(int i=0;i< serviceList.length;++i){
      cardList.add(servicesCard(serviceList[i]));
    }
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}