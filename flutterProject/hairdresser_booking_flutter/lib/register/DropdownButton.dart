import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesLogin.dart';

import 'SignUp.dart';
class DropdownButtonClass extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
class _MyAppState extends State<DropdownButtonClass> {
  String _email, _password,_name,_phone;
  String _compName,_addr;
  _MyAppState({this.firestore});
  final auth = FirebaseAuth.instance;
  final firestoreInstance = FirebaseFirestore.instance;
  final FirebaseFirestore firestore;
  String dropdownValue,nameValue;
  Future<void>  _onPressed(User user) {
    DocumentReference users = FirebaseFirestore.instance.collection('Hairdressers').doc(user.uid);
    return users
        .set({
      'acceptanceCase': "false",
      'name': _name,
      'email': _email,
      'phone': _phone,
      'companyName':_compName,
      'address':_addr,
      'user_id':user.uid
    })
        .then((value) => print("Kullanıcı eklendi"))
        .catchError((error) => print("Kullanıcı eklenirken hata oluştu: $error"));
  }
  _register() async {
    if (_email == null || _password==null || _phone==null || _name==null) {
      Fluttertoast.showToast(
          msg: "Lütfen tüm alanları doldurunuz.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else {
      try {
        UserCredential result = await auth.createUserWithEmailAndPassword(email: _email, password: _password);
        User user = result.user;
        _onPressed(user);
        Navigator.push(context,MaterialPageRoute(builder: (context) => HomePage()));

      } on FirebaseAuthException catch (error) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(error.toString()),
        ));
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child:SafeArea(
          child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 150),
          decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/loginback.png"),fit: BoxFit.fill
                )
          ),
            child: StreamBuilder(
                stream: FirebaseFirestore.instance.collection('Admins').snapshots(),
                // ignore: missing_return
                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 5),
                          child: TextField(
                            keyboardType: TextInputType.name,
                            decoration: InputDecoration(
                              hintText: 'İsim-Soyisim',
                              prefixIcon: Icon(Icons.person),
                            ),

                            onChanged: (value) {
                              setState(() {
                                _name = value.trim();
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 5),
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              hintText: 'Email',
                              prefixIcon: Icon(Icons.email),
                            ),
                            onChanged: (value) {
                              setState(() {
                                _email = value.trim();
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 5),
                          child: TextFormField(
                            decoration: InputDecoration(hintText: 'Telefon',
                              prefixIcon: Icon(Icons.local_phone),),

                            onChanged: (value) {
                              setState(() {
                                _phone = value.trim();
                              });
                            },
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 5),
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: _compName,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 42,
                            style: TextStyle(color: Colors.black,),
                            underline: Container(
                              height: 2,
                              color: Color(0xFF6B8BC8),
                            ),
                            hint: Text('İşletme Adı'),
                            onChanged: (String newValue) {
                              setState(() {
                                _compName = newValue;
                              });
                            },
                            items: snapshot.data.docs.map((
                                DocumentSnapshot document) {
                              return new DropdownMenuItem<String>(
                                  value: document.get("companyName"),
                                  child: new Container(
                                    decoration: new BoxDecoration(
                                        borderRadius: new BorderRadius.circular(
                                            3.0)
                                    ),

                                    height: 40.0,
                                    padding: EdgeInsets.fromLTRB(
                                        10.0, 2.0, 10.0, 0.0),
                                    //color: primaryColor,
                                    child: new Text(document.get("companyName")),
                                  )
                              );
                            }).toList(),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 5),
                        child: DropdownButton<String>(
                          isExpanded: true,
                          value: _addr,
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: 42,
                          style: TextStyle(color: Colors.black),
                          underline: Container(
                            height: 2,
                            color: Color(0xFF6B8BC8),
                          ),
                          hint: Text('İşletme Adres'),
                          onChanged: (String newValue) {
                            setState(() {
                              _addr = newValue;
                            });
                          },
                          items: snapshot.data.docs.map((
                              DocumentSnapshot document) {
                            return new DropdownMenuItem<String>(
                                value: document.get("address"),
                                child: new Container(
                                  decoration: new BoxDecoration(
                                    //color: Colors.blueAccent,
                                      borderRadius: new BorderRadius.circular(
                                          3.0)
                                  ),
                                  height: 40.0,
                                  padding: EdgeInsets.fromLTRB(
                                      10.0, 2.0, 10.0, 0.0),
                                  //color: primaryColor,
                                  child: new Text(document.get("address")),
                                )
                            );
                          }).toList(),
                        ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 5),
                          child: TextFormField(
                            keyboardType: TextInputType.phone,
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: 'Şifre', prefixIcon: Icon(Icons.lock),),
                            onChanged: (value) {
                              setState(() {
                                _password = value.trim();
                              });
                            },
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(
                              horizontal: 70, vertical: 10),
                          child: MaterialButton(
                            minWidth: 260,
                            height: 60,
                            onPressed: () {
                              _register() ;
                            },
                            //Navigator.push(context, MaterialPageRoute(builder: (context)=> SignupPage()));
                            color: Color(0xFF6B8BC8),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)
                            ),
                            child: Text(
                              "Kayıt Ol",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18
                              ),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => TabPagesLogin_Third()))
                            },
                            child: Text(
                              "Zaten Hesabınız Var Mı? Giriş Yapın",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF6B8BC8)
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
            ),
          ),
        ),
      ),
    );
  }
}