import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesLogin.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/welcome/HomePage.dart';

class AdminRegister extends StatefulWidget {
  @override
  _AdminRegisterState createState() => _AdminRegisterState();
}
class _AdminRegisterState extends State<AdminRegister> {
  String _email, _password,_name,_phone;
  String _compName,_province,_district,_neighborhood,_street,_addr;
  final auth = FirebaseAuth.instance;
  int flag;
  Future<void>  _onPressed(User user) {

    // Create a CollectionReference called users that references the firestore collection
    DocumentReference users = FirebaseFirestore.instance.collection('Admins').doc(user.uid);
    return users
        .set({
      'acceptanceCase': "waiting",
      'name': _name,
      'email': _email,
      'phone': _phone,
      'companyName':_compName,
      'address':_addr,
      'province':_province,
      'district':_district,
      'street':_street,
      'neighborhood':_neighborhood,
      'admin_id':user.uid
    })
        .then((value) => print("Kullanıcı eklendi"))
        .catchError((error) => print("Kullanıcı eklenirken hata oluştu: $error"));
  }

  _register() async {
    if (_email == null || _password==null || _phone==null || _name==null) {
      Fluttertoast.showToast(
          msg: "Lütfen tüm alanları doldurunuz.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else {
      try {
        UserCredential result = await auth.createUserWithEmailAndPassword(email: _email, password: _password);
        User user = result.user;
        _onPressed(user);
        Navigator.push(context, MaterialPageRoute(builder: (context) => TabPagesLogin_Third()));

      } on FirebaseAuthException catch (error) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(error.toString()),
        ));
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child:SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/loginback.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 120),
          child: SingleChildScrollView(
            child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextField(
                  keyboardType: TextInputType.name,
                  decoration: InputDecoration(
                    hintText: 'İsim-Soyisim',
                    prefixIcon: Icon(Icons.person),
                  ),

                  onChanged: (value) {
                    setState(() {
                      _name = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    hintText: 'Email',
                    prefixIcon: Icon(Icons.email),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _email = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  decoration: InputDecoration(hintText: 'Telefon', prefixIcon: Icon(Icons.local_phone),),

                  onChanged: (value) {
                    setState(() {
                      _phone = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  decoration: InputDecoration(hintText: 'İşletme Adı', prefixIcon: Icon(Icons.info),),
                  onChanged: (value) {
                    setState(() {
                      _compName = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  decoration: InputDecoration(hintText: 'İşletme İl', prefixIcon: Icon(Icons.location_city),),
                  onChanged: (value) {
                    setState(() {
                      _province = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  decoration: InputDecoration(hintText: 'İşletme İlçe', prefixIcon: Icon(Icons.location_on),),
                  onChanged: (value) {
                    setState(() {
                      _district = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  decoration: InputDecoration(hintText: 'İşletme Mahalle', prefixIcon: Icon(Icons.location_on),),
                  onChanged: (value) {
                    setState(() {
                      _neighborhood = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  decoration: InputDecoration(hintText: 'İşletme Cadde', prefixIcon: Icon(Icons.location_on),),
                  onChanged: (value) {
                    setState(() {
                      _street= value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  keyboardType: TextInputType.phone,
                  obscureText: true,
                  decoration: InputDecoration(hintText: 'Şifre' , prefixIcon: Icon(Icons.lock),),
                  onChanged: (value) {
                    setState(() {
                      _password = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  decoration: InputDecoration(hintText: 'İşletme Adres', prefixIcon: Icon(Icons.location_on),),
                  onChanged: (value) {
                    setState(() {
                      _addr= value.trim();
                    });
                  },
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10),
                child: MaterialButton(
                  minWidth: 260,
                  height: 60,
                  onPressed: (){
                    _register() ;
                  },
                  //Navigator.push(context, MaterialPageRoute(builder: (context)=> SignupPage()));
                  color: Color(0xFF6B8BC8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)
                  ),
                  child: Text(
                    "Kayıt Ol",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                child: GestureDetector(
                  onTap: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => TabPagesLogin_Third()))
                  },
                  child: Text(
                    "Zaten Hesabınız Var Mı? Giriş Yapın",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF6B8BC8)
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      ),
      ),
    );
  }
}