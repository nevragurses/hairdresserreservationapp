import 'package:flutter/material.dart';
import 'package:hairdresser_booking_flutter/login/Login.dart';
import 'package:swipeable_page_route/swipeable_page_route.dart';
class TabPagesLogin extends StatefulWidget {
  @override
  TabPagesState createState() => TabPagesState();
}
class TabPagesLogin_Second extends StatefulWidget {
  @override
  TabPagesSecond createState() => TabPagesSecond();
}
class TabPagesLogin_Third extends StatefulWidget {
  @override
  TabPagesThird createState() => TabPagesThird();
}
class TabPagesState extends State<TabPagesLogin>
    with SingleTickerProviderStateMixin {
  static const _tabCount = 3;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabCount, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MorphingAppBar(
        backgroundColor: Color(0xFF6B8BC8) ,
        title: Text('Giriş Yap'),
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: <Widget>[
            Tab(text: 'KULLANICI'),
            Tab(text: 'KUAFÖR'),
            Tab(text: 'FİRMA YÖNETİCİSİ'),
          ],
        ),
      ),
      body: TabBarView(
          controller: _tabController,
          children: [
            LoginScreen(0),
            LoginScreen(1),
            LoginScreen(2),
          ]
      ),
    );
  }
}
class TabPagesSecond extends State<TabPagesLogin_Second>
    with SingleTickerProviderStateMixin {
  static const _tabCount = 3;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabCount, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MorphingAppBar(
        backgroundColor: Color(0xFF6B8BC8) ,
        title: Text('Giriş Yap'),
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: <Widget>[
            Tab(text: 'KUAFÖR'),
            Tab(text: 'KULLANICI'),
            Tab(text: 'FİRMA YÖNETİCİSİ'),
          ],
        ),
      ),
      body: TabBarView(
          controller: _tabController,
          children: [
            LoginScreen(1),
            LoginScreen(0),
            LoginScreen(2),
          ]
      ),
    );
  }
}
class TabPagesThird extends State<TabPagesLogin_Third>
    with SingleTickerProviderStateMixin {
  static const _tabCount = 3;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabCount, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MorphingAppBar(
        backgroundColor: Color(0xFF6B8BC8) ,
        title: Text('Giriş Yap'),
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: <Widget>[
            Tab(text: 'FİRMA YÖNETİCİSİ'),
            Tab(text: 'KULLANICI'),
            Tab(text: 'KUAFÖR'),
          ],
        ),
      ),
      body: TabBarView(
          controller: _tabController,
          children: [
            LoginScreen(2),
            LoginScreen(0),
            LoginScreen(1),
            //LoginScreen("Kullanıcı"),
            //LoginScreen("Kuaför"),
            //LoginScreen("Firma Yöneticisi")
          ]
      ),
    );
  }
}