import 'package:flutter/material.dart';
import 'package:hairdresser_booking_flutter/register/CostumerRegister.dart';
import 'package:hairdresser_booking_flutter/register/HairdresserRegister.dart';
import 'package:hairdresser_booking_flutter/register/AdminRegister.dart';
import 'package:swipeable_page_route/swipeable_page_route.dart';
class TabPagesRegister extends StatefulWidget {
  TabPagesRegisterFirst createState() => TabPagesRegisterFirst();
}
class TabPagesRegister_Second extends StatefulWidget {
  TabPagesRegisterSecond createState() => TabPagesRegisterSecond();
}
class TabPagesRegister_Third extends StatefulWidget {
  TabPagesRegisterThird createState() => TabPagesRegisterThird();
}
class TabPagesRegisterFirst extends State<TabPagesRegister>
    with SingleTickerProviderStateMixin {
  static const _tabCount = 3;
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabCount, vsync: this);
  }
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MorphingAppBar(
        backgroundColor: Color(0xFF6B8BC8) ,
        title: Text('Üye Ol'),
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: <Widget>[
            Tab(text: 'KULLANICI'),
            Tab(text: 'KUAFÖR'),
            Tab(text: 'FİRMA YÖNETİCİSİ'),
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
      body: TabBarView(
          controller: _tabController,
          children:<Widget> [
            CostumerRegister(),
            HairdresserRegister(),
            AdminRegister(),
          ]
      ),
    );
  }
}
class TabPagesRegisterSecond extends State<TabPagesRegister_Second>
    with SingleTickerProviderStateMixin {
  static const _tabCount2 = 3;
  TabController _tabController2;
  @override
  void initState() {
    super.initState();
    _tabController2 = TabController(length: _tabCount2, vsync: this);
  }

  @override
  void dispose() {
    _tabController2.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MorphingAppBar(
        backgroundColor: Color(0xFF6B8BC8) ,
        title: Text('Üye Ol'),
        bottom: TabBar(
          controller: _tabController2,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: <Widget>[
            Tab(text: 'KUAFÖR'),
            Tab(text: 'KULLANICI'),
            Tab(text: 'FİRMA YÖNETİCİSİ'),
          ],
        ),
      ),

      body: TabBarView(
          controller: _tabController2,
          children: [
            HairdresserRegister(),
            CostumerRegister(),
            AdminRegister(),
          ]
      ),
    );
  }
}

class TabPagesRegisterThird extends State<TabPagesRegister_Third>
    with SingleTickerProviderStateMixin {
  static const _tabCount2 = 3;
  TabController _tabController2;
  @override
  void initState() {
    super.initState();
    _tabController2 = TabController(length: _tabCount2, vsync: this);
  }

  @override
  void dispose() {
    _tabController2.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MorphingAppBar(
        backgroundColor: Color(0xFF6B8BC8) ,
        title: Text('Üye Ol'),
        bottom: TabBar(
          controller: _tabController2,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: <Widget>[
            Tab(text: 'FİRMA YÖNETİCİSİ'),
            Tab(text: 'KULLANICI'),
            Tab(text: 'KUAFÖR'),
          ],
        ),
      ),

      body: TabBarView(
          controller: _tabController2,
          children: [
            AdminRegister(),
            CostumerRegister(),
            HairdresserRegister(),
          ]
      ),
    );
  }
}

