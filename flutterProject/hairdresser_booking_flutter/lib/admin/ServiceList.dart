import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/model/HairdresserModel.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/model/ServiceModel.dart';
import 'package:hairdresser_booking_flutter/register/SignUp.dart';
class ServiceList extends StatefulWidget {
  @override
  _ServiceListState createState() => _ServiceListState();
}
class _ServiceListState extends State<ServiceList> {
  List<Card> cardList = new List();
  List<ServiceModel> serviceList= new List();
  List<String> serviceNameList= new List();
  List<Text> prices = new List();
  bool setStateRan=false;
  String updatedPrice;
  _delete(String id){
    var user = FirebaseAuth.instance.currentUser;
    var uid = user.uid;
    DocumentReference users = FirebaseFirestore.instance.collection("Admins").doc(uid).collection("Services").doc(id);
    users.get().then((documentSnapshot) {
      documentSnapshot.reference.delete();
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });

    Fluttertoast.showToast(
        msg: "Hizmet Silindi!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );

  }
  _update(String id,String price){
    if(price==null){
      Fluttertoast.showToast(
          msg: "Lütfen fiyat giriniz! İşlem Başarısız.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else if(price!=null && price.length==0){
      Fluttertoast.showToast(
          msg: "Lütfen fiyat giriniz! İşlem Başarısız.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else {
      var user = FirebaseAuth.instance.currentUser;
      var uid = user.uid;
      DocumentReference users = FirebaseFirestore.instance.collection("Admins")
          .doc(uid).collection("Services")
          .doc(id);
      users.get().then((documentSnapshot) {
        final Map<String, String> someMap = {
          "price": price,
        };
        documentSnapshot.reference.update(someMap);
      }).catchError((onError) {
        print("getCloudFirestoreUsers: ERROR");
        print(onError);
      });

      Fluttertoast.showToast(
          msg: "Fiyat Güncellendi!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
      setState(() {
      });
    }
  }
  Future<void> _displayUpdateDialog(BuildContext context,String id,int i) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Yeni Fiyatı Giriniz'),
            content: TextField(
              keyboardType: TextInputType.number,
              onChanged: (value) {
                setState(() {
                  updatedPrice = value;
                });
              },
              decoration: InputDecoration(hintText: "Fiyat"),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('İptal'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              FlatButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Güncelle'),
                onPressed: () {
                  setState(() {
                      prices[i]= new Text ("Fiyat: " + updatedPrice.toString());
                      _update(id, updatedPrice);
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }
  Card newsCard(ServiceModel services,int i){
    prices.add(new Text("Fiyat: " +  services.price.toString()));
    return  Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),
        title: Text("Hizmet: " + services.name.toString()),
        subtitle: prices[prices.length-1],
        isThreeLine: true,
        trailing: Wrap(
          spacing: 12, // space between two icons
          children: <Widget>[
            new IconButton(
              icon: new Icon(Icons.delete),
              onPressed: ()
                => showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                  title: const Text('Hizmeti Silmek İstiyor Musunuz?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Hayır'),
                      child: const Text('Hayır'),
                    ),
                    TextButton(
                      onPressed: () {
                        _delete(services.name) ;
                          setState(() {
                            int index = serviceList.indexOf(services);
                            serviceList.removeAt(index);
                            print(i);
                            cardList.removeAt(index);
                          });
                        Navigator.pop(context, 'Evet');
                      },
                      child: const Text('Evet'),
                    ),
                  ],
                ),
                ),
                //
              highlightColor: Colors.green,
            ),
            new IconButton(
              icon: new Icon(Icons.update),
              onPressed: (){
                _displayUpdateDialog(context,services.name.toString(),i);
              },
              highlightColor: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
  Future<List<ServiceModel>> getList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<ServiceModel> _needs = [];
    var user = await FirebaseAuth.instance.currentUser;
    var uid = user.uid;
    await firestoreInstance.collection("Admins").doc(uid).collection("Services").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(ServiceModel.fromMap(values));
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    return await _needs;
  }

  createCardList()async{
    serviceList =await getList();
    for(int i=0;i< serviceList.length;++i){
      setState(() {
        serviceNameList.add(serviceList[i].name);
        cardList.add(newsCard(serviceList[i],i));
      });
    }

  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
class ServiceListPage extends StatelessWidget { //BUNU ÇAĞIR
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Hizmetler"),
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(vertical: 120),
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 60),
                child:ServiceList()
            ),
          ),
        ),
      ),
    );
  }
}