import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/model/HairdresserModel.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/register/SignUp.dart';
class HairdresserRequests extends StatefulWidget {
  @override
  _HairdresserRequestsState createState() => _HairdresserRequestsState();
}
class _HairdresserRequestsState extends State<HairdresserRequests> {
  List<Card> cardList = new List();
  bool setStateRan=false;
  _confirm(String id){
    setState(() {

      DocumentReference users = FirebaseFirestore.instance.collection('Hairdressers').doc(id);
      users.get().then((documentSnapshot) {
        final Map<String, String> someMap = {
          "acceptanceCase": "true",
        };
        documentSnapshot.reference.update(someMap);
      }).catchError((onError) {
        print("getCloudFirestoreUsers: ERROR");
        print(onError);
      });
      Fluttertoast.showToast(
          msg: "Kayıt Onaylandı!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    });
  }
  _reject(String id){
    DocumentReference users = FirebaseFirestore.instance.collection('Hairdressers').doc(id);
    users.get().then((documentSnapshot) {
      final Map<String, String> someMap = {
        "acceptanceCase": "false",
      };
      documentSnapshot.reference.update(someMap);
      print(documentSnapshot.get("acceptanceCase"));
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Fluttertoast.showToast(
        msg: "Kayıt Reddedildi!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );
  }

  Card newsCard(HairdresserModel news){
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
        //borderRadius: BorderRadius.circular(35.0),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),

        title: Text("İsim-Soyisim: " +news.name.toString()),
        subtitle: Text("Email: " + news.email.toString() + "\n" + "Telefon: " +  news.phone.toString() ),
        isThreeLine: true,
        trailing: Wrap(
          spacing: 12, // space between two icons
          children: <Widget>[
            new IconButton(
                icon: new Icon(Icons.assignment_turned_in),
                onPressed: (){
                  _confirm(news.hairdresser_id) ;
                },
                highlightColor: Colors.green,
            ),
            new IconButton(
              icon: new Icon(Icons.clear),
              onPressed: (){
                _reject(news.hairdresser_id) ;
              },
              highlightColor: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
  Future<List<HairdresserModel>> getNewsList()async{
    var user = await FirebaseAuth.instance.currentUser;
    var uid = user.uid;
    final firestoreInstance = FirebaseFirestore.instance;
    List<HairdresserModel> _needs = [];
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
          title: const Text(''),
          content: Container(
              child:Column(
                  children:[
                    CircularProgressIndicator(
                      semanticsLabel: 'Linear progress indicator',
                    )
                  ]
              ),
              height:75,
              width:75
          )
      ),
    );
    await firestoreInstance.collection("Hairdressers").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("acceptanceCase").toString()=="waiting" && value.get("admin_id").toString()==uid ) {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(HairdresserModel.fromMap(values));
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Navigator.pop(context);
    return await _needs;
  }

  createCardList()async{
    List<HairdresserModel> newsList =await getNewsList();
    for(int i=0;i< newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
    }
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
class HairdresserRequestPage extends StatelessWidget { //BUNU ÇAĞIR
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Kuaför Kayıt Talepleri"),
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(vertical: 120),
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 60),
                child:HairdresserRequests()
            ),
          ),
        ),
      ),
    );
  }
}