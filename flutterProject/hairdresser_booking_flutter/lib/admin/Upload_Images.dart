import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'package:path/path.dart';

class Upload_Images extends StatefulWidget {
  @override
  _UploadImagesState createState() => _UploadImagesState();
}

class _UploadImagesState extends State<Upload_Images> {
  File _image;
  @override
  Widget build(BuildContext context) {
    Future getImage() async {
      final picker=ImagePicker();
      var image = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        if (image != null) {
          _image= File(image.path);
          print('Image Path $_image');
        }

      });
    }
    Future uploadPic(BuildContext context) async{
      String fileName;
      if(_image!=null){
          fileName= basename(_image.path);
      }
      if(fileName==null){
        Fluttertoast.showToast(
            msg: "Lütfen resim seçiniz!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else if(fileName!=null && fileName.length==0){
        Fluttertoast.showToast(
            msg: "Lütfen resim seçiniz!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else {
        showDialog<String>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
              title: const Text(''),
              content: Container(
                  child:Column(
                      children:[
                        CircularProgressIndicator(
                          semanticsLabel: 'Linear progress indicator',
                        )
                      ]
                  ),
                  height:75,
                  width:75
              )
          ),
        );
        FirebaseStorage storage = FirebaseStorage.instance;
        Reference ref = await storage.ref().child(fileName);
        UploadTask uploadTask =  ref.putFile(_image);
        uploadTask.then((res) async{
          res.ref.getDownloadURL();
          Fluttertoast.showToast(
              msg: "Resim Başarıyla Yüklendi!" ,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 4
          );
          Navigator.pop(context);
          var user =  await FirebaseAuth.instance.currentUser;
          var uid = user.uid;
          final Map<String, String> imageMap = {
            "admin_id": uid,
            "image_name": fileName,
          };
          await FirebaseFirestore.instance.collection('Admins').doc(uid)
              .collection("Images").doc().set(imageMap);
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('Resim Başarıyla Yüklendi!')));
        });
      }

    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8), //ÖNEMLİİİİİİİİ
        leading: IconButton(
            icon: Icon(FontAwesomeIcons.arrowLeft),

            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('İşletme Resim Yükle'),
      ),
      body: Builder(
        builder: (context) =>  Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          padding: EdgeInsets.symmetric(vertical: 150),
          child: Column(

            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                        child: new SizedBox(
                          width: 300.0,
                          height: 250.0,
                          child: (_image!=null)?Image.file(
                            _image,
                            fit: BoxFit.fill,
                          ): Image.asset(('assets/camera.png'),height: 115,width: 115,
                          ),
                        ),
                      ),
                  Padding(
                    padding: EdgeInsets.only(top: 60.0),
                    child: IconButton(
                      icon: Icon(
                        FontAwesomeIcons.camera,
                        size: 30.0,
                      ),
                      onPressed: () {
                        getImage();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    color: Color(0xFF6B8BC8),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    elevation: 4.0,
                    splashColor: Colors.blue,
                    child: Text(
                      'İptal',
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),
                  RaisedButton(
                    color: Color(0xFF6B8BC8),
                    onPressed: () {
                      uploadPic(context);
                    },
                    elevation: 4.0,
                    splashColor: Colors.blue,
                    child: Text(
                      'Resmi Yükle',
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),

                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}