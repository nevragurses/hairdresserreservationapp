import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesLogin.dart';
import 'package:hairdresser_booking_flutter/admin/AddingService.dart';
import 'package:hairdresser_booking_flutter/admin/ConfirmedHairdressers.dart';
import 'package:hairdresser_booking_flutter/admin/HairdresserRequests.dart';
import 'package:hairdresser_booking_flutter/admin/RejectedHairdressers.dart';
import 'package:hairdresser_booking_flutter/admin/ServiceList.dart';
import 'package:hairdresser_booking_flutter/admin/Upload_Images.dart';

import 'ShowImages.dart';
class AdminPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState()  => _AdminPageState();
}
class _AdminPageState extends State<AdminPage> {
  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        Navigator.push(context, MaterialPageRoute(builder: (context) => TabPagesLogin_Third()));

    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("İşletme Yöneticisi Ana Sayfa"),
        actions: [
          Theme(
            data: Theme.of(context).copyWith(
              dividerColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.white),
              textTheme: TextTheme().apply(bodyColor: Colors.white),
            ),
            child: PopupMenuButton<int>(
              color: Colors.indigo,
              onSelected: (item) => onSelected(context, item),
              itemBuilder: (context) => [
                PopupMenuDivider(),
                PopupMenuItem<int>(
                  value: 0,
                  child: Row(
                    children: [
                      Icon(Icons.logout),
                      const SizedBox(width: 8),
                      Text('Çıkış'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
        body: SafeArea(
        child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/back.png"),fit: BoxFit.fill
          )
        ),
        child: Container(
        margin:  const EdgeInsets.only(top: 180),
        child: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(30),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          children: <Widget>[
          SingleChildScrollView(
            child: Container(
              color:  Color(0xFFC5D8FF),
              padding: const EdgeInsets.all(8),
                    child: Column(
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                              child: Container(
                                  child: GestureDetector(
                                    onTap: () =>
                                    {
                                      Navigator.push(context, MaterialPageRoute(
                                          builder: (context) => HairdresserRequestPage()))
                                    },
                                child: new Text(
                                  'KUAFÖR KAYIT TALEPLERİ',
                                  style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    color: Colors.redAccent,
                                  ),
                                ),
                              )
                          ),
                          ),
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                            child: Container(
                                child: GestureDetector(
                                  onTap: () =>
                                  {
                                    new CircularProgressIndicator(),
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => HairdresserRequestPage()))
                                  },
                              child: new  Image.asset(('assets/waiting.png'),height: 115,width: 115,
                              )
                                ),
                            )
                            ),
                ],
              )
            ),
            ),
            SingleChildScrollView(
            child: Container(
                color:  Color(0xFFC5D8FF),
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                        child: Container(
                            child: GestureDetector(
                              onTap: () =>
                              {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => ConfirmedHairdressersPage())),
                              },
                          child: new Text(
                            'ONAYLANAN KUAFÖRLER',
                            style: TextStyle(
                              fontStyle: FontStyle.italic,
                              color: Colors.redAccent,
                            ),
                          ),
                        )
                    ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Container(
                            child: GestureDetector(
                              onTap: () =>
                              {
                                 Navigator.push(context, MaterialPageRoute(
                                 builder: (context) => ConfirmedHairdressersPage())),

                              },
                              child: new  Image.asset(('assets/confirm.png'),height: 115,width: 115,
                          ),
                        )
                        ),
                    ),
                  ],
                )
              ),
            ),
            SingleChildScrollView(
            child: Container(
                color:  Color(0xFFC5D8FF),
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                        child: Container(
                            child: GestureDetector(
                              onTap: () =>
                              {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => RejectedHairdressersPage()))
                              },
                          child: new Text(
                            'REDDEDİLEN KUAFÖRLER',
                            style: TextStyle(
                              fontStyle: FontStyle.italic,
                              color: Colors.redAccent,
                            ),
                          ),
                        )
                    ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Container(
                            child: GestureDetector(
                              onTap: () =>
                              {
                                Navigator.push(context, MaterialPageRoute(
                                builder: (context) => RejectedHairdressersPage()))
                              },
                          child: new Image.asset(('assets/reject.png'),height: 115,width: 115,
                          ),
                            ),
                        )
                    ),
                  ],
                )
              ),
            ),
          SingleChildScrollView(
            child: Container(
                color:  Color(0xFFC5D8FF),
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => Upload_Images()))
                            },
                            child: new Text(
                              'İŞLETME RESİM YÜKLE',
                              style: TextStyle(
                                fontStyle: FontStyle.italic,
                                color: Colors.redAccent,
                              ),
                            ),
                          )
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => Upload_Images()))
                            },
                            child: new Image.asset(('assets/camera.png'),height: 115,width: 115,
                            ),
                          ),
                        )
                    ),
                  ],
                )
              ),
            ),
            SingleChildScrollView(
            child: Container(
                color:  Color(0xFFC5D8FF),
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => LoadFirbaseStorageImage()))
                            },
                            child: new Text(
                              'İŞLETME RESİMLERİ',
                              style: TextStyle(
                                fontStyle: FontStyle.italic,
                                color: Colors.redAccent,
                              ),
                            ),
                          )
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => LoadFirbaseStorageImage()))
                            },
                            child: new Image.asset(('assets/imagelist.png'),height: 115,width: 115,
                            ),
                          ),
                        )
                    ),
                  ],
                )
            ),
            ),
            SingleChildScrollView(
            child: Container(
                color:  Color(0xFFC5D8FF),
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => AddingService()))
                            },
                            child: new Text(
                              'HİZMET EKLE',
                              style: TextStyle(
                                fontStyle: FontStyle.italic,
                                color: Colors.redAccent,
                              ),
                            ),
                          )
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                               builder: (context) => AddingService()))
                            },
                            child: new Image.asset(('assets/scissor.jpg'),height: 115,width: 115,
                            ),
                          ),
                        )
                    ),
                  ],
                )
            ),
            ),
            SingleChildScrollView(
            child: Container(
                color:  Color(0xFFC5D8FF),
                padding: const EdgeInsets.all(8),

                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                      child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => ServiceListPage()))
                            },
                            child: new Text(
                              'HİZMET GÜNCELLE',
                              style: TextStyle(
                                fontStyle: FontStyle.italic,
                                color: Colors.redAccent,
                              ),
                            ),
                          )
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Container(
                          child: GestureDetector(
                            onTap: () =>
                            {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => ServiceListPage()))
                            },
                            child: new Image.asset(('assets/exchange.png'),height: 115,width: 115,
                            ),
                          ),
                        )
                    ),
                  ],
                )
            ),
            ),
              ],

            ),
        ),
          ),
        ),
        );

  }
}