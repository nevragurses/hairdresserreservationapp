import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hairdresser_booking_flutter/model/HairdresserModel.dart';
class HairdresserList extends StatefulWidget {
  String userId;
  HairdresserList(String user) {
    this.userId = user;
    print("UserId-1: $user");
  }
  @override
  _HairdresserListState createState() => _HairdresserListState(userId);
}
class _HairdresserListState extends State<HairdresserList> {
  String userId;
  _HairdresserListState(String user) {
    this.userId = user;
    print("UserId-1: $user");
  }
  List<Card> cardHairdresserList = new List();
  bool setStateRan2=false;
  Card hairdresserListCard(HairdresserModel news){
    return Card(
      shadowColor: Color(0xFF6B8BC8),  // Change this
      child: ListTile(
        title: Text("İsim-Soyisim: " +news.name.toString(),
          style: TextStyle(
            color: Colors.redAccent,
            fontWeight: FontWeight.w600,
            fontSize: 16
        ),),
        subtitle: Text("Email: " + news.email.toString() + "\n" + "Telefon: " +  news.phone.toString() ),
        isThreeLine: true,
        trailing: Wrap(
          spacing: 12, // space between two icons
        ),
      ),
    );
  }
  Future<List<HairdresserModel>> getHairdresserList()async{
    var user = await FirebaseAuth.instance.currentUser;
    var uid = userId;
    final firestoreInstance = FirebaseFirestore.instance;
    List<HairdresserModel> _needs = [];
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
          title: const Text(''),
          content: Container(
              child:Column(
                  children:[
                    CircularProgressIndicator(
                      semanticsLabel: 'Linear progress indicator',
                    )
                  ]
              ),
              height:75,
              width:75
          )
      ),
    );
    await firestoreInstance.collection("Hairdressers").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("acceptanceCase").toString()=="true" && value.get("admin_id").toString()==uid ) {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(HairdresserModel.fromMap(values));
        }
        //print(value.data());
      });
      // Navigator.pop(context);
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Navigator.pop(context);
    return await _needs;
  }
  createHairdresserCardList()async{
    List<HairdresserModel> newsList =await getHairdresserList();
    for(int i=0;i< newsList.length;++i){
      cardHairdresserList.add(hairdresserListCard(newsList[i]));
    }

    setState(() {});

  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan2==false){
      createHairdresserCardList();
      setStateRan2=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardHairdresserList,
      ),
    );
  }
}