import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
class LoadFirbaseStorageImage extends StatefulWidget {
  @override
  _LoadFirbaseStorageImageState createState() =>
      _LoadFirbaseStorageImageState();
}

class _LoadFirbaseStorageImageState extends State<LoadFirbaseStorageImage> {
  FirebaseStorage storage = FirebaseStorage.instance;
  List<String> _needs = [];


  // Retriew the uploaded images
  // This function is called when the app launches for the first time or when an image is uploaded or deleted
  Future<List<Map<String, dynamic>>> _loadingImages() async {
    List<Map<String, dynamic>> files = [];
    final ListResult result = await storage.ref().list();
    final List<Reference> allFiles = result.items;
      await Future.forEach<Reference>(allFiles, (file) async {
        var user = await FirebaseAuth.instance.currentUser;
        var uid = user.uid;
        final firestoreInstance = FirebaseFirestore.instance;
        await firestoreInstance.collection("Admins").doc(uid).collection("Images").get().then((querySnapshot) {
          querySnapshot.docs.forEach((value) {
            _needs.add(value.get("image_name"));
            //print(value.data());
          });
        }).catchError((onError) {
          print("getCloudFirestoreUsers: ERROR");
          print(onError);
        });
        /*await firestoreInstance.collection("Company_Images").get().then((querySnapshot) {
          querySnapshot.docs.forEach((value) {
            if (value.get("admin_id").toString()==uid) {
              _needs.add(value.get("image_name"));
            }
          });
          // Navigator.pop(context);
        }).catchError((onError) {
          print("getCloudFirestoreUsers: ERROR");
          print(onError);
        });*/
        if(_needs.contains(file.name).toString()=="true"){
          final String fileUrl = await file.getDownloadURL();
          final FullMetadata fileMeta = await file.getMetadata();
          files.add({
            "url": fileUrl,
            "path": file.fullPath,
          });
        }
      });
      return files;
  }
  // Delete the selected image
  // This function is called when a trash icon is pressed
  Future<void> _delete(String ref) async {
    await storage.ref(ref).delete();
    var user = await FirebaseAuth.instance.currentUser;
    var uid = user.uid;
    final firestoreInstance = FirebaseFirestore.instance;
    await firestoreInstance.collection("Admins").doc(uid).collection("Images").get().then((querySnapshot) {
      querySnapshot.docs.forEach((value) {
          if(value.get("image_name")==ref){
            value.reference.delete();
          }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    setState(() {}); //BU ANINDA GERÇEKLEŞTİRİYOR OLABİLİR!!!!!!!!
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text('İşletme Resimler'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            Expanded(
              child: FutureBuilder(
                future: _loadingImages(),
                builder: (context,
                    AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemExtent: 320.0,
                      itemBuilder: (context, index) {
                        final image = snapshot.data[index];
                        return Container(
                          height: 320,
                          child: Card(
                            child: SingleChildScrollView(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                                  child: SizedBox(
                                      height: 300.0,
                                      width: 270.0, //BUNA BAK
                                      child: Image.network(image['url'],
                                        height: 100,
                                        width: 100,),
                                  )
                              ),
                             Padding(
                              padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                              child: SizedBox(
                                height: 60.0,
                                width: 60.0,
                                 child: IconButton(
                                      onPressed: () => _delete(image['path']),
                                      icon: Icon(
                                        Icons.delete,
                                        color: Colors.red,
                                        size:30

                                      ),
                                 ),
                          ),
                            ),

                          ],
                                 ),
                          ),
                          ),
                        );
                      },
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}