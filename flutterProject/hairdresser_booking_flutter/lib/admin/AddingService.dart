import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class AddingService extends StatefulWidget {
  @override
  _AddingServiceState createState() => _AddingServiceState();
}
class _AddingServiceState extends State<AddingService> {
  String _service, _price;
  final auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    _recordDatabase(String _service, String _price) async {
      if (_service == null  ) {
        Fluttertoast.showToast(
            msg: "Lütfen hizmet giriniz.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else if (_price == null ) {
        Fluttertoast.showToast(
            msg: "Lütfen fiyat giriniz.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else if (_service !=null && _service.length==0  ) {
        Fluttertoast.showToast(
            msg: "Lütfen hizmet giriniz.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else if (_price !=null && _price.length==0  ) {
        Fluttertoast.showToast(
            msg: "Lütfen fiyat giriniz.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
      else {
        var user = await FirebaseAuth.instance.currentUser;
        var uid = user.uid;
        final Map<String, String> serviceMap = {
          "name": _service,
          "price": _price,
        };
        FirebaseFirestore.instance.collection('Admins').doc(uid)
            .collection("Services").doc(_service).set(serviceMap);

        Fluttertoast.showToast(
            msg: "Hizmet Başarıyla Eklendi!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4
        );
      }
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Hizmet Ekle"),
      ),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Hizmet',
                    prefixIcon: Icon(Icons.add),
                  ),

                  onChanged: (value) {
                    setState(() {
                      _service = value.trim();
                    });
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(hintText: 'Fiyat', prefixIcon: Icon(Icons.info),),
                  onChanged: (value) {
                    setState(() {
                      _price = value.trim();
                    });
                  },
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10),
                child: MaterialButton(
                  minWidth: 260,
                  height: 60,
                  onPressed: (){
                    _recordDatabase(_service, _price) ;
                  },
                  //Navigator.push(context, MaterialPageRoute(builder: (context)=> SignupPage()));
                  color: Color(0xFF6B8BC8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)
                  ),
                  child: Text(
                    "Hizmet Ekle",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}