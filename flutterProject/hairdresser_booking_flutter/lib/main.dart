import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hairdresser_booking_flutter/Tab/TabPagesLogin.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //GestureBinding.instance.resamplingEnabled = true;
  runApp(MaterialApp(
    localizationsDelegates: [
      GlobalMaterialLocalizations.delegate
    ],
    supportedLocales: [
      const Locale('tr'),
    ],
    debugShowCheckedModeBanner: false,
    home:TabPagesLogin(),
  ));
}