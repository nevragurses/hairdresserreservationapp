import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:adminandroid/model/AdminModel.dart';
class ConfirmedHairdressers extends StatefulWidget {
  @override
  _ConfirmedRequestsState createState() => _ConfirmedRequestsState();
}
class _ConfirmedRequestsState extends State<ConfirmedHairdressers> {
  List<Card> cardList = new List.filled(0, new Card(),growable: true);
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool setStateRan=false;
  Future<void> _displayDialog(BuildContext context,String? id) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('İşletmeyi Silme İşlemini Onaylıyor Musunuz?'),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('Hayır'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              FlatButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Evet'),
                onPressed: () {
                  setState(() {
                    _delete(id!);
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }
  Future<void> _displaySuspendDialog(BuildContext context,String? id) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('İşletmeyi Askıya Alma İşlemini Onaylıyor Musunuz?'),
            actions: <Widget>[
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text('Hayır'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              FlatButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Text('Evet'),
                onPressed: () {
                  setState(() {
                    _suspend(id!);
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }
  _delete(String id){
    FirebaseFirestore.instance.collection("Admins").doc(id).collection("Services").get().then((querySnapshot) {
      querySnapshot.docs.forEach((value) {
        value.reference.delete();
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    DocumentReference users = FirebaseFirestore.instance.collection("Admins").doc(id);
    users.delete();
    Fluttertoast.showToast(
        msg: "İşletme Silindi!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );
  }
  _suspend(String id){
    DocumentReference users = FirebaseFirestore.instance.collection("Admins").doc(id);
    users.get().then((documentSnapshot) {
      final Map<String, String> someMap = {
        "acceptanceCase": "suspended",
      };
      documentSnapshot.reference.update(someMap);
      setState(() {});
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Fluttertoast.showToast(
        msg: "İşletme Askıya Alındı!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );
  }
  Card newsCard(AdminModel news){
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),
        title: Text("İşletme İsmi: " +news.name.toString()),
        subtitle: Text("İşletme Yöneticisi İsim: " + news.name.toString() +
            "\n" + "İşletme Telefon: " +  news.phone.toString() +
            "\n" + "İşletme Email: " +  news.email.toString() +
            "\n" + "İşletme İl: " +  news.province.toString()+
            "\n" + "İşletme İlçe: " +  news.district.toString()+
            "\n" + "İşletme Cadde: " +  news.street.toString()+
            "\n" + "İşletme Mahalle: " +  news.neighborhood.toString()+
            "\n" + "İşletme Adres: " +  news.companyAddress.toString()

        ),

        isThreeLine: true,
        trailing: Wrap(
          spacing: 12, // space between two icons
          children: <Widget>[
            new MaterialButton(
              minWidth: 150,
              height: 60,
              onPressed: (){
                _displayDialog(context,news.id) ;
              },
              color: Color(0xFF6B8BC8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
              ),
              child: Text(
                "İşletmeyi Sil",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 16
                ),
              ),
            ),
            new MaterialButton(
              minWidth: 150,
              height: 60,
              onPressed: (){
                _displaySuspendDialog(context,news.id) ;
              },
              color: Color(0xFF6B8BC8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
              ),
              child: Text(
                "İşletmeyi Askıya Al",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 16
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
  Future<List<AdminModel>> getNewsList()async{
    var user = await FirebaseAuth.instance.currentUser;
    final firestoreInstance = FirebaseFirestore.instance;
    List<AdminModel> _needs = [];
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
          title: const Text(''),
          content: Container(
              child:Column(
                  children:[
                    CircularProgressIndicator(
                      semanticsLabel: 'Linear progress indicator',
                    )
                  ]
              ),
              height:75,
              width:75
          )
      ),
    );
    await firestoreInstance.collection("Admins").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("acceptanceCase").toString()=="true" ) {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(AdminModel.fromMap(values));
        }
        //print(value.data());
      });
      // Navigator.pop(context);
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Navigator.pop(context);
    return await _needs;
  }

  createCardList()async{
    List<AdminModel> newsList =await getNewsList();
    for(int i=0;i< newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
    }

    setState(() {});

  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
class ConfirmedHairdressersPage extends StatelessWidget { //BUNU ÇAĞIR
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(vertical:10),
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child:ConfirmedHairdressers()
            ),
          ),
        ),
      ),
    );
  }
}