import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:adminandroid/adminpage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'Reset_Password.dart';
import 'hairdresser_salon_requests.dart';
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}
class _LoginScreenState extends State<LoginScreen> {
  String ?_email, _password;
  final auth = FirebaseAuth.instance;

  _signin(String _email, String _password) async {
    if (_email == null) {
      Fluttertoast.showToast(
          msg: "Lütfen email adresi giriniz.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else if (_password == null) {
      Fluttertoast.showToast(
          msg: "Lütfen şifre giriniz.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4
      );
    }
    else {
      try {
        await auth.signInWithEmailAndPassword(
            email: _email, password: _password);
            if(_email == "generaladmin@gmail.com") {
              Fluttertoast.showToast(
                  msg: "Giriş Başarılı",
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 4
              );
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AdminPage()));
            }
            else{
              Fluttertoast.showToast(
                  msg: "Email adresi geçersiz!",
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 4
              );
            }
      } on FirebaseAuthException catch (error) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Email adresi geçersiz veya şifre hatalı!"),
        ));
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xFF6B8BC8),
        title: Text("Admin Paneli"),
      ),
      body: Center(
        child:Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/loginback.jpg"),fit: BoxFit.scaleDown
              )
          ),
          width: MediaQuery.of(context).size.width/3,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 60),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Admin Paneli Giriş",
                  style: TextStyle(
                      fontSize: 22,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF6B8BC8)
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric( vertical: 30),
                  child: TextField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      prefixIcon: Icon(Icons.email),
                    ),

                    onChanged: (value) {
                      setState(() {
                        _email = value.trim();
                      });
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric( vertical: 5),
                  child: TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(hintText: 'Şifre', prefixIcon: Icon(Icons.lock),),
                    onChanged: (value) {
                      setState(() {
                        _password = value.trim();
                      });
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.push(context,MaterialPageRoute(builder: (context) => Reset_Password()))
                      //Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()))
                    },
                    child: Text(
                      "Şifrenizi Mi Unuttunuz?",
                      style: TextStyle(
                          fontSize: 14,
                          color: Color(0xFF6B8BC8)
                      ),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 70, vertical: 10),
                  child: MaterialButton(
                    minWidth: 260,
                    height: 60,
                    onPressed: (){
                      _signin(_email!, _password!) ;
                    },
                    color: Color(0xFF6B8BC8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)
                    ),
                    child: Text(
                      "Giriş Yap",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 18
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}