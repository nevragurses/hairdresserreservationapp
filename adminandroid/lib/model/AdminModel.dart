class AdminModel {
  String? name;
  String? email;
  String? phone;
  String? companyName;
  String? province;
  String? district;
  String? neighborhood;
  String? street;
  String? companyAddress;
  String? id;
  String? password;

  AdminModel(
      this.name,
      this.email,
      this.phone,
      this.companyName,
      this.province,
      this.district,
      this.neighborhood,
      this.street,
      this.companyAddress,
      this.id,
      this.password
      );

  AdminModel.fromMap(Map<dynamic, dynamic> data) {
    name = data['name'];
    email = data['email'];
    phone= data['phone'];
    district = data['district'];
    companyName = data['companyName'];
    street = data['street'];
    neighborhood = data['neighborhood'];
    companyAddress = data['companyAddress'];
    province = data['province'];
    id=data['id'];
    password = data['password'];
  }
  Map<String, dynamic> toJson() => {
    'name': name,
    'email': email,
    'phone': phone,
    'address': companyAddress,
    'province': province,
    'admin_id':id
  };
}
