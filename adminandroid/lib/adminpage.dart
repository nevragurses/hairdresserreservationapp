import 'package:flutter/material.dart';
import 'package:adminandroid/ConfirmedHairdressers.dart';
import 'package:adminandroid/welcome.dart';
import 'package:swipeable_page_route/swipeable_page_route.dart';
import 'RejectedHairdressers.dart';
import 'SuspendedHairdressers.dart';
import 'hairdresser_salon_requests.dart';
class AdminPage extends StatefulWidget {
  @override
  TabPagesState createState() => TabPagesState();
}
class TabPagesState extends State<AdminPage>
    with SingleTickerProviderStateMixin {
  static const _tabCount = 4;
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabCount, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MorphingAppBar(
        backgroundColor: Color(0xFF6B8BC8) ,
        title: Text('Admin Paneli'),
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: <Widget>[
            Tab(text: 'Kuaför İşletme Kayıt Talepleri'),
            Tab(text: 'Kayıtlı Kuaför İşletmeleri'),
            Tab(text: 'Kayıt Talebi Reddedilen İşletmeler'),
            Tab(text: 'Askıya Alınmış İşletmeler'),
          ],
        ),
      ),
      body: TabBarView(
          controller: _tabController,
          children: [
            HairdresserRequestPage(),
            ConfirmedHairdressersPage(),
            RejectedHairdressersPage(),
            SuspendedHairdressersPage(),
            //LoginScreen(2),
          ]
      ),
    );
  }
}
