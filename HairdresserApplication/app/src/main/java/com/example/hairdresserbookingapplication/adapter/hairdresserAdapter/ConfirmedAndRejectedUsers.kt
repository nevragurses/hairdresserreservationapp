package com.example.hairdresserbookingapplication.adapter.hairdresserAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.ReservationModel

class ConfirmedAndRejectedUsers(private val mContext: Context, private val reservationModels: MutableList<ReservationModel?>?) : RecyclerView.Adapter<ConfirmedAndRejectedUsers.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.confirmed_and_rejected_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reservationModel = reservationModels?.get(position)
        holder.services.text = reservationModel!!.selected_services
        holder.name.text = reservationModel.name
        holder.phone.text = reservationModel.phone
        holder.mail.text = reservationModel.email
        holder.day.text = reservationModel.day
        holder.gender.text = reservationModel.gender
        holder.time.text = reservationModel.time
    }

    override fun getItemCount(): Int {
        return reservationModels!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView
        var day: TextView
        var time: TextView
        var gender: TextView
        var services: TextView

        init {
            name = itemView.findViewById(R.id.name)
            day = itemView.findViewById(R.id.day)
            time = itemView.findViewById(R.id.time)
            services = itemView.findViewById(R.id.services)
            gender = itemView.findViewById(R.id.gender)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
        }
    }

}