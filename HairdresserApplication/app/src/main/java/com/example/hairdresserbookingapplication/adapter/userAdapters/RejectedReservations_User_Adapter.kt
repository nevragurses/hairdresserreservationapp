package com.example.hairdresserbookingapplication.adapter.userAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.ReservationModel
import com.google.firebase.firestore.FirebaseFirestore

class RejectedReservations_User_Adapter(private val mContext: Context, private val reservationModels: MutableList<ReservationModel?>?) : RecyclerView.Adapter<RejectedReservations_User_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.rejected_reservations, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reservationModel = reservationModels?.get(position)
        holder.services.text = reservationModel!!.selected_services
        holder.name.text = reservationModel.hairdresser
        val mFirestore = FirebaseFirestore.getInstance()
        reservationModel.companyId?.let {
            mFirestore.collection("Admins").document(it).get().addOnCompleteListener { task ->
            holder.companyName.text = task.result!!["companyName"].toString()
            holder.phone.text = task.result!!["phone"].toString()
        }
        }
        reservationModel.hairdresser_id?.let { mFirestore.collection("Hairdressers").document(it).get().addOnCompleteListener { task -> holder.phone_hairdresser.text = task.result!!["phone"].toString() } }
        holder.day.text = reservationModel.day
        holder.time.text = reservationModel.time
    }

    override fun getItemCount(): Int {
        return reservationModels!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var companyName: TextView
        var day: TextView
        var time: TextView
        var phone_hairdresser: TextView
        var services: TextView

        init {
            name = itemView.findViewById(R.id.name)
            day = itemView.findViewById(R.id.day)
            time = itemView.findViewById(R.id.time)
            services = itemView.findViewById(R.id.services)
            phone = itemView.findViewById(R.id.phone)
            companyName = itemView.findViewById(R.id.companyName)
            phone_hairdresser = itemView.findViewById(R.id.phone_hairdresser)
        }
    }

}