package com.example.hairdresserbookingapplication.model

class SelectionModel {
    var isSelected: Boolean
    var name: String
    var id: String? = null

    constructor(isSelected: Boolean, name: String, id: String?) {
        this.isSelected = isSelected
        this.name = name
        this.id = id
    }

    constructor(isSelected: Boolean, name: String) {
        this.isSelected = isSelected
        this.name = name
    }

}