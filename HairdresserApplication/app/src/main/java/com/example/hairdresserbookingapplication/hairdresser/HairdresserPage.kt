package com.example.hairdresserbookingapplication.hairdresser

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import com.example.hairdresserbookingapplication.R
import java.util.*

class HairdresserPage : AppCompatActivity() {
    private var cardView1: CardView? = null
    private var cardView2: CardView? = null
    private var cardView3: CardView? = null
    private var cardView4: CardView? = null
    private var cardView5: CardView? = null
    private val cardView6: CardView? = null
    private val cardView7: CardView? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById<View>(R.id.actionbar) as Toolbar
        setSupportActionBar(actionbar)
        cardView1 = findViewById<View>(R.id.card1) as CardView
        cardView1!!.setOnClickListener {
            val intent = Intent(this@HairdresserPage, setCalendar::class.java)
            startActivity(intent)
        }
        cardView2 = findViewById<View>(R.id.card2) as CardView
        cardView2!!.setOnClickListener {
            val loginIntent = Intent(this@HairdresserPage, ReservationRequests::class.java)
            startActivity(loginIntent)
        }
        cardView3 = findViewById<View>(R.id.card3) as CardView
        cardView3!!.setOnClickListener {
            val loginIntent = Intent(this@HairdresserPage, ConfirmedUsers::class.java)
            startActivity(loginIntent)
        }
        cardView4 = findViewById<View>(R.id.card4) as CardView
        cardView4!!.setOnClickListener {
            val loginIntent = Intent(this@HairdresserPage, RejectedUsers::class.java)
            startActivity(loginIntent)
        }
        cardView5 = findViewById<View>(R.id.card5) as CardView
        cardView5!!.setOnClickListener {
            val loginIntent = Intent(this@HairdresserPage, AfterRejected::class.java)
            startActivity(loginIntent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hairdresser_page)
        init()
        Objects.requireNonNull(supportActionBar)?.setDisplayShowTitleEnabled(false)
    }
}