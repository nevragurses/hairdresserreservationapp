package com.example.hairdresserbookingapplication.fragment

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.util.*

class DatePickerFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_MONTH, 9) //add a day
        cal[Calendar.HOUR_OF_DAY] = 23 //set hour to last hour
        cal[Calendar.MINUTE] = 59 //set minutes to last minute
        cal[Calendar.SECOND] = 59 //set seconds to last second
        cal[Calendar.MILLISECOND] = 999 //set milliseconds to last millisecond
        val maxDate = cal.timeInMillis
        val datePickerDialog = DatePickerDialog(activity!!, activity as OnDateSetListener?, year, month, day)
        datePickerDialog.datePicker.maxDate = maxDate
        datePickerDialog.datePicker.minDate = Date().time
        return datePickerDialog
    }
}