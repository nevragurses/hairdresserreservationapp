package com.example.hairdresserbookingapplication.users

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.ReservationSteps.GenderSelection
import com.example.hairdresserbookingapplication.adapter.userAdapters.List_Services
import com.example.hairdresserbookingapplication.adapter.userAdapters.OneCompanyAdapter
import com.example.hairdresserbookingapplication.model.ServiceModel
import com.example.hairdresserbookingapplication.model.UploadModel
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.util.*

class OneCompany : AppCompatActivity() {
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: OneCompanyAdapter? = null
    private var mStorage: FirebaseStorage? = null
    private var mDatabaseRef: DatabaseReference? = null
    private var serviceModels: MutableList<ServiceModel?>? = null
    private var serviceListAdapter: List_Services? = null
    private var mDBListener: ValueEventListener? = null
    private var mUploads: MutableList<UploadModel>? = null
    private var reservation: TextView? = null
    private var company_id: String? = null
    private var companyName: TextView? = null
    private var serviceTittle: TextView? = null
    private var hairdresserTittle: TextView? = null
    private var recycler: RecyclerView? = null
    private var mFirestore: FirebaseFirestore? = null
    private var information: TextView? = null
    private var actionbar: Toolbar? = null
    private var askQuestion: TextView? = null
    private fun underlineText(textView: TextView?) {
        val content = SpannableString(textView!!.text)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        textView.text = content
    }

    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "İşletme Bilgileri"
        serviceTittle = findViewById(R.id.serviceTittle)
        hairdresserTittle = findViewById(R.id.hairdresserTittle)
        mRecyclerView = findViewById(R.id.recycler_view)
        mRecyclerView?.setHasFixedSize(true)
        companyName = findViewById(R.id.companyName)
        askQuestion = findViewById(R.id.question)
        company_id = getIntent().getStringExtra("company_id")
        reservation = findViewById(R.id.reservation)
        information = findViewById(R.id.information)
        underlineText(serviceTittle)
        underlineText(hairdresserTittle)
        underlineText(reservation)
        underlineText(information)
        information?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@OneCompany, CompanyInformation::class.java)
            intent.putExtra("companyId", company_id)
            startActivity(intent)
        })
        askQuestion?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@OneCompany, AskingQuestion::class.java)
            intent.putExtra("companyId", company_id)
            startActivity(intent)
        })
        reservation?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@OneCompany, GenderSelection::class.java)
            intent.putExtra("companyId", company_id)
            startActivity(intent)
        })
        hairdresserTittle?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@OneCompany, HairdresserList::class.java)
            intent.putExtra("companyId", company_id)
            startActivity(intent)
        })
        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        mRecyclerView?.setLayoutManager(manager)
        mUploads = ArrayList()
        mAdapter = OneCompanyAdapter(this@OneCompany, mUploads as ArrayList<UploadModel>)
        mRecyclerView?.setAdapter(mAdapter)
        mStorage = FirebaseStorage.getInstance()
        mFirestore = FirebaseFirestore.getInstance()
        mFirestore!!.collection("Admins").document(company_id!!).get().addOnSuccessListener { documentSnapshot -> companyName?.setText(documentSnapshot["companyName"].toString()) }
        recycler = findViewById(R.id.recycler)
        recycler?.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        serviceModels = ArrayList()
        readServices()
        images
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one_company)
        init()
    }

    private val images: Unit
        private get() {
            mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")
            mDBListener = mDatabaseRef!!.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    mUploads!!.clear()
                    for (postSnapshot in dataSnapshot.children) {
                        val upload: UploadModel? = postSnapshot.getValue<UploadModel>(UploadModel::class.java)
                        upload!!.key = postSnapshot.key
                        if (upload.adminKey == company_id) mUploads!!.add(upload)
                    }
                    mAdapter!!.notifyDataSetChanged()
                    registerForContextMenu(mRecyclerView)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(this@OneCompany, databaseError.message, Toast.LENGTH_SHORT).show()
                }
            })
        }

    private fun readServices() {
        mFirestore!!.collection("Admins").document(company_id!!)
                .collection("Services").get().addOnCompleteListener { task ->
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<ServiceModel>(ServiceModel::class.java)
                        serviceModels!!.add(model)
                    }
                    serviceListAdapter = List_Services(this@OneCompany, serviceModels)
                    recycler!!.adapter = serviceListAdapter
                }
    }

    override fun onDestroy() {
        super.onDestroy()
        mDatabaseRef!!.removeEventListener(mDBListener!!)
    }
}