package com.example.hairdresserbookingapplication.ReservationSteps

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CalendarView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.Common
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.reservationAdapter.TimeSlotAdapter_User
import com.example.hairdresserbookingapplication.model.TimeSlotModel
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class TimeSelection : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var times: List<String>? = null
    private var timeSlotAdapter: TimeSlotAdapter_User? = null
    private var selectedTime: String? = null
    private var selectedDate: String? = null
    private var companyId: String? = null
    private var hairdresser_id: String? = null
    private var busyTimesList: List<String>? = null
    private var timeSlotModels: MutableList<TimeSlotModel?>? = null
    private var clockSelection: TextView? = null
    //private var intent: Intent? = null
    private var back: Button? = null
    private var next: Button? = null
    private var slot: String? = null
    private fun changeLanguage() {
        val languageToLoad = "tr" // your language
        val locale = Locale(languageToLoad)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config,
                baseContext.resources.displayMetrics)
    }

    private fun init() {
        changeLanguage()
        clockSelection = findViewById(R.id.clockselection)
        busyTimesList = ArrayList()
        timeSlotModels = ArrayList()
        companyId = getIntent().getStringExtra("companyId")
        hairdresser_id = getIntent().getStringExtra("hairdresser_id")
        recyclerView = findViewById(R.id.recycler_view)
        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView?.setLayoutManager(manager)
        times = ArrayList()
        times = Common.convertTimeSlotToString()
        next = findViewById(R.id.next)
        back = findViewById(R.id.back)
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_MONTH, 9)
        cal[Calendar.HOUR_OF_DAY] = 23
        cal[Calendar.MINUTE] = 59
        cal[Calendar.SECOND] = 59
        cal[Calendar.MILLISECOND] = 999
        val maxDate = cal.timeInMillis
        val simpleCalendarView = findViewById<View>(R.id.calendari) as CalendarView
        simpleCalendarView.maxDate = maxDate
        simpleCalendarView.minDate = System.currentTimeMillis()
        simpleCalendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            selectedDate = dayOfMonth.toString() + "." + (month + 1).toString() + "." + year.toString()
            //Toast.makeText(TimeSelection.this, "gün: " + selectedDate, Toast.LENGTH_LONG).show();
            readHairdresser(selectedDate!!)
        }
        intent = getIntent()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_time_selection)
        LocalBroadcastManager.getInstance(this).registerReceiver(getTime, IntentFilter("time"))
        init()
        val gender = getIntent().getStringExtra("gender")
        back!!.setOnClickListener {
            val intent = Intent(this@TimeSelection, ServiceSelection::class.java)
            putExtra(intent, gender)
            startActivity(intent)
        }
        next!!.setOnClickListener {
            if (selectedDate == null) {
                Toast.makeText(this@TimeSelection, "Lütfen bir gün seçiniz!", Toast.LENGTH_LONG).show()
            }
            if (selectedTime == null) {
                Toast.makeText(this@TimeSelection, "Lütfen bir saat seçiniz!", Toast.LENGTH_LONG).show()
            }
            if (slot == null) {
                Toast.makeText(this@TimeSelection, "Lütfen bir saat seçiniz!", Toast.LENGTH_LONG).show()
            }
            if (selectedDate != null && selectedTime != null && slot != null) {
                val intent = Intent(this@TimeSelection, ConfirmationStep::class.java)
                putExtra(intent, gender)
                startActivity(intent)
            }
        }
    }

    private fun putExtra(intent: Intent, gender: String?) {
        if (getIntent().getStringArrayListExtra("service_list") != null) {
            intent.putStringArrayListExtra("service_list", getIntent().getStringArrayListExtra("service_list"))
        }
        if (getIntent().getStringArrayListExtra("service_indexes") != null) {
            intent.putStringArrayListExtra("service_indexes", getIntent().getStringArrayListExtra("service_indexes"))
        }
        if (getIntent().getStringExtra("hairdresser_index") != null) {
            intent.putExtra("hairdresser_index", getIntent().getStringExtra("hairdresser_index"))
        }
        if (getIntent().getStringExtra("hairdresser_id") != null) {
            intent.putExtra("hairdresser_id", getIntent().getStringExtra("hairdresser_id"))
        }
        if (getIntent().getStringExtra("hairdresser") != null) {
            intent.putExtra("hairdresser", getIntent().getStringExtra("hairdresser"))
        }
        if (selectedDate != null) {
            intent.putExtra("day", selectedDate)
        }
        if (selectedTime != null) {
            intent.putExtra("time", selectedTime)
        }
        if (companyId != null) {
            intent.putExtra("companyId", companyId)
        }
        if (gender != null) {
            intent.putExtra("gender", gender)
        }
        if (slot != null) {
            intent.putExtra("slot", slot.toString())
        }
    }

    private fun readHairdresser(selectedDate: String) {
        val firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseFirestore.collection("Hairdressers").document(hairdresser_id!!)
                .collection("FullTimes").get().addOnCompleteListener { task ->
                    timeSlotModels!!.clear()
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<TimeSlotModel>(TimeSlotModel::class.java)
                        if (model!!.date == selectedDate) {
                            timeSlotModels!!.add(model)
                        }
                    }
                    clockSelection!!.visibility = View.VISIBLE
                    timeSlotAdapter = TimeSlotAdapter_User(this@TimeSelection, times!!, hairdresser_id!!, selectedDate, timeSlotModels!!)
                    recyclerView!!.adapter = timeSlotAdapter
                }
    }

    var getTime: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val get = intent.getStringExtra("time")
            selectedTime = get.toString()
            slot = intent.getStringExtra("slot")
        }
    }
}