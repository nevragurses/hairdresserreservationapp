package com.example.hairdresserbookingapplication.users

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.CostumerModel
import com.example.hairdresserbookingapplication.users.AccountSettings
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class AccountSettings : AppCompatActivity() {
    private var accountSettingProgress: ProgressBar? = null
    private var name: EditText? = null
    private var email: EditText? = null
    private var phone: EditText? = null
    private var password: TextView? = null
    private var mFirestore: FirebaseFirestore? = null
    private var firebaseAuth: FirebaseAuth? = null
    private var user: FirebaseUser? = null
    private var docRef: DocumentReference? = null
    private var actionbar: Toolbar? = null
    private val values: Unit
        private get() {
            docRef!!.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    if (document != null && document.exists()) {
                        val model = document.toObject<CostumerModel>(CostumerModel::class.java)
                        name!!.setText(model!!.name)
                        name!!.isFocusable = false
                        email!!.setText(model.email)
                        email!!.isFocusable = false
                        phone!!.setText(model.phone)
                        phone!!.isFocusable = false
                    }
                } else {
                    Toast.makeText(this@AccountSettings, "İşlem Başarısız", Toast.LENGTH_LONG).show()
                }
            }
        }

    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Kullanıcı Profili"
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        mFirestore = FirebaseFirestore.getInstance()
        password = findViewById(R.id.password)
        firebaseAuth = FirebaseAuth.getInstance()
        accountSettingProgress = findViewById(R.id.accountsettin_progress)
        docRef = mFirestore!!.collection("Costumers").document(Objects.requireNonNull(firebaseAuth!!.uid)!!)
        user = FirebaseAuth.getInstance().currentUser
        values
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_settings)
        init()
        email!!.setOnClickListener { view -> editMail("email", view) }
        name!!.setOnClickListener { view -> edit("name", view, "isim-soyisim") }
        phone!!.setOnClickListener { view -> edit("phone", view, "telefon numarası") }
        password!!.setOnClickListener { view -> editPassword(view) }
    }

    private fun validateMail(string: String): Boolean {
        val emailPattern = Regex ("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")
        return if (!string.matches(emailPattern)) {
            false
        } else {
            email!!.error = null
            true
        }
    }

    private fun editMail(string: String, view: View) {
        accountSettingProgress!!.visibility = View.VISIBLE
        val builder = AlertDialog.Builder(this@AccountSettings)
        builder.setMessage("Yeni $string adresini giriniz.")
        val updatedValue = EditText(view.context)
        builder.setView(updatedValue)
        builder.setPositiveButton("Kaydet") { dialogInterface, i ->
            val updated = updatedValue.text.toString()
            if (updated != "") {
                if (updatedValue.text.toString().trim { it <= ' ' } == "") {
                    Toast.makeText(this@AccountSettings, "Lütfen bir $string adresi giriniz.", Toast.LENGTH_LONG).show()
                    accountSettingProgress!!.visibility = View.GONE
                }
                if (!validateMail(updatedValue.text.toString().trim { it <= ' ' })) {
                    Toast.makeText(this@AccountSettings, "Geçerli bir $string adresi giriniz.", Toast.LENGTH_LONG).show()
                }
                if (user != null && updatedValue.text.toString().trim { it <= ' ' } != "" && validateMail(updatedValue.text.toString().trim { it <= ' ' })) {
                    user!!.updateEmail(updatedValue.text.toString().trim { it <= ' ' })
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    docRef!!.get().addOnSuccessListener { documentSnapshot ->
                                        documentSnapshot.reference.update(string, updatedValue.text.toString())
                                        Toast.makeText(this@AccountSettings, "$string başarı ile güncellendi.", Toast.LENGTH_LONG).show()
                                        email!!.setText(updatedValue.text.toString())
                                        email!!.isFocusable = false
                                        accountSettingProgress!!.visibility = View.GONE
                                    }
                                } else {
                                    Toast.makeText(this@AccountSettings, "$string güncellemesi başarısız. Lütfen daha sonra tekrar deneyiniz.", Toast.LENGTH_LONG).show()
                                    accountSettingProgress!!.visibility = View.GONE
                                }
                            }
                }
            } else {
                Toast.makeText(this@AccountSettings, "Alan Boş Bırakılamaz! Güncelleme Başarısız.", Toast.LENGTH_LONG).show()
            }
        }
        builder.setNegativeButton("İptal") { dialogInterface, i -> }
        builder.show()
    }

    private fun edit(string: String, view: View, call: String) {
        accountSettingProgress!!.visibility = View.VISIBLE
        val builder = AlertDialog.Builder(this@AccountSettings)
        builder.setMessage("Yeni $call giriniz.")
        val updatedValue = EditText(view.context)
        builder.setView(updatedValue)
        builder.setPositiveButton("Kaydet") { dialogInterface, i ->
            val updated = updatedValue.text.toString()
            if (updated != "") {
                if (updatedValue.text.toString().trim { it <= ' ' } == "") {
                    Toast.makeText(this@AccountSettings, "Lütfen bir $call giriniz.", Toast.LENGTH_LONG).show()
                    accountSettingProgress!!.visibility = View.GONE
                }
                if (user != null && updatedValue.text.toString().trim { it <= ' ' } != "") {
                    docRef!!.get().addOnSuccessListener { documentSnapshot ->
                        documentSnapshot.reference.update(string, updatedValue.text.toString())
                        Toast.makeText(this@AccountSettings, "$call başarı ile güncellendi.", Toast.LENGTH_LONG).show()
                        values
                        accountSettingProgress!!.visibility = View.GONE
                    }
                }
            }
        }
        builder.setNegativeButton("İptal") { dialogInterface, i -> }
        builder.show()
    }

    private fun validatePass(`val`: String): Boolean {
        return if (`val`.length < 8) {
            false
        } else {
            true
        }
    }

    private fun editPassword(view: View) {
        accountSettingProgress!!.visibility = View.VISIBLE
        val builder = AlertDialog.Builder(this@AccountSettings)
        builder.setMessage("Yeni şifrenizi giriniz.")
        val updatedValue = EditText(view.context)
        builder.setView(updatedValue)
        builder.setPositiveButton("Kaydet") { dialogInterface, i ->
            val updated = updatedValue.text.toString()
            if (updated != "") {
                if (updatedValue.text.toString().trim { it <= ' ' } == "") {
                    Toast.makeText(this@AccountSettings, "Lütfen bir şifre giriniz.", Toast.LENGTH_LONG).show()
                    accountSettingProgress!!.visibility = View.GONE
                }
                if (!validatePass(updatedValue.text.toString().trim { it <= ' ' })) {
                    Toast.makeText(this@AccountSettings, "Parolanızın uzunluğu en az 8 karakter olmalıdır.", Toast.LENGTH_LONG).show()
                }
                if (user != null && updatedValue.text.toString().trim { it <= ' ' } != "" && validatePass(updatedValue.text.toString().trim { it <= ' ' })) {
                    user!!.updatePassword(updatedValue.text.toString().trim { it <= ' ' })
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Toast.makeText(this@AccountSettings, "Şifre başarı ile güncellendi.", Toast.LENGTH_LONG).show()
                                    accountSettingProgress!!.visibility = View.GONE
                                } else {
                                    Toast.makeText(this@AccountSettings, "Şifre güncellemesi başarısız. Lütfen daha sonra tekrar deneyiniz.", Toast.LENGTH_LONG).show()
                                    accountSettingProgress!!.visibility = View.GONE
                                }
                            }
                }
            } else {
                Toast.makeText(this@AccountSettings, "Alan Boş Bırakılamaz! Güncelleme Başarısız.", Toast.LENGTH_LONG).show()
            }
        }
        builder.setNegativeButton("İptal") { dialogInterface, i -> }
        builder.show()
    }
}