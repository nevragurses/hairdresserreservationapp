package com.example.hairdresserbookingapplication.users

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R

class SeeQuestion_User : AppCompatActivity() {
    private var question_text: TextView? = null
    private var question: String? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Gönderilen Soru"
        question = intent.getStringExtra("question")
        question_text = findViewById(R.id.question_text)
        question_text!!.setText(question)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_question__user)
        init()
    }
}