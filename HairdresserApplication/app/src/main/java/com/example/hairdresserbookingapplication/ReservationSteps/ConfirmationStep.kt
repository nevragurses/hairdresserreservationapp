package com.example.hairdresserbookingapplication.ReservationSteps

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.ReservationSteps.ConfirmationStep

class ConfirmationStep : AppCompatActivity() {
    var day: TextView? = null
    var time: TextView? = null
    var hairdresser: TextView? = null
    var service: TextView? = null
    var gender: TextView? = null
    var back: Button? = null
    var next: Button? = null
    private fun init() {
        gender = findViewById(R.id.gender_selection)
        hairdresser = findViewById(R.id.hairdresser_selection)
        service = findViewById(R.id.service_selection)
        day = findViewById(R.id.day_selection)
        time = findViewById(R.id.time_selection)
        back = findViewById(R.id.back)
        next = findViewById(R.id.next)
        back?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@ConfirmationStep, TimeSelection::class.java)
            putExtra(intent)
            startActivity(intent)
        })
        next?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@ConfirmationStep, LastStep::class.java)
            putExtra(intent)
            startActivity(intent)
        })
        getExtras(intent)
    }

    private fun putExtra(intent: Intent) {
        intent.putStringArrayListExtra("service_list", getIntent().getStringArrayListExtra("service_list"))
        intent.putStringArrayListExtra("service_indexes", getIntent().getStringArrayListExtra("service_indexes"))
        intent.putExtra("hairdresser_index", getIntent().getStringExtra("hairdresser_index"))
        intent.putExtra("hairdresser_id", getIntent().getStringExtra("hairdresser_id"))
        intent.putExtra("hairdresser", getIntent().getStringExtra("hairdresser"))
        intent.putExtra("day", getIntent().getStringExtra("day"))
        intent.putExtra("time", getIntent().getStringExtra("time"))
        intent.putExtra("companyId", getIntent().getStringExtra("companyId"))
        intent.putExtra("gender", getIntent().getStringExtra("gender"))
        intent.putExtra("slot", getIntent().getStringExtra("slot"))
    }

    private fun getExtras(intent: Intent) {
        if (intent.getStringExtra("gender") != null) {
            gender!!.text = intent.getStringExtra("gender")
        }
        if (getIntent().getStringArrayListExtra("service_list") != null) {
            service!!.text = intent.getStringArrayListExtra("service_list").toString()
        }
        if (getIntent().getStringExtra("hairdresser") != null) {
            hairdresser!!.text = intent.getStringExtra("hairdresser")
        }
        if (getIntent().getStringExtra("day") != null) {
            day!!.text = intent.getStringExtra("day")
        }
        if (getIntent().getStringExtra("time") != null) {
            time!!.text = intent.getStringExtra("time")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation_step)
        init()
    }
}