package com.example.hairdresserbookingapplication.model

class TimeSlotModel {
    var time: String? = null
    var date: String? = null
    var slot: String? = null
    var acceptanceCase: String? = null

    constructor() {}
    constructor(time: String?, date: String?, slot: String?, acceptanceCase: String?) {
        this.time = time
        this.date = date
        this.slot = slot
        this.acceptanceCase = acceptanceCase
    }

}