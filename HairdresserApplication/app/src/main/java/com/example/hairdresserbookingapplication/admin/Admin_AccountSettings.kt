package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.AdminModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

class Admin_AccountSettings : AppCompatActivity() {
    private var name: EditText? = null
    private var email: EditText? = null
    private var phone: EditText? = null
    private var password: TextView? = null
    private var addr: EditText? = null
    private var district: EditText? = null
    private var province: EditText? = null
    private var neighborhood: EditText? = null
    private var street: EditText? = null
    private var kind: EditText? = null
    private var companyName: EditText? = null
    private var mFirestore: FirebaseFirestore? = null
    private var firebaseAuth: FirebaseAuth? = null
    private var user: FirebaseUser? = null
    private var docRef: DocumentReference? = null
    private var actionbar: Toolbar? = null
    private val values: Unit
        private get() {
            docRef!!.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    if (document != null && document.exists()) {
                        val model = document.toObject<AdminModel>(AdminModel::class.java)
                        name!!.setText(model!!.name)
                        email!!.setText(model.email)
                        phone!!.setText(model.phone)
                        companyName!!.setText(model.companyName)
                        addr!!.setText(model.companyAddress)
                        province!!.setText(model.province)
                        district!!.setText(model.district)
                        neighborhood!!.setText(model.neighborhood)
                        kind!!.setText(model.type)
                        street!!.setText(model.street)
                    }
                } else {
                    Toast.makeText(this@Admin_AccountSettings, "İşlem Başarısız", Toast.LENGTH_LONG).show()
                }
            }
        }

    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Admin Profili"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        addr = findViewById(R.id.addr)
        province = findViewById(R.id.province)
        street = findViewById(R.id.street)
        neighborhood = findViewById(R.id.neighborhood)
        companyName = findViewById(R.id.companyName)
        district = findViewById(R.id.district)
        kind = findViewById(R.id.kind)
        mFirestore = FirebaseFirestore.getInstance()
        password = findViewById(R.id.password)
        firebaseAuth = FirebaseAuth.getInstance()
        docRef = firebaseAuth!!.uid?.let { mFirestore!!.collection("Admins").document(it) }
        user = FirebaseAuth.getInstance().currentUser
        values
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin__account_settings)
        init()
        email!!.setOnClickListener { view -> editMail("email", view) }
        name!!.setOnClickListener { view -> edit("name", view, "isim-soyisim") }
        phone!!.setOnClickListener { view -> edit("phone", view, "telefon numarası") }
        password!!.setOnClickListener { view -> editPassword(view) }
        addr!!.setOnClickListener { view -> edit("companyAddress", view, "işletme adresi") }
        neighborhood!!.setOnClickListener { view -> edit("neighborhood", view, "mahalle") }
        street!!.setOnClickListener { view -> edit("street", view, "cadde") }
        district!!.setOnClickListener { view -> edit("district", view, "ilçe") }
        province!!.setOnClickListener { view -> edit("province", view, "il") }
        kind!!.setOnClickListener { view -> edit("type", view, "işletme türü (Kadın / Erkek / Kadın-Erkek) ") }
        companyName!!.setOnClickListener { view -> edit("companyName", view, "işletme ismi ") }
    }


    private fun validateMail(trim: String): Boolean {
        val `val` = email!!.text.toString()
        val emailPattern = Regex ("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")
        return if (`val`.isEmpty()) {
            email!!.error = "Lütfen e-posta adresinizi giriniz."
            false
        } else if (!`val`.matches(emailPattern)) {
            email!!.error = "E-posta adresiniz geçersizdir. Lütfen geçerli bir e-posta adresi giriniz."
            false
        } else {
            email!!.error = null
            true
        }
    }

    private fun editMail(string: String, view: View) {
        val builder = AlertDialog.Builder(this@Admin_AccountSettings)
        builder.setMessage("Yeni $string adresini giriniz.")
        val updatedValue = EditText(view.context)
        builder.setView(updatedValue)
        builder.setPositiveButton("Kaydet") { dialogInterface, i ->
            val updated = updatedValue.text.toString()
            if (updated != "") {
                if (updatedValue.text.toString().trim { it <= ' ' } == "") {
                    Toast.makeText(this@Admin_AccountSettings, "Lütfen bir $string adresi giriniz.", Toast.LENGTH_LONG).show()
                }
                if (!validateMail(updatedValue.text.toString().trim { it <= ' ' })) {
                    Toast.makeText(this@Admin_AccountSettings, "Geçerli bir $string adresi giriniz.", Toast.LENGTH_LONG).show()
                }
                if (user != null && updatedValue.text.toString().trim { it <= ' ' } != "" && validateMail(updatedValue.text.toString().trim { it <= ' ' })) {
                    user!!.updateEmail(updatedValue.text.toString().trim { it <= ' ' })
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    docRef!!.get().addOnSuccessListener { documentSnapshot ->
                                        documentSnapshot.reference.update(string, updatedValue.text.toString())
                                        Toast.makeText(this@Admin_AccountSettings, "$string başarı ile güncellendi.", Toast.LENGTH_LONG).show()
                                        email!!.setText(updatedValue.text.toString())
                                        email!!.isFocusable = false
                                    }
                                } else {
                                    Toast.makeText(this@Admin_AccountSettings, "$string güncellemesi başarısız. Lütfen daha sonra tekrar deneyiniz.", Toast.LENGTH_LONG).show()
                                }
                            }
                }
            } else {
                Toast.makeText(this@Admin_AccountSettings, "Alan Boş Bırakılamaz! Güncelleme Başarısız.", Toast.LENGTH_LONG).show()
            }
        }
        builder.setNegativeButton("İptal") { dialogInterface, i -> }
        builder.show()
    }

    private fun edit(string: String, view: View, call: String) {
        val builder = AlertDialog.Builder(this@Admin_AccountSettings)
        builder.setMessage("Yeni $call giriniz.")
        val updatedValue = EditText(view.context)
        builder.setView(updatedValue)
        builder.setPositiveButton("Kaydet") { dialogInterface, i ->
            val updated = updatedValue.text.toString()
            if (updated != "") {
                if (updatedValue.text.toString().trim { it <= ' ' } == "") {
                    Toast.makeText(this@Admin_AccountSettings, "Lütfen bir $call giriniz.", Toast.LENGTH_LONG).show()
                }
                if (user != null && updatedValue.text.toString().trim { it <= ' ' } != "") {
                    docRef!!.get().addOnSuccessListener { documentSnapshot ->
                        documentSnapshot.reference.update(string, updatedValue.text.toString())
                        Toast.makeText(this@Admin_AccountSettings, "$call başarı ile güncellendi.", Toast.LENGTH_LONG).show()
                        values
                    }
                }
            }
        }
        builder.setNegativeButton("İptal") { dialogInterface, i -> }
        builder.show()
    }

    private fun validatePass(trim: String): Boolean {
        val `val` = password!!.text.toString()
        return if (`val`.isEmpty()) {
            password!!.error = "Lütfen parolanızı giriniz."
            false
        } else if (`val`.length < 8) {
            password!!.error = "Parolanızın uzunluğu en az 8 karakter olmalıdır."
            false
        } else {
            password!!.error = null
            true
        }
    }
    private fun editPassword(view: View) {
        val builder = AlertDialog.Builder(this@Admin_AccountSettings)
        builder.setMessage("Yeni şifrenizi giriniz.")
        val updatedValue = EditText(view.context)
        builder.setView(updatedValue)
        builder.setPositiveButton("Kaydet") { dialogInterface, i ->
            val updated = updatedValue.text.toString()
            if (updated != "") {
                if (updatedValue.text.toString().trim { it <= ' ' } == "") {
                    Toast.makeText(this@Admin_AccountSettings, "Lütfen bir şifre giriniz.", Toast.LENGTH_LONG).show()
                }
                if (!validatePass(updatedValue.text.toString().trim { it <= ' ' })) {
                    Toast.makeText(this@Admin_AccountSettings, "Parolanızın uzunluğu en az 8 karakter olmalıdır.", Toast.LENGTH_LONG).show()
                }
                if (user != null && updatedValue.text.toString().trim { it <= ' ' } != "" && validatePass(updatedValue.text.toString().trim { it <= ' ' })) {
                    user!!.updatePassword(updatedValue.text.toString().trim { it <= ' ' })
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Toast.makeText(this@Admin_AccountSettings, "Şifre başarı ile güncellendi.", Toast.LENGTH_LONG).show()
                                } else {
                                    Toast.makeText(this@Admin_AccountSettings, "Şifre güncellemesi başarısız. Lütfen daha sonra tekrar deneyiniz.", Toast.LENGTH_LONG).show()
                                }
                            }
                }
            } else {
                Toast.makeText(this@Admin_AccountSettings, "Alan Boş Bırakılamaz! Güncelleme Başarısız.", Toast.LENGTH_LONG).show()
            }
        }
        builder.setNegativeButton("İptal") { dialogInterface, i -> }
        builder.show()
    }
}