package com.example.hairdresserbookingapplication.fragment.adminFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.fragment.RegisterFragment_Common
import com.example.hairdresserbookingapplication.login.Login
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class AdminRegisterFragment : RegisterFragment_Common() {
    private var companyName: EditText? = null
    private var companyAddr: EditText? = null
    private var province: EditText? = null
    private var street: EditText? = null
    private var neighborhood: EditText? = null
    private var district: EditText? = null
    private var radioGroup: RadioGroup? = null
    private var radioButton: RadioButton? = null
    private fun init(view: View) {
        auth = FirebaseAuth.getInstance()
        val mFirebaseUser = auth?.currentUser
        if (mFirebaseUser != null) {
            userId = mFirebaseUser.uid
        }
        mFirestore = FirebaseFirestore.getInstance()
        alreadyRegister = view.findViewById(R.id.alreadyRegister)
        name = view.findViewById(R.id.nameProf)
        phone = view.findViewById(R.id.phoneProf)
        email = view.findViewById(R.id.emailProf)
        password = view.findViewById(R.id.passwordProf)
        radioGroup = view.findViewById(R.id.radioGroup)
        register = view.findViewById(R.id.register_button)
        login = view.findViewById(R.id.login)
        companyAddr = view.findViewById(R.id.companyAddr)
        companyName = view.findViewById(R.id.company)
        street = view.findViewById(R.id.street)
        district = view.findViewById(R.id.district)
        neighborhood = view.findViewById(R.id.neighborhood)
        province = view.findViewById(R.id.province)
        register!!.setOnClickListener {
            val selectedId = radioGroup!!.getCheckedRadioButtonId()
            radioButton = activity!!.findViewById(selectedId)
            createNewAccount()
        }
        alreadyRegister!!.setOnClickListener {
            val intent = Intent(thisContext, Login::class.java)
            intent.putExtra("FRAGMENT_ID", "2")
            startActivity(intent)
        }
        login?.setOnClickListener {
            val intent = Intent(thisContext, Login::class.java)
            intent.putExtra("FRAGMENT_ID", "2")
            startActivity(intent)
        }
    }

    private fun validateCompanyName(): Boolean {
        val `val` = companyName!!.text.toString()
        return if (`val`.isEmpty()) {
            companyName!!.error = "Lütfen işletme adını giriniz"
            false
        } else {
            companyName!!.error = null
            true
        }
    }

    private fun validateAddr(): Boolean {
        val `val` = companyAddr!!.text.toString()
        return if (`val`.isEmpty()) {
            companyAddr!!.error = "Lütfen işletme adresini giriniz"
            false
        } else {
            companyAddr!!.error = null
            true
        }
    }

    private fun validateDistrict(): Boolean {
        val `val` = district!!.text.toString()
        return if (`val`.isEmpty()) {
            district!!.error = "Lütfen işletme ilçesini giriniz"
            false
        } else {
            district!!.error = null
            true
        }
    }

    private fun validateStreet(): Boolean {
        val `val` = street!!.text.toString()
        return if (`val`.isEmpty()) {
            street!!.error = "Lütfen işletme caddesini giriniz"
            false
        } else {
            street!!.error = null
            true
        }
    }

    private fun validateProvince(): Boolean {
        val `val` = province!!.text.toString()
        return if (`val`.isEmpty()) {
            province!!.error = "Lütfen işletme ilini giriniz"
            false
        } else {
            province!!.error = null
            true
        }
    }

    private fun validateNeighborhood(): Boolean {
        val `val` = neighborhood!!.text.toString()
        return if (`val`.isEmpty()) {
            neighborhood!!.error = "Lütfen işletme mahallesini giriniz"
            false
        } else {
            neighborhood!!.error = null
            true
        }
    }

    private fun createNewAccount() {
        if (!validateName() || !validateMail() || !validatePass() || !validatePhone()
                || !validateCompanyName() || !validateAddr() || !validateProvince()
                || !validateDistrict() || !validateNeighborhood() || !validateStreet()) {
            return
        }
        val userName = name!!.text.toString()
        val e_mail = email!!.text.toString()
        val pass = password!!.text.toString()
        val phoneNum = phone!!.text.toString()
        val company_addr = companyAddr!!.text.toString()
        val company_name = companyName!!.text.toString()
        val company_province = province!!.text.toString()
        val company_district = district!!.text.toString()
        val company_neighborhood = neighborhood!!.text.toString()
        val company_street = street!!.text.toString()
        auth!!.createUserWithEmailAndPassword(e_mail, pass).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                //sendVerificationLink(); ONEMLİİİİİİİİİİİİİİİİİ
                userId = auth!!.currentUser!!.uid
                val user = HashMap<String, String?>()
                user["id"] = auth!!.uid
                user["name"] = userName
                user["email"] = e_mail
                user["phone"] = phoneNum
                user["companyName"] = company_name
                user["companyAddress"] = company_addr
                user["province"] = company_province
                user["district"] = company_district
                user["street"] = company_street
                user["neighborhood"] = company_neighborhood
                user["acceptanceCase"] = "false"
                user["type"] = radioButton!!.text.toString()
                mFirestore!!.collection("Admins").document(userId!!).set(user).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val loginIntent = Intent(thisContext, Login::class.java)
                        loginIntent.putExtra("FRAGMENT_ID", "2")
                        //Toast.makeText(thisContext,"Kayıt başarı ile yapıldı.",Toast.LENGTH_LONG).show();
                        startActivity(loginIntent)
                    } else {
                        Toast.makeText(thisContext, "Kayıt yapılırken problem oluştu.", Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                Toast.makeText(thisContext, "Bu e-posta adresi ile kayıtlı kullanıcı zaten var!", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        thisContext = container!!.context
        val view = inflater.inflate(R.layout.fragment_admin_register, container, false)
        radioGroup = view.findViewById(R.id.radioGroup)
        init(view)
        return view
    }
}