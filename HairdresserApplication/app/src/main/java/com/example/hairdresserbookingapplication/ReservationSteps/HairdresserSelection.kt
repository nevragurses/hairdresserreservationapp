package com.example.hairdresserbookingapplication.ReservationSteps

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.ReservationSteps.HairdresserSelection
import com.example.hairdresserbookingapplication.adapter.reservationAdapter.HairdresserSelectionAdapter
import com.example.hairdresserbookingapplication.model.HairdresserModel
import com.example.hairdresserbookingapplication.model.SelectionModel
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class HairdresserSelection : AppCompatActivity() {
    private var firebaseFirestore: FirebaseFirestore? = null
    private var companyId: String? = null
    var preSelectedIndex = -1
    private var hairdresserModels: MutableList<HairdresserModel?>? = null
    private var recyclerView: ListView? = null
    private var back: Button? = null
    private var next: Button? = null
    private var selectedHairdresser: String? = null
    private var selectedHairdresserId: String? = null
    private var startIndex: String? = null
    private var start = -10
    private fun init() {
        firebaseFirestore = FirebaseFirestore.getInstance()
        recyclerView = findViewById(R.id.recycler)
        next = findViewById(R.id.next)
        back = findViewById(R.id.back)
        hairdresserModels = ArrayList()
        val intent = intent
        companyId = intent.getStringExtra("companyId")
        val getIndex = intent.getStringExtra("hairdresser_index")
        if (getIndex != null) {
            start = getIndex.toInt()
        }
        val gender = intent.getStringExtra("gender")
        back?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@HairdresserSelection, GenderSelection::class.java)
            putExtra(intent, gender)
            startActivity(intent)
        })
        next?.setOnClickListener(View.OnClickListener {
            if (selectedHairdresser == null) {
                Toast.makeText(this@HairdresserSelection, "Lütfen bir kuaför seçiniz!", Toast.LENGTH_LONG).show()
            } else {
                val intent = Intent(this@HairdresserSelection, ServiceSelection::class.java)
                putExtra(intent, gender)
                startActivity(intent)
            }
        })
        findHairdressers()
    }

    private fun putExtra(intent: Intent, gender: String?) {
        if (getIntent().getStringArrayListExtra("service_list") != null) {
            intent.putStringArrayListExtra("service_list", getIntent().getStringArrayListExtra("service_list"))
        }
        if (getIntent().getStringArrayListExtra("service_indexes") != null) {
            intent.putStringArrayListExtra("service_indexes", getIntent().getStringArrayListExtra("service_indexes"))
        }
        if (selectedHairdresser != null) intent.putExtra("hairdresser", selectedHairdresser)
        if (selectedHairdresserId != null) intent.putExtra("hairdresser_id", selectedHairdresserId)
        if (startIndex != null) intent.putExtra("hairdresser_index", startIndex)
        if (gender != null) intent.putExtra("gender", gender)
        if (companyId != null) intent.putExtra("companyId", companyId)
    }

    private fun findHairdressers() {
        firebaseFirestore!!.collection("Hairdressers").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<HairdresserModel>(HairdresserModel::class.java)
                if (model!!.adminId == companyId && model!!.acceptanceCase == "true") {
                    hairdresserModels!!.add(model)
                }
            }
            hairdresserAdapter
        }
    }

    //onceden kayıt kontroluuu
    private val hairdresserAdapter: Unit
        private get() {
            val hairdresser: MutableList<SelectionModel> = ArrayList()
            for (i in hairdresserModels!!.indices) {
                hairdresser.add(SelectionModel(false, hairdresserModels!![i]!!.name!!, hairdresserModels!![i]!!.id))
            }
            val adapter = HairdresserSelectionAdapter(this@HairdresserSelection, hairdresser)
            recyclerView!!.adapter = adapter
            //onceden kayıt kontroluuu
            if (start != -10) {
                choiceInitialize(hairdresser, start)
            }
            recyclerView!!.onItemClickListener = OnItemClickListener { adapterView, view, i, l ->
                choiceInitialize(hairdresser, i)
                adapter.updateRecords(hairdresser)
            }
        }

    private fun choiceInitialize(hairdresser: MutableList<SelectionModel>, i: Int) {
        val model = hairdresser[i]
        hairdresser[i] = model
        if (preSelectedIndex > -1) {
            val preRecord = hairdresser[preSelectedIndex]
            preRecord.isSelected = false
            hairdresser[preSelectedIndex] = preRecord
        }
        preSelectedIndex = i
        model.isSelected = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hairdresser_selection)
        init()
        LocalBroadcastManager.getInstance(this).registerReceiver(getHairdresser, IntentFilter("hairdresser"))
    }

    var getHairdresser: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val get = intent.getStringExtra("name")
            selectedHairdresserId = intent.getStringExtra("hairdresser_id")
            selectedHairdresser = get
            startIndex = intent.getStringExtra("hairdresser_index")
        }
    }
}