package com.example.hairdresserbookingapplication.model

class QuestionModel {
    var companyId: String? = null
    var name: String? = null
    var email: String? = null
    var phone: String? = null
    var user_id: String? = null
    var answer: String? = null
    var id: String? = null
    var question: String? = null
    var question_removed: String? = null
    var question_removed_user: String? = null

    constructor() {}

    constructor(companyId: String?, name: String?, email: String?,
                phone: String?, user_id: String?, answer: String?, id: String?) {
        this.companyId = companyId
        this.name = name
        this.email = email
        this.phone = phone
        this.user_id = user_id
        this.answer = answer
        this.id = id
    }

}