package com.example.hairdresserbookingapplication.users

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.userAdapters.HairdresserInformations
import com.example.hairdresserbookingapplication.adapter.userAdapters.OneCompanyAdapter
import com.example.hairdresserbookingapplication.model.HairdresserModel
import com.example.hairdresserbookingapplication.model.UploadModel
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.util.*

class HairdresserList : AppCompatActivity() {
    private var recycler: RecyclerView? = null
    private var firebaseFirestore: FirebaseFirestore? = null
    private var hairdresserModels: MutableList<HairdresserModel?>? = null
    private var mRecyclerView: RecyclerView? = null
    private var hairdresserAdapter: HairdresserInformations? = null
    private var mAdapter: OneCompanyAdapter? = null
    private var companyId: String? = null
    private var mStorage: FirebaseStorage? = null
    private var mDBListener: ValueEventListener? = null
    private var mUploads: MutableList<UploadModel>? = null
    private var companyName: TextView? = null
    private var mDatabaseRef: DatabaseReference? = null
    private var mFirestore: FirebaseFirestore? = null
    private var serviceTittle: TextView? = null
    private var hairdresserTittle: TextView? = null
    private var actionbar: Toolbar? = null
    private fun underlineText() {
        val content = SpannableString(serviceTittle!!.text)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        val content2 = SpannableString(hairdresserTittle!!.text)
        content2.setSpan(UnderlineSpan(), 0, content.length, 0)
        serviceTittle!!.text = content
        hairdresserTittle!!.text = content2
    }

    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Kuaförler"
        serviceTittle = findViewById(R.id.serviceTittle)
        hairdresserTittle = findViewById(R.id.hairdresserTittle)
        underlineText()
        mRecyclerView = findViewById(R.id.recycler_view)
        mRecyclerView?.setHasFixedSize(true)
        companyName = findViewById(R.id.companyName)
        recycler = findViewById(R.id.recycler)
        companyId = getIntent().getStringExtra("companyId")
        serviceTittle?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@HairdresserList, OneCompany::class.java)
            intent.putExtra("company_id", companyId)
            startActivity(intent)
        })
        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        mRecyclerView?.setLayoutManager(manager)
        mUploads = ArrayList()
        mAdapter = OneCompanyAdapter(this@HairdresserList, mUploads as ArrayList<UploadModel>)
        mRecyclerView?.setAdapter(mAdapter)
        mStorage = FirebaseStorage.getInstance()
        mFirestore = FirebaseFirestore.getInstance()
        mFirestore!!.collection("Admins").document(companyId!!).get().addOnSuccessListener { documentSnapshot -> companyName?.setText(documentSnapshot["companyName"].toString()) }
        recycler?.setHasFixedSize(true)
        firebaseFirestore = FirebaseFirestore.getInstance()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        hairdresserModels = ArrayList()
        readServices()
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")
        images
    }

    private val images: Unit
        private get() {
            mDBListener = mDatabaseRef!!.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    mUploads!!.clear()
                    for (postSnapshot in dataSnapshot.children) {
                        val upload: UploadModel? = postSnapshot.getValue<UploadModel>(UploadModel::class.java)
                        upload?.key = postSnapshot.key
                        if (upload?.adminKey == companyId) upload?.let { mUploads!!.add(it) }
                    }
                    mAdapter!!.notifyDataSetChanged()
                    registerForContextMenu(mRecyclerView)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(this@HairdresserList, databaseError.message, Toast.LENGTH_SHORT).show()
                }
            })
        }

    private fun readServices() {
        firebaseFirestore!!.collection("Hairdressers").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<HairdresserModel>(HairdresserModel::class.java)
                if (model!!.adminId == companyId && model!!.acceptanceCase == "true") hairdresserModels!!.add(model)
            }
            hairdresserAdapter = HairdresserInformations(this@HairdresserList, hairdresserModels)
            recycler!!.adapter = hairdresserAdapter
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hairdresser_list)
        init()
    }
}