package com.example.hairdresserbookingapplication.adapter.userAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.UploadModel
import com.squareup.picasso.Picasso

class OneCompanyAdapter(private val mContext: Context, private val mUploads: List<UploadModel>) : RecyclerView.Adapter<OneCompanyAdapter.ImageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.one_company_item, parent, false)
        return ImageViewHolder(v)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val uploadCurrent = mUploads[position]
        Picasso.with(mContext)
                .load(uploadCurrent.imageUrl)
                .placeholder(R.drawable.hairdresser3)
                .fit()
                .centerCrop()
                .into(holder.imageView)
    }

    override fun getItemCount(): Int {
        return mUploads.size
    }

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView

        init {
            imageView = itemView.findViewById(R.id.image_view_upload)
        }
    }

}