package com.example.hairdresserbookingapplication.adapter.adminAdapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.admin.ConfirmedHairdressers
import com.example.hairdresserbookingapplication.admin.RejectedHairdressers
import com.example.hairdresserbookingapplication.model.HairdresserModel
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class HairdresserConfirmation_Adapter(private val mContext: Context, private val hairdresserModels: MutableList<HairdresserModel?>?) : RecyclerView.Adapter<HairdresserConfirmation_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.hairdresser_confirm_and_reject, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val appointmentModel = hairdresserModels?.get(position)
        holder.name.text = appointmentModel!!.name
        holder.phone.text = appointmentModel.phone
        holder.mail.text = appointmentModel.email
        holder.button.setOnClickListener { appointmentModel.id?.let { it1 -> changeStore(it1) } }
        holder.noconfirm.setOnClickListener { appointmentModel.id?.let { it1 -> makeReject(it1) } }
    }

    private fun changeStore(id: String) {
        val mFirestore = FirebaseFirestore.getInstance()
        mFirestore.collection("Hairdressers").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<HairdresserModel>(HairdresserModel::class.java)
                if (model!!.id == id && model!!.acceptanceCase == "false") {
                    Toast.makeText(mContext, "Onaylama İşlemi Gerçekleşti", Toast.LENGTH_LONG).show()
                    document.reference.update("acceptanceCase", "true")
                    val intent = Intent(mContext, ConfirmedHairdressers::class.java)
                    mContext.startActivity(intent)
                }
            }
        }
    }

    private fun makeReject(id: String) {
        val mFirestore = FirebaseFirestore.getInstance()
        mFirestore.collection("Hairdressers").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<HairdresserModel>(HairdresserModel::class.java)
                if (model!!.id == id && model!!.acceptanceCase == "false") {
                    Toast.makeText(mContext, "Reddetme İşlemi Gerçekleşti.", Toast.LENGTH_LONG).show()
                    document.reference.update("acceptanceCase", "rejected")
                    val intent = Intent(mContext, RejectedHairdressers::class.java)
                    mContext.startActivity(intent)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return hairdresserModels!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView
        var button: ImageView
        var noconfirm: ImageView

        init {
            name = itemView.findViewById(R.id.name)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
            button = itemView.findViewById(R.id.confirm)
            noconfirm = itemView.findViewById(R.id.noconfirm)
        }
    }

}