package com.example.hairdresserbookingapplication.adapter.hairdresserAdapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.hairdresser.ConfirmedUsers
import com.example.hairdresserbookingapplication.hairdresser.RejectedUsers
import com.example.hairdresserbookingapplication.model.ReservationModel
import com.example.hairdresserbookingapplication.model.TimeSlotModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class ReservationConfirmationAdapter(private val mContext: Context, private val reservationModels: MutableList<ReservationModel?>?) : RecyclerView.Adapter<ReservationConfirmationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.reservation_confirm_and_reject, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reservationModel = reservationModels?.get(position)
        holder.services.text = reservationModel!!.selected_services
        holder.name.text = reservationModel.name
        holder.phone.text = reservationModel.phone
        holder.mail.text = reservationModel.email
        holder.day.text = reservationModel.day
        holder.gender.text = reservationModel.gender
        holder.time.text = reservationModel.time
        holder.button.setOnClickListener {
            reservationModel.id?.let { it1 -> changeStore(it1) }
            reservationModel.day?.let { it1 -> reservationModel.time?.let { it2 -> confirmCalendar(it1, it2) } }
        }
        holder.noconfirm.setOnClickListener {
            reservationModel.id?.let { it1 -> makeReject(it1) }
            reservationModel.day?.let { it1 -> reservationModel.time?.let { it2 -> rejectCalendar(it1, it2) } }
        }
    }

    private fun changeStore(id: String) {
        val mFirestore = FirebaseFirestore.getInstance()
        mFirestore.collection("ReservationRequests").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<ReservationModel>(ReservationModel::class.java)
                if (model!!.id == id && model!!.acceptanceCase == "waiting") {
                    Toast.makeText(mContext, "Onaylama İşlemi Gerçekleşti", Toast.LENGTH_LONG).show()
                    document.reference.update("acceptanceCase", "yes")
                    val intent = Intent(mContext, ConfirmedUsers::class.java)
                    mContext.startActivity(intent)
                }
            }
        }
    }

    private fun confirmCalendar(date: String, time: String) {
        val firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseFirestore.collection("Hairdressers").document(FirebaseAuth.getInstance().uid!!)
                .collection("FullTimes").get().addOnCompleteListener { task ->
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<TimeSlotModel>(TimeSlotModel::class.java)
                        if (model!!.date == date && model!!.time == time && model!!.acceptanceCase == "waiting") {
                            document.reference.update("acceptanceCase", "yes")
                        }
                    }
                }
    }

    private fun rejectCalendar(date: String, time: String) {
        val firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseFirestore.collection("Hairdressers").document(FirebaseAuth.getInstance().uid!!)
                .collection("FullTimes").get().addOnCompleteListener { task ->
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<TimeSlotModel>(TimeSlotModel::class.java)
                        if (model!!.date == date && model!!.time == time && model!!.acceptanceCase == "waiting") {
                            document.reference.update("acceptanceCase", "no")
                            document.reference.delete()
                        }
                    }
                }
    }

    private fun makeReject(id: String) {
        val mFirestore = FirebaseFirestore.getInstance()
        mFirestore.collection("ReservationRequests").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<ReservationModel>(ReservationModel::class.java)
                if (model!!.id == id && model!!.acceptanceCase == "waiting") {
                    Toast.makeText(mContext, "Reddetme İşlemi Gerçekleşti", Toast.LENGTH_LONG).show()
                    document.reference.update("acceptanceCase", "no")
                    val intent = Intent(mContext, RejectedUsers::class.java)
                    mContext.startActivity(intent)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return reservationModels!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView
        var day: TextView
        var time: TextView
        var gender: TextView
        var button: ImageView
        var noconfirm: ImageView
        var services: TextView

        init {
            name = itemView.findViewById(R.id.name)
            day = itemView.findViewById(R.id.day)
            time = itemView.findViewById(R.id.time)
            services = itemView.findViewById(R.id.services)
            gender = itemView.findViewById(R.id.gender)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
            button = itemView.findViewById(R.id.confirm)
            noconfirm = itemView.findViewById(R.id.noconfirm)
        }
    }

}