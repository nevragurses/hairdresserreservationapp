package com.example.hairdresserbookingapplication.fragment.adminFragment

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.admin.adminPage
import com.example.hairdresserbookingapplication.fragment.LoginFragment_Common
import com.example.hairdresserbookingapplication.register.Register
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class AdminLoginFragment : LoginFragment_Common() {
    private fun init(view: View) {
        register = view.findViewById(R.id.noRegister)
        auth = FirebaseAuth.getInstance()
        mFirestore = FirebaseFirestore.getInstance()
        mFirebaseUser = auth?.currentUser
        if (mFirebaseUser != null) {
            userId = mFirebaseUser!!.uid
        }
        email = view.findViewById(R.id.email)
        password = view.findViewById(R.id.editTextPassword)
        forgotPassword = view.findViewById(R.id.forgotPassword)
        loginButton = view.findViewById(R.id.button)
        firstRegister = view.findViewById(R.id.register)
        register?.setOnClickListener {
            val intent = Intent(context, Register::class.java)
            intent.putExtra("FRAGMENT_REGISTER", "2")
            startActivity(intent)
        }
        firstRegister?.setOnClickListener {
            val intent = Intent(context, Register::class.java)
            intent.putExtra("FRAGMENT_REGISTER", "2")
            startActivity(intent)
        }
        loginButton?.setOnClickListener {
            collectionReference = mFirestore?.collection("Admins")
            loginAdmin()
        }
        forgotPassword?.setOnClickListener { view -> resetPassword(view) }
    }

    private fun checkUserAccessLevel() {
        userId?.let {
            collectionReference?.document(it)?.get()?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                if (Objects.requireNonNull(task.result)?.getString("email") != null) {
                    Toast.makeText(context, "Giriş Başarılı Oldu.", Toast.LENGTH_LONG).show()
                    val mainIntent = Intent(context, adminPage::class.java)
                    startActivity(mainIntent)
                } else {
                    Toast.makeText(context, "Giriş Başarısız-2!", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(context, "Giriş Başarısız!", Toast.LENGTH_LONG).show()
            }
        }
        }
    }

    private fun loginAdmin() {
        val e_mail = email?.text.toString()
        val pass = password?.text.toString()
        if (TextUtils.isEmpty(e_mail)) {
            Toast.makeText(context, "E-mail alanı boş olamaz", Toast.LENGTH_LONG).show()
        } else if (TextUtils.isEmpty(pass)) {
            Toast.makeText(context, "Şifre alanı boş olamaz", Toast.LENGTH_LONG).show()
        } else {
            auth?.signInWithEmailAndPassword(e_mail, pass)?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    userId = auth?.currentUser?.uid
                    val userVerification = auth?.currentUser
                    //if(userVerification.isEmailVerified()) {
                    checkUserAccessLevel()
                    //}
                    //else{
                    //   Toast.makeText(context,"Lütfen e-postanıza gönderilen doğrulama kodunu onaylayınız!",Toast.LENGTH_LONG).show();

                    //  }
                } else {
                    Toast.makeText(context, "Giriş Başarısız!" + task.exception!!.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        context = container!!.context
        val view = inflater.inflate(R.layout.fragment_admin_login, container, false)
        init(view)
        return view
    }
}