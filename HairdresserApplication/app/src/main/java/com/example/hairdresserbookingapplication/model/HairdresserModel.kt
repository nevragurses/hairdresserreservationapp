package com.example.hairdresserbookingapplication.model

class HairdresserModel {
    var name: String? = null
    var email: String? = null
    var id: String? = null
    var phone: String? = null
    var companyName: String? = null
    var companyAddress: String? = null
    var acceptanceCase: String? = null
    var province: String? = null
    var district: String? = null
    var street: String? = null
    var neighborhood: String? = null
    var type: String? = null
    var adminId: String? = null

    constructor() {}
    constructor(name: String?, email: String?, id: String?, phone: String?, companyName: String?, companyAddress: String?, acceptanceCase: String?,
                province: String?, district: String?, street: String?, neighborhood: String?, type: String?, adminId: String?) {
        this.name = name
        this.email = email
        this.id = id
        this.phone = phone
        this.companyName = companyName
        this.companyAddress = companyAddress
        this.acceptanceCase = acceptanceCase
        this.province = province
        this.district = district
        this.street = street
        this.neighborhood = neighborhood
        this.type = type
        this.adminId = adminId
    }

    constructor(name: String?, email: String?, id: String?, phone: String?, companyName: String?, companyAddress: String?, acceptanceCase: String?) {
        this.name = name
        this.email = email
        this.id = id
        this.phone = phone
        this.companyName = companyName
        this.companyAddress = companyAddress
        this.acceptanceCase = acceptanceCase
    }

}