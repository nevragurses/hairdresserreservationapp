package com.example.hairdresserbookingapplication.model

class HairdresserConfirmationModel {
    var name: String? = null
    var phone: String? = null

    var mail: String? = null
    var hairdresser_id: String? = null
    var admin_id: String? = null
    var confirm: String? = null

}