package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.QuestionModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class SendAnswer : AppCompatActivity() {
    private var name: String? = null
    private var phone: String? = null
    private var user_id: String? = null
    private var question: String? = null
    private var question_text: TextView? = null
    private var answer: EditText? = null
    private var send: Button? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Cevap Oluştur"
        name = intent.getStringExtra("name")
        phone = intent.getStringExtra("phone")
        user_id = intent.getStringExtra("user_id")
        question = intent.getStringExtra("question")
        question_text = findViewById(R.id.question)
        question_text?.setText(question)
        send = findViewById(R.id.send)
        answer = findViewById(R.id.answer)
        send?.setOnClickListener(View.OnClickListener {
            val msg = answer?.getText().toString()
            if (msg.length == 0) {
                Toast.makeText(this@SendAnswer, "Lütfen bir cevap giriniz!", Toast.LENGTH_LONG).show()
                return@OnClickListener
            }
            changeState(msg)
        })
    }

    private fun changeState(get_answer: String) {
        //Toast.makeText(SendAnswer.this,"HEELLO!" + question + " " + user_id   ,Toast.LENGTH_LONG).show();
        val mFirestore = FirebaseFirestore.getInstance()
        mFirestore.collection("Questions").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<QuestionModel>(QuestionModel::class.java)
                if (model!!.user_id == user_id && model!!.companyId == FirebaseAuth.getInstance().uid && model!!.answer == "waiting" && model!!.question == question) {
                    Toast.makeText(this@SendAnswer, "Cevap Gönderildi!", Toast.LENGTH_LONG).show()
                    document.reference.update("answer", get_answer)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_answer)
        init()
    }
}