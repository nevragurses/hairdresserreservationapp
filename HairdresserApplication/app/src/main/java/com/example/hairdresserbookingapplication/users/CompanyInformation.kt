package com.example.hairdresserbookingapplication.users

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.AdminModel
import com.example.hairdresserbookingapplication.users.CompanyInformation
import com.google.firebase.firestore.FirebaseFirestore

class CompanyInformation : AppCompatActivity() {
    private var companyId: String? = null
    private var mFirestore: FirebaseFirestore? = null
    private var email: TextView? = null
    private var adress: TextView? = null
    private var phone: TextView? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "İletişim Bilgileri"
        mFirestore = FirebaseFirestore.getInstance()
        companyId = intent.getStringExtra("companyId")
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        phone?.setOnClickListener(View.OnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + phone!!.getText())
            startActivity(intent)
        })
        adress = findViewById(R.id.addr)
        values
    }

    private val values: Unit
        private get() {
            val docRef = mFirestore!!.collection("Admins").document(companyId!!)
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    if (document!!.exists()) {
                        val adminModel = document.toObject<AdminModel>(AdminModel::class.java)
                        email!!.text = adminModel!!.email
                        phone!!.text = adminModel.phone
                        adress!!.text = adminModel.companyAddress
                    } else {
                    }
                } else {
                    Toast.makeText(this@CompanyInformation, "İşlem Başarısız", Toast.LENGTH_LONG).show()
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_information)
        init()
    }
}