package com.example.hairdresserbookingapplication.adapter.userAdapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.AdminModel
import com.example.hairdresserbookingapplication.model.UploadModel
import com.example.hairdresserbookingapplication.users.OneCompany
import com.squareup.picasso.Picasso

class AllCompaniesAdapter(private val mContext: Context, private val allCompanies: MutableList<AdminModel?>?, private val uploadModel: List<UploadModel>) : RecyclerView.Adapter<AllCompaniesAdapter.ViewHolder>() {
    private var cardView: CardView? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.company_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val company = allCompanies?.get(position)
        val uploadCurrent = uploadModel[position]
        holder.name.text = company!!.companyName
        holder.addr.text = company.companyAddress
        holder.call.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + company!!.phone)
            mContext.startActivity(intent)
        }
        Picasso.with(mContext)
                .load(uploadCurrent.imageUrl)
                .placeholder(R.drawable.defaultsalon)
                .fit()
                .centerCrop()
                .into(holder.image)
        cardView!!.setOnClickListener {
            val intent = Intent(mContext, OneCompany::class.java)
            intent.putExtra("company_id", company!!.id)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return allCompanies!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var addr: TextView
        var image: ImageView
        var call: TextView

        init {
            image = itemView.findViewById(R.id.image)
            name = itemView.findViewById(R.id.companyName)
            addr = itemView.findViewById(R.id.addr)
            call = itemView.findViewById(R.id.call)
            cardView = itemView.findViewById(R.id.card)
        }
    }

}