package com.example.hairdresserbookingapplication.users

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.userAdapters.AllCompaniesAdapter
import com.example.hairdresserbookingapplication.login.Login
import com.example.hairdresserbookingapplication.model.AdminModel
import com.example.hairdresserbookingapplication.model.UploadModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class UserPage : AppCompatActivity() {
    private var recycler: RecyclerView? = null
    private var mFirestore: FirebaseFirestore? = null
    private var companies: MutableList<AdminModel?>? = null
    private var uploadModel: MutableList<UploadModel>? = null
    private var allCompaniesAdapter: AllCompaniesAdapter? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById<View>(R.id.actionbar) as Toolbar
        setSupportActionBar(actionbar)
        recycler = findViewById(R.id.recycler)
        recycler?.setHasFixedSize(true)
        mFirestore = FirebaseFirestore.getInstance()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        companies = ArrayList()
        uploadModel = ArrayList()
        getCompanies()
    }

    private fun getCompanies() {
        val flag = intArrayOf(0)
        mFirestore!!.collection("Admins").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<AdminModel>(AdminModel::class.java)
                companies!!.add(model)
                val mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")
                mDatabaseRef.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (postSnapshot in dataSnapshot.children) {
                            val upload: UploadModel? = postSnapshot.getValue<UploadModel>(UploadModel::class.java)
                            upload!!.key = postSnapshot.key
                            if (upload.adminKey == model!!.id && upload.name == "kapak") {
                                uploadModel!!.add(upload)
                                flag[0] = 1
                            }
                        }
                        if (flag[0] == 0) {
                            uploadModel!!.add(UploadModel())
                        }
                        allCompaniesAdapter = AllCompaniesAdapter(this@UserPage, companies, uploadModel!!)
                        recycler!!.adapter = allCompaniesAdapter
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
            }
            flag[0] = 0
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_page)
        init()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menuuser, menu)
        Objects.requireNonNull(supportActionBar)!!.setDisplayShowTitleEnabled(false)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item0 -> {
                val intent0 = Intent(this@UserPage, AccountSettings::class.java)
                startActivity(intent0)
                return true
            }
            R.id.item1 -> {
                val intent1 = Intent(this@UserPage, Reservation_Requests_User::class.java)
                startActivity(intent1)
                return true
            }
            R.id.item2 -> {
                val intent2 = Intent(this@UserPage, ConfirmedReservations::class.java)
                startActivity(intent2)
                return true
            }
            R.id.item3 -> {
                val intent3 = Intent(this@UserPage, RejectedReservations::class.java)
                startActivity(intent3)
                return true
            }
            R.id.item4 -> {
                val intent4 = Intent(this@UserPage, QuestionList::class.java)
                startActivity(intent4)
                return true
            }
            R.id.item5 -> {
                val intent5 = Intent(this@UserPage, AnsweredQuestions::class.java)
                startActivity(intent5)
                return true
            }
            R.id.item6 -> {
                Toast.makeText(this, "Çıkış Yapılıyor", Toast.LENGTH_SHORT).show()
                val intent = Intent(this@UserPage, Login::class.java)
                intent.putExtra("FRAGMENT_ID", "0")
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}