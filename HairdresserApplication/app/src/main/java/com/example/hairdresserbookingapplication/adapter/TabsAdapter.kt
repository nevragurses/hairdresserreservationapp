package com.example.hairdresserbookingapplication.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.*

class TabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val fragments: ArrayList<Fragment>
    private val titles: ArrayList<String>

    //Shows tabs.
    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    //tab number
    override fun getCount(): Int {
        return fragments.size
    }

    //adding fragment
    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titles.add(title)
    }

    //for screen titles.
    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    init {
        fragments = ArrayList()
        titles = ArrayList()
    }
}