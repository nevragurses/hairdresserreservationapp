package com.example.hairdresserbookingapplication.adapter.reservationAdapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.TimeSlotModel
import java.util.*

class TimeSlotAdapter_User(private val mContext: Context, private val getTime: List<String>, private val hairdresser_id: String, date: String, timeSlots: MutableList<TimeSlotModel?>) : RecyclerView.Adapter<TimeSlotAdapter_User.ViewHolder>() {
    private val cardViewList: MutableList<CardView>
    private val date: String
    private val slots: MutableList<Int>
    private val timeSlots: MutableList<TimeSlotModel?>
    private val cardViewFull: MutableList<CardView>
    private val userId: String? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.time_slot, parent, false)
        return ViewHolder(view)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.timeslot.text = getTime[position]
        if (timeSlots.size == 0) {
            holder.cardTime.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite))
        } else {
            for (timeSlotModel in timeSlots) {
                val slotVal: Int = timeSlotModel!!.slot!!.toInt()
                if (slotVal == position) {
                    slots.add(slotVal)
                    holder.cardTime.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray))
                    cardViewFull.add(holder.cardTime)
                    if (timeSlotModel.acceptanceCase == "waiting") {
                        holder.desc.text = "Beklemede"
                    } else {
                        holder.desc.text = "Dolu"
                    }
                }
            }
        }
        if (!cardViewList.contains(holder.cardTime)) {
            cardViewList.add(holder.cardTime)
        }
        holder.cardTime.setOnClickListener {
            for (cardView in cardViewList) {
                if (cardViewFull.contains(cardView)) {
                    cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray))
                } else {
                    cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite))
                }
            }
            if (!slots.contains(position)) {
                holder.cardTime.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorBlue))
                val intent = Intent("time")
                intent.putExtra("time", holder.timeslot.text.toString())
                intent.putExtra("slot", position.toString())
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
            } else {
                Toast.makeText(mContext, "Dolu veya Onay Bekleme Sürecinde!", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return getTime.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cardTime: CardView
        var timeslot: TextView
        var desc: TextView

        init {
            cardTime = itemView.findViewById(R.id.card_time_slot)
            timeslot = itemView.findViewById(R.id.txt_timeslot)
            desc = itemView.findViewById(R.id.desc)
        }
    }

    init {
        cardViewList = ArrayList()
        cardViewFull = ArrayList()
        this.date = date
        this.timeSlots = timeSlots
        slots = ArrayList()
    }
}