package com.example.hairdresserbookingapplication.adapter.userAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.ServiceModel

class List_Services(private val mContext: Context, private val serviceModel: MutableList<ServiceModel?>?) : RecyclerView.Adapter<List_Services.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.service_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val appointmentModel = serviceModel?.get(position)
        holder.name.text = appointmentModel!!.name + ": "
        holder.price.text = appointmentModel.price
    }

    override fun getItemCount(): Int {
        return serviceModel!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var price: TextView

        init {
            name = itemView.findViewById(R.id.name)
            price = itemView.findViewById(R.id.price)
        }
    }

}