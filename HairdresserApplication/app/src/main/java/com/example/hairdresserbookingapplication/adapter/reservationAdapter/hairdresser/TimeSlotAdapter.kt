package com.example.hairdresserbookingapplication.adapter.reservationAdapter.hairdresser

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.ReservationModel
import com.example.hairdresserbookingapplication.model.TimeSlotModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class TimeSlotAdapter(private val mContext: Context, private val getTime: List<String>, private val hairdresser_id: String, date: String, timeSlots: MutableList<TimeSlotModel?>) : RecyclerView.Adapter<TimeSlotAdapter.ViewHolder>() {
    private val cardViewList: MutableList<CardView>
    private val date: String
    private val slots: MutableList<Int>
    private val timeSlots: MutableList<TimeSlotModel?>
    private val cardViewFull: MutableList<CardView>
    private val userId: String? = null
    var `val` = false
    var count = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.time_slot, parent, false)
        return ViewHolder(view)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.timeslot.text = getTime[position]
        if (timeSlots.size == 0) {
            holder.cardTime.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite))
        } else {
            for (timeSlotModel in timeSlots) {
                val slotVal: Int = timeSlotModel!!.slot!!.toInt()
                if (slotVal == position) {
                    slots.add(slotVal)
                    holder.cardTime.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray))
                    cardViewFull.add(holder.cardTime)
                    if (timeSlotModel.acceptanceCase == "waiting") {
                        holder.desc.text = "Beklemede"
                    } else {
                        holder.desc.text = "Dolu"
                    }
                }
            }
        }
        if (!cardViewList.contains(holder.cardTime)) {
            cardViewList.add(holder.cardTime)
        }
        holder.cardTime.setOnClickListener { view ->
            for (cardView in cardViewList) {
                if (cardViewFull.contains(cardView)) {
                    cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray))
                } else {
                    cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite))
                }
            }
            if (!slots.contains(position)) {
                holder.cardTime.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorBlue))
                val intent = Intent("BUSY_TIMES")
                intent.putExtra("time", holder.timeslot.text.toString())
                intent.putExtra("slot", position.toString())
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
            } else {
                alertDialog(view, holder.timeslot.text.toString())
                //Toast.makeText(mContext, "Dolu veya Onay Bekleme Sürecinde!" , Toast.LENGTH_LONG).show();
                //controlDatabase(holder.timeslot.getText().toString());
            }
        }
    }

    private fun controlDatabase(time: String) {
        val firebaseFirestore = FirebaseFirestore.getInstance()
        val ref = firebaseFirestore.collection("Hairdressers")
                .document(hairdresser_id)
                .collection("FullTimes")
        ref.get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<TimeSlotModel>(TimeSlotModel::class.java)
                if (model!!.date == date && model!!.time == time) {
                    Toast.makeText(mContext, "Rezervasyon İptali Gerçekleşti. ", Toast.LENGTH_LONG).show()
                    ref.document().delete()
                    document.reference.delete()
                }
            }
        }
    }

    private fun changeStore(time: String) {
        val mFirestore = FirebaseFirestore.getInstance()
        mFirestore.collection("ReservationRequests").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<ReservationModel>(ReservationModel::class.java)
                if (model!!.hairdresser_id == FirebaseAuth.getInstance().uid && model!!.day == date && model!!.time == time) {
                    document.reference.update("acceptanceCase", "afterNo")
                }
            }
        }
    }

    private fun alertDialog(view: View, time: String) {
        val builder = AlertDialog.Builder(mContext)
        builder.setMessage("Rezervasyon Yapılmış Randevuyu İptal Etmek İstiyor Musunuz?")
        builder.setPositiveButton("Onaylıyorum") { dialogInterface, i ->
            changeStore(time)
            controlDatabase(time)
        }
        builder.setNegativeButton("İptal") { dialogInterface, i -> }
        builder.show()
    }

    override fun getItemCount(): Int {
        //Toast.makeText(mContext,"SIZE:" + getTime.size(),Toast.LENGTH_LONG).show();
        return getTime.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cardTime: CardView
        var timeslot: TextView
        var desc: TextView

        init {
            cardTime = itemView.findViewById(R.id.card_time_slot)
            timeslot = itemView.findViewById(R.id.txt_timeslot)
            desc = itemView.findViewById(R.id.desc)
        }
    }

    init {
        cardViewList = ArrayList()
        cardViewFull = ArrayList()
        this.date = date
        this.timeSlots = timeSlots
        slots = ArrayList()
    }
}