package com.example.hairdresserbookingapplication.adapter.userAdapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.admin.SeeAnswer
import com.example.hairdresserbookingapplication.model.QuestionModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class AnsweredQuestions_Adapter(private val mContext: Context, private val questions: MutableList<QuestionModel?>?) : RecyclerView.Adapter<AnsweredQuestions_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.answered_question_item_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val questionModel = questions?.get(position)
        val mFirestore = FirebaseFirestore.getInstance()
        questionModel!!.companyId?.let {
            mFirestore.collection("Admins").document(it).get().addOnCompleteListener { task ->
            holder.name.text = task.result!!["companyName"].toString()
            holder.phone.text = task.result!!["phone"].toString()
            holder.mail.text = task.result!!["email"].toString()
        }
        }
        holder.click.setOnClickListener {
            val intent = Intent(mContext, SeeAnswer::class.java)
            intent.putExtra("question", questionModel.question)
            intent.putExtra("answer", questionModel.answer)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return questions!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView
        var click: TextView
        var trash: ImageView

        init {
            trash = itemView.findViewById(R.id.trash)
            name = itemView.findViewById(R.id.name)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
            click = itemView.findViewById(R.id.click)
            (mContext as Activity).runOnUiThread {
                trash.setOnClickListener {
                    val questionModel2 = questions?.get(adapterPosition)
                    val ref = FirebaseFirestore.getInstance().collection("Questions")
                    ref.get().addOnCompleteListener { task ->
                        for (document in Objects.requireNonNull(task.result)!!) {
                            val model = document.toObject<QuestionModel>(QuestionModel::class.java)
                            if (model!!.user_id == FirebaseAuth.getInstance().uid && model!!.question == questionModel2!!.question && model!!.answer == questionModel2.answer && model!!.user_id == questionModel2.user_id && model!!.question_removed_user == "no") {
                                document.reference.update("question_removed_user", "yes")
                                questions!!.removeAt(adapterPosition)
                                //synchronized(questions) { questions.notify() }
                                //notifyDataSetChanged()
                                Toast.makeText(mContext, "Başarı ile Silindi", Toast.LENGTH_LONG).show()
                                break
                            }
                        }
                    }
                }
            }
        }
    }

}