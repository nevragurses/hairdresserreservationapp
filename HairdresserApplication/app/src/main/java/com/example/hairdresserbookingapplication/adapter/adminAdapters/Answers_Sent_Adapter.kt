package com.example.hairdresserbookingapplication.adapter.adminAdapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.admin.SeeAnswer
import com.example.hairdresserbookingapplication.model.QuestionModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class Answers_Sent_Adapter(private val mContext: Context, private val questions: MutableList<QuestionModel?>?) : RecyclerView.Adapter<Answers_Sent_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.sent_answer, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val questionModel = questions?.get(position)
        holder.name.text = questionModel?.name
        holder.phone.text = questionModel?.phone
        holder.mail.text = questionModel?.email
        holder.click.setOnClickListener {
            val intent = Intent(mContext, SeeAnswer::class.java)
            intent.putExtra("name", questionModel?.name)
            intent.putExtra("phone", questionModel?.phone)
            intent.putExtra("mail", questionModel?.email)
            intent.putExtra("user_id", questionModel?.user_id)
            intent.putExtra("question", questionModel?.question)
            intent.putExtra("answer", questionModel?.answer)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return questions!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView
        var click: TextView
        var trash: ImageView

        init {
            trash = itemView.findViewById(R.id.trash)
            name = itemView.findViewById(R.id.name)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
            click = itemView.findViewById(R.id.click)
            (mContext as Activity).runOnUiThread {
                trash.setOnClickListener {
                    val questionModel2 = questions?.get(adapterPosition)
                    val ref = FirebaseFirestore.getInstance().collection("Questions")
                    ref.get().addOnCompleteListener { task ->
                        for (document in Objects.requireNonNull(task.result)!!) {
                            val model = document.toObject<QuestionModel>(QuestionModel::class.java)
                            if (model!!.companyId == FirebaseAuth.getInstance().uid && model!!.question == questionModel2!!.question && model!!.answer == questionModel2!!.answer && model!!.user_id == questionModel2.user_id && model!!.question_removed == "no") {
                                document.reference.update("question_removed", "yes")
                                questions?.removeAt(adapterPosition)
                                //synchronized(questions) { questions?.notify() } BAKILACAK
                                //notifyDataSetChanged()
                                Toast.makeText(mContext, "Başarı ile Silindi", Toast.LENGTH_LONG).show()
                                break
                            }
                        }
                    }
                }
            }
        }
    }

}