package com.example.hairdresserbookingapplication.adapter.adminAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.HairdresserModel

class ConfirmedAndRejected_Hairdressers(private val mContext: Context, private val hairdresserModels: MutableList<HairdresserModel?>?) : RecyclerView.Adapter<ConfirmedAndRejected_Hairdressers.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.confirmed_and_rejected, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val appointmentModel = hairdresserModels?.get(position)
        holder.name.text = appointmentModel!!.name
        holder.phone.text = appointmentModel.phone
        holder.mail.text = appointmentModel.email
    }

    override fun getItemCount(): Int {
        return hairdresserModels!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView

        init {
            name = itemView.findViewById(R.id.name)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
        }
    }

}