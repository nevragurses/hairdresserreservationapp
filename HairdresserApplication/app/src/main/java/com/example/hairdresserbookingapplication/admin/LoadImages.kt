package com.example.hairdresserbookingapplication.admin

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.admin.LoadImages
import com.example.hairdresserbookingapplication.model.UploadModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso
import java.util.*

class LoadImages : AppCompatActivity() {
    private var mButtonChooseImage: Button? = null
    private var mButtonUpload: Button? = null
    private var mTextViewShowUploads: TextView? = null
    private var mEditTextFileName: EditText? = null
    private var mImageView: ImageView? = null
    private var mProgressBar: ProgressBar? = null
    private var mImageUri: Uri? = null
    private var mStorageRef: StorageReference? = null
    private var mDatabaseRef: DatabaseReference? = null
    private var mUploadTask: StorageTask<UploadTask.TaskSnapshot>? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "İşletme Resim Yükleme"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        mButtonChooseImage = findViewById(R.id.button_choose_image)
        mButtonUpload = findViewById(R.id.button_upload)
        mTextViewShowUploads = findViewById(R.id.text_view_show_uploads)
        mEditTextFileName = findViewById(R.id.edit_text_file_name)
        mImageView = findViewById(R.id.image_view)
        mProgressBar = findViewById(R.id.progress_bar)
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads")
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")
        mButtonChooseImage?.setOnClickListener(View.OnClickListener { openFileChooser() })
        mButtonUpload?.setOnClickListener(View.OnClickListener {
            if (mUploadTask != null && mUploadTask!!.isInProgress) {
                Toast.makeText(this@LoadImages, "Yükleme gerçekleşiyor...", Toast.LENGTH_SHORT).show()
            } else {
                uploadFile()
            }
        })
        mTextViewShowUploads?.setOnClickListener(View.OnClickListener { openImagesActivity() })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_load_images)
        init()
    }

    private fun openFileChooser() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            mImageUri = data.data
            Picasso.with(this).load(mImageUri).into(mImageView)
        }
    }

    private fun getFileExtension(uri: Uri): String? {
        val cR = contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    private fun uploadFile() {
        if (mImageUri != null) {
            val fileReference = mStorageRef!!.child(System.currentTimeMillis()
                    .toString() + "." + getFileExtension(mImageUri!!))
            mUploadTask = fileReference.putFile(mImageUri!!)
                    .addOnSuccessListener {
                        mUploadTask!!.continueWithTask { task ->
                            if (!task.isSuccessful) {
                                throw Objects.requireNonNull(task.exception)!!
                            }
                            fileReference.downloadUrl
                        }.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                val handler = Handler()
                                handler.postDelayed({ mProgressBar!!.progress = 0 }, 500)
                                Toast.makeText(this@LoadImages, "Yükleme başarı ile gerçekleşti.", Toast.LENGTH_LONG).show()
                                val upload = UploadModel(mEditTextFileName!!.text.toString().trim { it <= ' ' },
                                        task.result.toString(), FirebaseAuth.getInstance().uid)
                                val uploadId = mDatabaseRef!!.push().key
                                mDatabaseRef!!.child(uploadId!!).setValue(upload)
                                mProgressBar!!.visibility = View.INVISIBLE
                                mImageView!!.setImageResource(R.drawable.photo_add)
                            }
                        }
                    }.addOnFailureListener { e -> Toast.makeText(this@LoadImages, e.message, Toast.LENGTH_SHORT).show() }
                    .addOnProgressListener { taskSnapshot ->
                        mProgressBar!!.visibility = View.VISIBLE
                        val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount
                        mProgressBar!!.progress = progress.toInt()
                    }
        } else {
            Toast.makeText(this, "Hiçbir dosya seçilmedi.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun openImagesActivity() {
        val intent = Intent(this, ImagesActivity::class.java)
        startActivity(intent)
    }

    companion object {
        private const val PICK_IMAGE_REQUEST = 1
    }
}