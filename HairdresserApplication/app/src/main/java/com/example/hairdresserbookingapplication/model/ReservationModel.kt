package com.example.hairdresserbookingapplication.model

class ReservationModel {
    var gender: String? = null
    var hairdresser: String? = null
    var day: String? = null
    var time: String? = null
    var companyId: String? = null
    var hairdresser_id: String? = null
    var user_id: String? = null
    var name: String? = null
    var phone: String? = null
    var email: String? = null
    var selected_services: String? = null
    var acceptanceCase: String? = null
    var id: String? = null

}