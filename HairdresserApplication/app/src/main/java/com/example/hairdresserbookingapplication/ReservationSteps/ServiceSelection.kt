package com.example.hairdresserbookingapplication.ReservationSteps

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.ReservationSteps.ServiceSelection
import com.example.hairdresserbookingapplication.adapter.reservationAdapter.ServiceSelectionAdapter
import com.example.hairdresserbookingapplication.model.SelectionModel
import com.example.hairdresserbookingapplication.model.ServiceModel
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class ServiceSelection : AppCompatActivity() {
    private var firebaseFirestore: FirebaseFirestore? = null
    private var companyId: String? = null
    var preSelectedIndex = -1
    private var serviceModels: MutableList<ServiceModel?>? = null
    private var recyclerView: ListView? = null
    private var serviceNameList: ArrayList<String?>? = null
    private var indexList: ArrayList<String?>? = null
    private var startIndexList: ArrayList<String>? = null
    private var back: Button? = null
    private var next: Button? = null
    private var currIntent: Intent? = null
    private var startIndex: String? = null
    private var selectedService: String? = null
    private fun init() {
        firebaseFirestore = FirebaseFirestore.getInstance()
        recyclerView = findViewById(R.id.recycler)
        next = findViewById(R.id.next)
        back = findViewById(R.id.back)
        serviceModels = ArrayList()
        val intent = intent
        serviceNameList = ArrayList()
        indexList = ArrayList()
        currIntent = getIntent()
        companyId = intent.getStringExtra("companyId")
        if (getIntent().getStringArrayListExtra("service_indexes") != null) {
            startIndexList = ArrayList()
            startIndexList = getIntent().getStringArrayListExtra("service_indexes")
        }
        val gender = intent.getStringExtra("gender")
        back?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@ServiceSelection, HairdresserSelection::class.java)
            putExtra(intent, gender)
            startActivity(intent)
        })
        next?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@ServiceSelection, TimeSelection::class.java)
            putExtra(intent, gender)
            if(serviceNameList!=null && serviceNameList!!.size !=0){
                //Toast.makeText(this@ServiceSelection,"HIZMETLER: " + serviceNameList,Toast.LENGTH_LONG).show();
                startActivity(intent)
            }
            else
                Toast.makeText(this@ServiceSelection,"Lütfen Hizmet Seçiniz!",Toast.LENGTH_LONG).show();

        })
        readServices()
    }

    private fun putExtra(intent: Intent, gender: String?) {
        intent.putExtra("companyId", companyId)
        intent.putExtra("hairdresser_index", getIntent().getStringExtra("hairdresser_index"))
        intent.putExtra("hairdresser_id", getIntent().getStringExtra("hairdresser_id"))
        intent.putExtra("hairdresser", getIntent().getStringExtra("hairdresser"))
        intent.putExtra("gender", gender)
        intent.putExtra("companyId", companyId)
        if (serviceNameList != null) intent.putStringArrayListExtra("service_list", serviceNameList)
        if (indexList != null) intent.putStringArrayListExtra("service_indexes", indexList)
    }

    private fun readServices() {
        firebaseFirestore!!.collection("Admins").document(companyId!!)
                .collection("Services").get().addOnCompleteListener { task ->
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<ServiceModel>(ServiceModel::class.java)
                        serviceModels!!.add(model)
                    }
                    serviceAdapter
                }
    }

    private val serviceAdapter: Unit
        get() {
            val service: MutableList<SelectionModel> = ArrayList()
            for (i in serviceModels!!.indices) {
                service.add(SelectionModel(false, serviceModels!![i]!!.name!!))
            }
            val adapter = ServiceSelectionAdapter(this@ServiceSelection, service)
            recyclerView!!.adapter = adapter
            if (startIndexList != null) {
                for (i in startIndexList!!.indices) {
                    choiceInitialize(service, startIndexList!![i].toInt())
                }
            }
            recyclerView!!.onItemClickListener = OnItemClickListener { adapterView, view, i, l ->
                choiceInitialize(service, i)
                adapter.updateRecords(service)
            }
        }

    private fun choiceInitialize(service: MutableList<SelectionModel>, i: Int) {
        val model = service[i]
        if (model.isSelected) {
            serviceNameList!!.remove(model.name)
            indexList!!.remove(Integer.toString(i))
            model.isSelected = false
        } else model.isSelected = true
        service[i] = model
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_selection)
        init()
        LocalBroadcastManager.getInstance(this).registerReceiver(getService, IntentFilter("service"))
    }

    var getService: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val get = intent.getStringExtra("name")
            selectedService = get
            startIndex = intent.getStringExtra("service_index")
            if (!serviceNameList!!.contains(get)) {

                serviceNameList!!.add(get)
            }
            if (!indexList!!.contains(startIndex)) {
                indexList!!.add(startIndex)
            }
        }
    }
}