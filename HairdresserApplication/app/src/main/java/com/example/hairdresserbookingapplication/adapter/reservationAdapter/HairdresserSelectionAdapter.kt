package com.example.hairdresserbookingapplication.adapter.reservationAdapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.SelectionModel
import java.util.*

class HairdresserSelectionAdapter : BaseAdapter {
    private var mContext: Context? = null
    private var contents: List<SelectionModel>? = null
    private var selected: List<Boolean>? = null
    private var inflater: LayoutInflater? = null
    var activity: Activity? = null

    constructor(activity: Activity?) {
        this.activity = activity
    }

    constructor(activity: Activity, contents: List<SelectionModel>) {
        this.activity = activity
        mContext = activity
        this.contents = contents
        selected = ArrayList()
        inflater = activity.layoutInflater
        for (i in contents.indices) {
            (selected as ArrayList<Boolean>).add(i, false)
        }
    }

    override fun getCount(): Int {
        return contents!!.size
    }

    override fun getItem(i: Int): Any {
        return i
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View? {
        var view = view
        var holder: ViewHolder? = null
        if (view == null) {
            view = inflater!!.inflate(R.layout.reservation_choice_item, viewGroup, false)
            holder = ViewHolder()
            holder.tvUserName = view.findViewById<View>(R.id.tv_name) as TextView
            holder.ivCheckBox = view.findViewById<View>(R.id.iv_check_box) as ImageView
            view.tag = holder
        } else holder = view.tag as ViewHolder
        val model = contents!![i]
        holder!!.tvUserName!!.text = model.name
        if (model.isSelected) {
            holder.ivCheckBox!!.setBackgroundResource(R.drawable.checked)
            val intent = Intent("hairdresser")
            intent.putExtra("name", model.name)
            intent.putExtra("hairdresser_id", model.id)
            intent.putExtra("hairdresser_index", Integer.toString(i))
            LocalBroadcastManager.getInstance(mContext!!).sendBroadcast(intent)
        } else holder.ivCheckBox!!.setBackgroundResource(R.drawable.check)
        return view
    }

    fun updateRecords(users: List<SelectionModel>?) {
        contents = users
        notifyDataSetChanged()
    }

    private inner class ViewHolder {
        var tvUserName: TextView? = null
        var ivCheckBox: ImageView? = null
    }
}