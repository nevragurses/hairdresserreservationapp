package com.example.hairdresserbookingapplication.adapter.adminAdapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.admin.SendAnswer
import com.example.hairdresserbookingapplication.model.QuestionModel

class ComingQuestion_Adapter(private val mContext: Context, private val questions: MutableList<QuestionModel?>?) : RecyclerView.Adapter<ComingQuestion_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.question_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val questionModel = questions?.get(position)
        holder.name.text = questionModel!!.name
        holder.phone.text = questionModel.phone
        holder.mail.text = questionModel.email
        holder.click.setOnClickListener {
            val intent = Intent(mContext, SendAnswer::class.java)
            intent.putExtra("name", questionModel.name)
            intent.putExtra("phone", questionModel.phone)
            intent.putExtra("mail", questionModel.email)
            intent.putExtra("user_id", questionModel.user_id)
            intent.putExtra("question", questionModel.question)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return questions!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView
        var click: TextView

        init {
            name = itemView.findViewById(R.id.name)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
            click = itemView.findViewById(R.id.click)
        }
    }

}