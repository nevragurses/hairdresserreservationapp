package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.adminAdapters.ServiceList_Adapter
import com.example.hairdresserbookingapplication.model.ServiceModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class ServiceList : AppCompatActivity() {
    private var recycler: RecyclerView? = null
    private var firebaseFirestore: FirebaseFirestore? = null
    private var serviceModels: MutableList<ServiceModel?>? = null
    private var userId: String? = null
    private var serviceListAdapter: ServiceList_Adapter? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Hizmet Güncelleme"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        recycler = findViewById(R.id.recycler)
        recycler?.setHasFixedSize(true)
        userId = FirebaseAuth.getInstance().uid
        firebaseFirestore = FirebaseFirestore.getInstance()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        serviceModels = ArrayList()
        readServices()
    }

    private fun readServices() {
        firebaseFirestore!!.collection("Admins").document(userId!!)
                .collection("Services").get().addOnCompleteListener { task ->
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<ServiceModel>(ServiceModel::class.java)
                        serviceModels!!.add(model)
                    }
                    serviceListAdapter = ServiceList_Adapter(this@ServiceList, serviceModels)
                    recycler!!.adapter = serviceListAdapter
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_list)
        init()
    }
}