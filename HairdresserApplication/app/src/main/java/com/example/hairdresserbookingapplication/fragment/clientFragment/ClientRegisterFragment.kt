package com.example.hairdresserbookingapplication.fragment.clientFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.fragment.RegisterFragment_Common
import com.example.hairdresserbookingapplication.login.Login
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class ClientRegisterFragment : RegisterFragment_Common() {
    private var gender: Spinner? = null
    private var selectedGender: String? = null
    var spinnerAdapter: ArrayAdapter<String>? = null
    private var spinnerDataList: List<String>? = null
    private fun init(view: View) {
        auth = FirebaseAuth.getInstance()
        val mFirebaseUser = auth!!.currentUser
        if (mFirebaseUser != null) {
            userId = mFirebaseUser.uid
        }
        alreadyRegister = view.findViewById(R.id.alreadyRegister)
        name = view.findViewById(R.id.editTextName)
        phone = view.findViewById(R.id.editTextMobile)
        email = view.findViewById(R.id.editTextEmail)
        mFirestore = FirebaseFirestore.getInstance()
        password = view.findViewById(R.id.editTextPassword)
        register = view.findViewById(R.id.register_button)
        gender = view.findViewById(R.id.gender_selection)
        login = view.findViewById(R.id.login)
        spinnerDataList = ArrayList()
        val subjectAdapter = thisContext?.let { ArrayAdapter.createFromResource(it, R.array.Gender, R.layout.support_simple_spinner_dropdown_item) }
        subjectAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        gender?.setAdapter(subjectAdapter)
        gender?.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val text = parent.getItemAtPosition(position).toString()
                selectedGender = text
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
        register!!.setOnClickListener { createNewAccount() }
        alreadyRegister!!.setOnClickListener {
            val intent = Intent(thisContext, Login::class.java)
            intent.putExtra("FRAGMENT_ID", "0")
            startActivity(intent)
        }
        login?.setOnClickListener {
            val intent = Intent(thisContext, Login::class.java)
            intent.putExtra("FRAGMENT_ID", "0")
            startActivity(intent)
        }
    }

    private fun createNewAccount() {
        if (!validateName() || !validateMail() || !validatePass() || !validatePhone()) {
            return
        }
        val userName = name!!.text.toString()
        val e_mail = email!!.text.toString()
        val pass = password!!.text.toString()
        val phoneNum = phone!!.text.toString()
        auth!!.createUserWithEmailAndPassword(e_mail, pass).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                //sendVerificationLink(); ONEMLİİİİİİİİİİİİİİİİİİİİİİİİİİ
                userId = auth!!.currentUser!!.uid
                val user = HashMap<String, String?>()
                user["id"] = userId
                user["name"] = userName
                user["email"] = e_mail
                user["phone"] = phoneNum
                user["gender"] = selectedGender
                mFirestore!!.collection("Costumers").document(userId!!).set(user).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val loginIntent = Intent(thisContext, Login::class.java)
                        loginIntent.putExtra("FRAGMENT_ID", "0")
                        //Toast.makeText(thisContext,"Kayıt başarı ile yapıldı.",Toast.LENGTH_LONG).show();
                        startActivity(loginIntent)
                    } else {
                        Toast.makeText(thisContext, "Kayıt yapılırken problem oluştu.", Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                Toast.makeText(thisContext, "Bu e-posta adresi ile kayıtlı kullanıcı zaten var!", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        thisContext = container!!.context
        val view = inflater.inflate(R.layout.fragment_client_register, container, false)
        init(view)
        return view
    }
}