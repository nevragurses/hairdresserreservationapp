package com.example.hairdresserbookingapplication.adapter.userAdapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.QuestionModel
import com.example.hairdresserbookingapplication.users.SeeQuestion_User
import com.google.firebase.firestore.FirebaseFirestore

class Questions_User_Adapter(private val mContext: Context, private val questions: MutableList<QuestionModel?>?) : RecyclerView.Adapter<Questions_User_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.question_item_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val questionModel = questions?.get(position)
        val mFirestore = FirebaseFirestore.getInstance()
        questionModel!!.companyId?.let {
            mFirestore.collection("Admins").document(it).get().addOnCompleteListener { task ->
            holder.name.text = task.result!!["companyName"].toString()
            holder.phone.text = task.result!!["phone"].toString()
            holder.mail.text = task.result!!["email"].toString()
        }
        }
        holder.click.setOnClickListener {
            val intent = Intent(mContext, SeeQuestion_User::class.java)
            intent.putExtra("question", questionModel.question)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return questions!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var mail: TextView
        var click: TextView

        init {
            name = itemView.findViewById(R.id.name)
            phone = itemView.findViewById(R.id.phone)
            mail = itemView.findViewById(R.id.mail)
            click = itemView.findViewById(R.id.click)
        }
    }

}