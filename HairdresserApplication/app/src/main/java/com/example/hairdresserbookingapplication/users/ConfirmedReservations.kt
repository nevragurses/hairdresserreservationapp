package com.example.hairdresserbookingapplication.users

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.userAdapters.ConfirmedReservations_User_Adapter
import com.example.hairdresserbookingapplication.model.ReservationModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class ConfirmedReservations : AppCompatActivity() {
    private var recycler: RecyclerView? = null
    private var mFirestore: FirebaseFirestore? = null
    private var reservationModelList: MutableList<ReservationModel?>? = null
    private var reservationConfirmationAdapter: ConfirmedReservations_User_Adapter? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Onaylanan Randevular"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        recycler = findViewById(R.id.recycler)
        recycler?.setHasFixedSize(true)
        mFirestore = FirebaseFirestore.getInstance()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        reservationModelList = ArrayList()
        readRequests()
    }

    private fun readRequests() {
        val ref = mFirestore!!.collection("ReservationRequests")
        ref.get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<ReservationModel>(ReservationModel::class.java)
                if (model!!.user_id == FirebaseAuth.getInstance().uid && model!!.acceptanceCase == "yes") {
                    reservationModelList!!.add(model)
                }
            }
            reservationConfirmationAdapter = ConfirmedReservations_User_Adapter(this@ConfirmedReservations, reservationModelList)
            recycler!!.adapter = reservationConfirmationAdapter
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmed_reservations)
        init()
    }
}