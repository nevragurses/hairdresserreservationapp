package com.example.hairdresserbookingapplication.adapter.adminAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.ServiceModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class ServiceList_Adapter(private val mContext: Context, private val serviceModel: MutableList<ServiceModel?>?) : RecyclerView.Adapter<ServiceList_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.service_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val appointmentModel = serviceModel?.get(position)
        holder.name.text = appointmentModel!!.name + ": "
        holder.price.text = appointmentModel!!.price + " ₺"
    }

    override fun getItemCount(): Int {
        return serviceModel!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var price: TextView
        var delete: Button
        var update: Button

        init {
            name = itemView.findViewById(R.id.name)
            price = itemView.findViewById(R.id.price)
            delete = itemView.findViewById(R.id.delete)
            update = itemView.findViewById(R.id.update)
            delete.setOnClickListener {
                val selectedItem = serviceModel?.get(adapterPosition)
                serviceModel!!.removeAt(adapterPosition)
                notifyDataSetChanged()
                val selectedService = selectedItem!!.name
                val mFirestore = FirebaseFirestore.getInstance()
                mFirestore.collection("Admins").document(FirebaseAuth.getInstance().uid!!)
                        .collection("Services").document(selectedService!!).delete()
                Toast.makeText(mContext, "Silme İşlemi Başarıyla Gerçekleşti.", Toast.LENGTH_LONG).show()
            }
            update.setOnClickListener { view -> // serviceModel.remove(getAdapterPosition());
                val builder = AlertDialog.Builder(mContext)
                builder.setMessage("Yeni fiyatı giriniz.")
                val updated = EditText(view.context)
                builder.setView(updated)
                val selectedItem = serviceModel?.get(adapterPosition)
                builder.setPositiveButton("Onaylıyorum") { dialogInterface, i ->
                    val updatedPrice = updated.text.toString()
                    if (updatedPrice != "") {
                        val selectedService = selectedItem!!.name
                        notifyItemChanged(adapterPosition)
                        val mFirestore = FirebaseFirestore.getInstance()
                        notifyItemChanged(adapterPosition, updatedPrice)
                        //synchronized(serviceModel) { serviceModel.notify() }
                        //notifyDataSetChanged()
                        val updateServices = HashMap<String, String?>()
                        updateServices["name"] = selectedService
                        updateServices["price"] = updatedPrice
                        mFirestore.collection("Admins").document(FirebaseAuth.getInstance().uid!!)
                                .collection("Services").document(selectedService!!).update(updateServices as Map<String, String?>)
                        Toast.makeText(mContext, "Güncelleme İşlemi Başarılı Oldu.", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(mContext, "Alan Boş Bırakılamaz! Güncelleme Başarısız.", Toast.LENGTH_LONG).show()
                    }
                }
                builder.setNegativeButton("İptal") { dialogInterface, i -> }
                builder.show()
            }
        }
    }

}