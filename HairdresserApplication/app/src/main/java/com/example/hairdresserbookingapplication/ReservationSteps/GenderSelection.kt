package com.example.hairdresserbookingapplication.ReservationSteps

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.ReservationSteps.GenderSelection

class GenderSelection : AppCompatActivity() {
    private var radioGroup: RadioGroup? = null
    private var radioButton: RadioButton? = null
    private var next: Button? = null
    private var companyId: String? = null
    private var currIntent: Intent? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Randevu Alma"
        radioGroup = findViewById(R.id.radioGroup)
        val women = findViewById<RadioButton>(R.id.radio_one)
        val man = findViewById<RadioButton>(R.id.radio_two)
        next = findViewById(R.id.next)
        currIntent = intent
        companyId = intent.getStringExtra("companyId")
        val gender = intent.getStringExtra("gender")
        if (gender != null) {
            if (gender == "Kadın") {
                women.isChecked = true
            } else man.isChecked = true
        }
        next?.setOnClickListener(View.OnClickListener {
            val selectedId = radioGroup!!.getCheckedRadioButtonId()
            radioButton = findViewById(selectedId)
            if (radioButton == null) {
                Toast.makeText(this@GenderSelection, "Lütfen cinsiyet seçiniz!", Toast.LENGTH_LONG).show()
            } else {
                //Toast.makeText(GenderSelection.this, "ID:" + companyId, Toast.LENGTH_LONG).show();
                val intent = Intent(this@GenderSelection, HairdresserSelection::class.java)
                putExtra(intent)
                startActivity(intent)
            }
        })
    }

    private fun putExtra(intent: Intent) {
        intent.putExtra("gender", radioButton!!.text.toString())
        if (companyId != null) intent.putExtra("companyId", companyId)
        if (currIntent!!.getStringArrayListExtra("service_list") != null) {
            intent.putStringArrayListExtra("service_list", getIntent().getStringArrayListExtra("service_list"))
        }
        if (currIntent!!.getStringArrayListExtra("service_indexes") != null) {
            intent.putStringArrayListExtra("service_indexes", getIntent().getStringArrayListExtra("service_indexes"))
        }
        if (currIntent!!.getStringExtra("hairdresser_index") != null) {
            intent.putExtra("hairdresser_index", getIntent().getStringExtra("hairdresser_index"))
        }
        if (currIntent!!.getStringExtra("hairdresser_id") != null) {
            intent.putExtra("hairdresser_id", getIntent().getStringExtra("hairdresser_id"))
        }
        if (currIntent!!.getStringExtra("hairdresser") != null) {
            intent.putExtra("hairdresser", getIntent().getStringExtra("hairdresser"))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gender_selection)
        init()
    }
}