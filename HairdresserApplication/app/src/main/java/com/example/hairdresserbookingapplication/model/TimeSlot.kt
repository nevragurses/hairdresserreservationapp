package com.example.hairdresserbookingapplication.model

class TimeSlot {
    var time: String? = null
    var slot: String? = null

    constructor() {}
    constructor(time: String?, slot: String?) {
        this.time = time
        this.slot = slot
    }

}