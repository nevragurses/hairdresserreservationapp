package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.adminAdapters.Answers_Sent_Adapter
import com.example.hairdresserbookingapplication.model.QuestionModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class Answers_Sent : AppCompatActivity() {
    private var recycler: RecyclerView? = null
    private var mFirestore: FirebaseFirestore? = null
    private var questionModelList: MutableList<QuestionModel?>? = null
    private var answers_sent_adapter: Answers_Sent_Adapter? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Gönderilen Cevaplar"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        recycler = findViewById(R.id.recycler)
        recycler?.setHasFixedSize(true)
        mFirestore = FirebaseFirestore.getInstance()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        questionModelList = ArrayList()
        readRequests()
    }

    private fun readRequests() {
        val ref = mFirestore!!.collection("Questions")
        ref.get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<QuestionModel>(QuestionModel::class.java)
                if (model!!.companyId == FirebaseAuth.getInstance().uid && model!!.answer != "waiting" && model!!.question_removed == "no") {
                    questionModelList!!.add(model)
                }
            }
            answers_sent_adapter = Answers_Sent_Adapter(this@Answers_Sent, questionModelList)
            recycler!!.adapter = answers_sent_adapter
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_answers__sent)
        init()
    }
}