package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.adminAdapters.ImageAdapter
import com.example.hairdresserbookingapplication.admin.ImagesActivity
import com.example.hairdresserbookingapplication.model.UploadModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import java.util.*

class ImagesActivity : AppCompatActivity() {
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: ImageAdapter? = null
    private var mProgressCircle: ProgressBar? = null
    private var mStorage: FirebaseStorage? = null
    private var mDatabaseRef: DatabaseReference? = null
    private var mDBListener: ValueEventListener? = null
    private var mUploads: MutableList<UploadModel>? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "İşletme Resimleri"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        mRecyclerView = findViewById(R.id.recycler_view)
        mRecyclerView?.setHasFixedSize(true)
        val manager = LinearLayoutManager(this)
        mRecyclerView?.setLayoutManager(manager)
        mProgressCircle = findViewById(R.id.progress_circle)
        mUploads = ArrayList()
        mAdapter = ImageAdapter(this@ImagesActivity, mUploads as ArrayList<UploadModel>)
        mRecyclerView?.setAdapter(mAdapter)
        mStorage = FirebaseStorage.getInstance()
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")
        images
    }

    private val images: Unit
        private get() {
            mDBListener = mDatabaseRef!!.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    mUploads!!.clear()
                    for (postSnapshot in dataSnapshot.children) {
                        val upload: UploadModel? = postSnapshot.getValue<UploadModel>(UploadModel::class.java)
                        upload?.key = postSnapshot.key
                        if (upload?.adminKey == FirebaseAuth.getInstance().uid) upload?.let { mUploads!!.add(it) }
                    }
                    mAdapter!!.notifyDataSetChanged()
                    mProgressCircle!!.visibility = View.INVISIBLE
                    registerForContextMenu(mRecyclerView)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(this@ImagesActivity, databaseError.message, Toast.LENGTH_SHORT).show()
                    mProgressCircle!!.visibility = View.INVISIBLE
                }
            })
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_images)
        init()
    }

    override fun onDestroy() {
        super.onDestroy()
        mDatabaseRef!!.removeEventListener(mDBListener!!)
    }
}