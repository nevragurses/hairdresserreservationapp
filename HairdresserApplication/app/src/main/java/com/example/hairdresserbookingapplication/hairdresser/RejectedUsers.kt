package com.example.hairdresserbookingapplication.hairdresser

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.hairdresserAdapter.ConfirmedAndRejectedUsers
import com.example.hairdresserbookingapplication.model.ReservationModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class RejectedUsers : AppCompatActivity() {
    private var recycler: RecyclerView? = null
    private var mFirestore: FirebaseFirestore? = null
    var reservationrequests: MutableList<ReservationModel?>? = null
    var confirmedUsers: ConfirmedAndRejectedUsers? = null
    var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Reddedilen Randevular"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        recycler = findViewById(R.id.recycler)
        recycler?.setHasFixedSize(true)
        mFirestore = FirebaseFirestore.getInstance()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        reservationrequests = ArrayList()
        readRequests()
    }

    private fun readRequests() {
        val ref = mFirestore!!.collection("ReservationRequests")
        ref.get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<ReservationModel>(ReservationModel::class.java)
                if (model!!.hairdresser_id == FirebaseAuth.getInstance().uid && model!!.acceptanceCase== "no") {
                    reservationrequests!!.add(model)
                }
            }
            confirmedUsers = ConfirmedAndRejectedUsers(this@RejectedUsers, reservationrequests)
            recycler!!.adapter = confirmedUsers
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmed_and_rejected_users)
        init()
    }
}