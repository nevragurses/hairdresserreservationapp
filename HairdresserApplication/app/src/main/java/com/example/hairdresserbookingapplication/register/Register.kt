package com.example.hairdresserbookingapplication.register

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.TabsAdapter
import com.example.hairdresserbookingapplication.fragment.adminFragment.AdminRegisterFragment
import com.example.hairdresserbookingapplication.fragment.clientFragment.ClientRegisterFragment
import com.example.hairdresserbookingapplication.fragment.hairdresserFragment.HairdresserRegisterFragment
import com.google.android.material.tabs.TabLayout

class Register : AppCompatActivity() {
    private var vpMain: ViewPager? = null
    private var tabsMain: TabLayout? = null
    private var adapter: TabsAdapter? = null
    fun init() {
        vpMain = findViewById<View>(R.id.vpMain) as ViewPager
        tabsMain = findViewById<View>(R.id.tabsMain) as TabLayout
        adapter = TabsAdapter(supportFragmentManager)
        if (intent.getStringExtra("FRAGMENT_REGISTER") != null && intent.getStringExtra("FRAGMENT_REGISTER") == "0") {
            adapter!!.addFragment(ClientRegisterFragment(), "Kullanıcı Üye Olmaa")
            adapter!!.addFragment(HairdresserRegisterFragment(), "Kuaför Üye Olma")
            adapter!!.addFragment(AdminRegisterFragment(), "Firma Yöneticisi Üye Olma")
        } else if (intent.getStringExtra("FRAGMENT_REGISTER") != null && intent.getStringExtra("FRAGMENT_REGISTER") == "1") {
            adapter!!.addFragment(HairdresserRegisterFragment(), "Kuaför Üye Olma")
            adapter!!.addFragment(ClientRegisterFragment(), "Kullanıcı Üye Olma")
            adapter!!.addFragment(AdminRegisterFragment(), "Firma Yöneticisi Üye Olma")
        } else if (intent.getStringExtra("FRAGMENT_REGISTER") != null && intent.getStringExtra("FRAGMENT_REGISTER") == "2") {
            adapter!!.addFragment(AdminRegisterFragment(), "Firma Yöneticisi Üye Olma")
            adapter!!.addFragment(ClientRegisterFragment(), "Kuaför Üye Olma")
            adapter!!.addFragment(HairdresserRegisterFragment(), "Kullanıcı Üye Olma")
        } else {
            adapter!!.addFragment(ClientRegisterFragment(), "Kullanıcı Üye Olma")
            adapter!!.addFragment(HairdresserRegisterFragment(), "Kuaför Üye Olma")
            adapter!!.addFragment(AdminRegisterFragment(), "Firma Yöneticisi Üye Olma")
        }
        vpMain!!.adapter = adapter
        tabsMain!!.setupWithViewPager(vpMain)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }
}