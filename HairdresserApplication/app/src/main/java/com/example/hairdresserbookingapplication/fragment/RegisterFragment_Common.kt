package com.example.hairdresserbookingapplication.fragment

import android.content.Context
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.FirebaseFirestore

open class RegisterFragment_Common : Fragment() {
    protected var thisContext: Context? = null
    protected var alreadyRegister: TextView? = null
    protected var name: EditText? = null
    protected var email: EditText? = null
    protected var phone: EditText? = null
    protected var password: EditText? = null
    protected var register: Button? = null
    protected var login: Button? = null
    protected var auth: FirebaseAuth? = null
    protected var userId: String? = null
    protected var reference: DatabaseReference? = null
    protected var mFirestore: FirebaseFirestore? = null
    protected fun validateName(): Boolean {
        val `val` = name!!.text.toString()
        return if (`val`.isEmpty()) {
            name!!.error = "Lütfen adınızı ve soyadınızı giriniz"
            false
        } else {
            name!!.error = null
            true
        }
    }

    protected fun validatePhone(): Boolean {
        val `val` = phone!!.text.toString()
        return if (`val`.isEmpty()) {
            phone!!.error = "Lütfen telefonunuzu giriniz"
            false
        } else {
            phone!!.error = null
            true
        }
    }

    protected fun validateMail(): Boolean {
        val `val` = email!!.text.toString()
        val emailPattern = Regex ("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")
        return if (`val`.isEmpty()) {
            email!!.error = "Lütfen e-posta adresinizi giriniz."
            false
        } else if (!`val`.matches(emailPattern)) {
            email!!.error = "E-posta adresiniz geçersizdir. Lütfen geçerli bir e-posta adresi giriniz."
            false
        } else {
            email!!.error = null
            true
        }
    }

    protected fun validatePass(): Boolean {
        val `val` = password!!.text.toString()
        return if (`val`.isEmpty()) {
            password!!.error = "Lütfen parolanızı giriniz."
            false
        } else if (`val`.length < 8) {
            password!!.error = "Parolanızın uzunluğu en az 8 karakter olmalıdır."
            false
        } else {
            password!!.error = null
            true
        }
    }

    protected fun sendVerificationLink() {
        Toast.makeText(thisContext, "SUPER.", Toast.LENGTH_LONG).show()
        auth!!.currentUser!!.sendEmailVerification().addOnSuccessListener {
            val builder = AlertDialog.Builder(thisContext!!)
            builder.setMessage("E-postanıza Gönderilen Doğrulama Kodunu Onaylayınız.")
            builder.setPositiveButton("Anladım", null)
            builder.show()
            Toast.makeText(thisContext, "E-postanıza Gönderilen Doğrulama Kodunu Onaylayınız.", Toast.LENGTH_LONG).show()
        }.addOnFailureListener { e ->
            val builder = AlertDialog.Builder(thisContext!!)
            builder.setMessage("E-postanıza doğrulama kodu gönderilemedi.")
            builder.setPositiveButton("Anladım", null)
            builder.show()
            Toast.makeText(thisContext, "E-postanıza doğrulama kodu gönderilemedi." + e.message, Toast.LENGTH_LONG).show()
        }
    }
}