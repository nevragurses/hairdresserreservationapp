package com.example.hairdresserbookingapplication.adapter.userAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.ReservationModel
import com.example.hairdresserbookingapplication.model.TimeSlotModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class ReservationRequests_Adapter(private val mContext: Context, private val reservationModels: MutableList<ReservationModel?>?) : RecyclerView.Adapter<ReservationRequests_Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.requested_reservations, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reservationModel = reservationModels?.get(position)
        holder.services.text = reservationModel!!.selected_services
        holder.name.text = reservationModel.hairdresser
        val mFirestore = FirebaseFirestore.getInstance()
        reservationModel.companyId?.let {
            mFirestore.collection("Admins").document(it).get().addOnCompleteListener { task ->
            holder.companyName.text = task.result!!["companyName"].toString()
            holder.phone.text = task.result!!["phone"].toString()
        }
        }
        reservationModel.hairdresser_id?.let { mFirestore.collection("Hairdressers").document(it).get().addOnCompleteListener { task -> holder.phone_hairdresser.text = task.result!!["phone"].toString() } }
        holder.day.text = reservationModel.day
        holder.time.text = reservationModel.time
        holder.reject.setOnClickListener { view ->
            val reset = AlertDialog.Builder(view.context)
            reset.setTitle("Randevu İptali Gerçekleştirmek İstediğinze Emin Misiniz?")
            reset.setPositiveButton("Evet") { dialogInterface, i ->
                //maili çıkar ve sıfırlama bağlantısı gönder.
                val ref = FirebaseFirestore.getInstance().collection("ReservationRequests")
                ref.get().addOnCompleteListener { task ->
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<ReservationModel>(ReservationModel::class.java)
                        if (model!!.user_id == FirebaseAuth.getInstance().uid && model!!.hairdresser_id == reservationModel.hairdresser_id && model!!.time == reservationModel.time && model!!.day == reservationModel.day) {
                            document.reference.delete()
                        }
                    }
                }
                reservationModel.day?.let { reservationModel.time?.let { it1 -> reservationModel.hairdresser_id?.let { it2 -> changeHairdresser(it1, it, it2) } } }
            }
            reset.setNegativeButton("Hayır") { dialogInterface, i ->
                //basarısız.
            }
            reset.create().show()
        }
    }

    private fun changeHairdresser(time: String, day: String, hairdresser_id: String) {
        val firebaseFirestore = FirebaseFirestore.getInstance()
        val ref = firebaseFirestore.collection("Hairdressers")
                .document(hairdresser_id)
                .collection("FullTimes")
        ref.get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<TimeSlotModel>(TimeSlotModel::class.java)
                if (model!!.date == day && model!!.time == time) {
                    Toast.makeText(mContext, "Randevu İptali Gerçekleşti. ", Toast.LENGTH_LONG).show()
                    ref.document().delete()
                    document.reference.delete()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return reservationModels!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var phone: TextView
        var companyName: TextView
        var day: TextView
        var time: TextView
        var phone_hairdresser: TextView
        var services: TextView
        var reject: TextView

        init {
            name = itemView.findViewById(R.id.name)
            reject = itemView.findViewById(R.id.reject)
            day = itemView.findViewById(R.id.day)
            time = itemView.findViewById(R.id.time)
            services = itemView.findViewById(R.id.services)
            phone = itemView.findViewById(R.id.phone)
            companyName = itemView.findViewById(R.id.companyName)
            phone_hairdresser = itemView.findViewById(R.id.phone_hairdresser)
        }
    }

}