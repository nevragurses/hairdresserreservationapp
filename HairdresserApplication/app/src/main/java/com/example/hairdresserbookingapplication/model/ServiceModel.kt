package com.example.hairdresserbookingapplication.model

class ServiceModel {
    var name: String? = null
    var price: String? = null

    constructor() {}
    constructor(name: String?, price: String?) {
        this.name = name
        this.price = price
    }

}