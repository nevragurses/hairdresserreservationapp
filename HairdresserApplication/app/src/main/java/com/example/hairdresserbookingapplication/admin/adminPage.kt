package com.example.hairdresserbookingapplication.admin

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.login.Login
import java.util.*

class adminPage : AppCompatActivity() {
    private var cardView1: CardView? = null
    private var cardView2: CardView? = null
    private var cardView3: CardView? = null
    private var cardView4: CardView? = null
    private var cardView5: CardView? = null
    private var cardView6: CardView? = null
    private var cardView7: CardView? = null
    private var cardView8: CardView? = null
    private var cardView9: CardView? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById<View>(R.id.actionbar) as Toolbar
        setSupportActionBar(actionbar)
        cardView1 = findViewById<View>(R.id.card1) as CardView
        cardView1!!.setOnClickListener {
            val intent = Intent(this@adminPage, WaitingHairdresser_Requests::class.java)
            startActivity(intent)
        }
        cardView2 = findViewById<View>(R.id.card2) as CardView
        cardView2!!.setOnClickListener {
            val loginIntent = Intent(this@adminPage, ConfirmedHairdressers::class.java)
            startActivity(loginIntent)
        }
        cardView3 = findViewById<View>(R.id.card3) as CardView
        cardView3!!.setOnClickListener {
            val loginIntent = Intent(this@adminPage, RejectedHairdressers::class.java)
            startActivity(loginIntent)
        }
        cardView4 = findViewById<View>(R.id.card4) as CardView
        cardView4!!.setOnClickListener {
            val loginIntent = Intent(this@adminPage, LoadImages::class.java)
            startActivity(loginIntent)
        }
        cardView5 = findViewById<View>(R.id.card5) as CardView
        cardView5!!.setOnClickListener {
            val loginIntent = Intent(this@adminPage, ImagesActivity::class.java)
            startActivity(loginIntent)
        }
        cardView6 = findViewById<View>(R.id.card6) as CardView
        cardView6!!.setOnClickListener {
            val loginIntent = Intent(this@adminPage, AddingServices::class.java)
            startActivity(loginIntent)
        }
        cardView7 = findViewById<View>(R.id.card7) as CardView
        cardView7!!.setOnClickListener {
            val loginIntent = Intent(this@adminPage, ServiceList::class.java)
            startActivity(loginIntent)
        }
        cardView8 = findViewById<View>(R.id.card8) as CardView
        cardView8!!.setOnClickListener {
            val loginIntent = Intent(this@adminPage, ComingQuestions::class.java)
            startActivity(loginIntent)
        }
        cardView9 = findViewById(R.id.card9)
        cardView9!!.setOnClickListener(View.OnClickListener {
            val loginIntent = Intent(this@adminPage, Answers_Sent::class.java)
            startActivity(loginIntent)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_page)
        init()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menuadmin, menu)
        Objects.requireNonNull(supportActionBar)?.setDisplayShowTitleEnabled(false)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item0 -> {
                val intent0 = Intent(this@adminPage, Admin_AccountSettings::class.java)
                startActivity(intent0)
                return true
            }
            R.id.item1 -> {
                Toast.makeText(this, "Çıkış Yapılıyor", Toast.LENGTH_SHORT).show()
                val intent = Intent(this@adminPage, Login::class.java)
                intent.putExtra("FRAGMENT_ID", "2")
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}