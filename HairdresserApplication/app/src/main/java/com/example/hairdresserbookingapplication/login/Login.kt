package com.example.hairdresserbookingapplication.login

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.TabsAdapter
import com.example.hairdresserbookingapplication.fragment.adminFragment.AdminLoginFragment
import com.example.hairdresserbookingapplication.fragment.clientFragment.ClientLoginFragment
import com.example.hairdresserbookingapplication.fragment.hairdresserFragment.HairdresserLoginFragment
import com.google.android.material.tabs.TabLayout

class Login : AppCompatActivity() {
    private val actionbar: Toolbar? = null
    private var vpMain: ViewPager? = null
    private var tabsMain: TabLayout? = null
    private var adapter: TabsAdapter? = null
    fun init() {
        vpMain = findViewById<View>(R.id.vpMain) as ViewPager
        tabsMain = findViewById<View>(R.id.tabsMain) as TabLayout
        adapter = TabsAdapter(supportFragmentManager)
        if (intent.getStringExtra("FRAGMENT_ID") != null && intent.getStringExtra("FRAGMENT_ID") == "0") {
            adapter!!.addFragment(ClientLoginFragment(), "Kullanıcı Girişi")
            adapter!!.addFragment(HairdresserLoginFragment(), "Kuaför Girişi")
            adapter!!.addFragment(AdminLoginFragment(), "Firma Yöneticisi Girişi")
        } else if (intent.getStringExtra("FRAGMENT_ID") != null && intent.getStringExtra("FRAGMENT_ID") == "1") {
            adapter!!.addFragment(HairdresserLoginFragment(), "Kuaför Girişi")
            adapter!!.addFragment(ClientLoginFragment(), "Kullanıcı Girişi")
            adapter!!.addFragment(AdminLoginFragment(), "Firma Yöneticisi Girişi")
        } else if (intent.getStringExtra("FRAGMENT_ID") != null && intent.getStringExtra("FRAGMENT_ID") == "2") {
            adapter!!.addFragment(AdminLoginFragment(), "Firma Yöneticisi Girişi")
            adapter!!.addFragment(HairdresserLoginFragment(), "Kuaför Girişi")
            adapter!!.addFragment(ClientLoginFragment(), "Kullanıcı Girişi")
        } else {
            adapter!!.addFragment(ClientLoginFragment(), "Kullanıcı Girişi")
            adapter!!.addFragment(HairdresserLoginFragment(), "Kuaför Girişi")
            adapter!!.addFragment(AdminLoginFragment(), "Firma Yöneticisi Girişi")
        }
        vpMain!!.adapter = adapter
        tabsMain!!.setupWithViewPager(vpMain)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }
}