package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R

class SeeAnswer : AppCompatActivity() {
    private var name: String? = null
    private var phone: String? = null
    private var user_id: String? = null
    private var question: String? = null
    private var answer: String? = null
    private var question_text: TextView? = null
    private var answer_text: TextView? = null
    private val send: Button? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Yanıtlanan Sorular ve Cevapları"
        name = intent.getStringExtra("name")
        name = intent.getStringExtra("name")
        phone = intent.getStringExtra("phone")
        user_id = intent.getStringExtra("user_id")
        question = intent.getStringExtra("question")
        answer = intent.getStringExtra("answer")
        question_text = findViewById(R.id.question)
        question_text?.setText(question)
        answer_text = findViewById(R.id.answer)
        question_text?.setText(question)
        answer_text?.setText(answer)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_answer)
        init()
    }
}