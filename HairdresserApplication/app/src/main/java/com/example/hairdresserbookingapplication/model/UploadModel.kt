package com.example.hairdresserbookingapplication.model

import com.google.firebase.firestore.Exclude

class UploadModel {
    var name: String? = null
    var imageUrl: String? = null

    @get:Exclude
    @set:Exclude
    var key: String? = null
    var adminKey: String? = null

    constructor() {
        //empty constructor needed
    }

    constructor(name: String, imageUrl: String?, adminKey: String?) {
        var name = name
        if (name.trim { it <= ' ' } == "") {
            name = ""
        }
        this.name = name
        this.imageUrl = imageUrl
        this.adminKey = adminKey
    }

    constructor(mImageUrl: String?) {
        imageUrl = mImageUrl
    }

}