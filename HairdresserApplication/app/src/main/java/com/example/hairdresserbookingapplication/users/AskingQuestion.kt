package com.example.hairdresserbookingapplication.users

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.CostumerModel
import com.example.hairdresserbookingapplication.users.AskingQuestion
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class AskingQuestion : AppCompatActivity() {
    private var company_id: String? = null
    private var question: EditText? = null
    private var actionbar: Toolbar? = null
    private var send: Button? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Soru Sor"
        company_id = intent.getStringExtra("companyId")
        question = findViewById(R.id.question)
        send = findViewById(R.id.send)
        send?.setOnClickListener(View.OnClickListener {
            val msg = question!!.getText().toString()
            if (msg.length == 0) {
                Toast.makeText(this@AskingQuestion, "Lütfen bir soru giriniz!", Toast.LENGTH_LONG).show()
                return@OnClickListener
            }
            findUser(msg)
        })
    }

    private fun recordDatabase(msg: String, email: String?, name: String?, phone: String?) {
        val mFirestore = FirebaseFirestore.getInstance()
        val sending_question = HashMap<String, String?>()
        sending_question["companyId"] = company_id
        sending_question["email"] = email
        sending_question["name"] = name
        sending_question["phone"] = phone
        sending_question["question"] = msg
        sending_question["user_id"] = FirebaseAuth.getInstance().uid
        sending_question["answer"] = "waiting"
        sending_question["question_removed"] = "no"
        sending_question["question_removed_user"] = "no"
        val ref = mFirestore.collection("Questions").document()
        sending_question["id"] = ref.id
        ref.set(sending_question).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Toast.makeText(this@AskingQuestion, "Soru Başarı İle Gönderildi.", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this@AskingQuestion, "Soru Gönderilirken Problem Oluştu.", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun findUser(msg: String) {
        val docRef = FirebaseFirestore.getInstance()
                .collection("Costumers")
                .document(Objects.requireNonNull(FirebaseAuth.getInstance().uid)!!)
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                if (document != null && document.exists()) {
                    val costumerModel = document.toObject<CostumerModel>(CostumerModel::class.java)
                    recordDatabase(msg, costumerModel!!.email, costumerModel.name, costumerModel.phone)
                }
            } else {
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asking_question)
        init()
    }
}