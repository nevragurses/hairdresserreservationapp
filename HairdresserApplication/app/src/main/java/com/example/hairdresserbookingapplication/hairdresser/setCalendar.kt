package com.example.hairdresserbookingapplication.hairdresser

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CalendarView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.Common
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.reservationAdapter.hairdresser.TimeSlotAdapter
import com.example.hairdresserbookingapplication.model.TimeSlot
import com.example.hairdresserbookingapplication.model.TimeSlotModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class setCalendar : AppCompatActivity() {
    //implements DatePickerDialog.OnDateSetListener {
    private var recyclerView: RecyclerView? = null
    private var times: List<String>? = null
    private var timeSlotAdapter: TimeSlotAdapter? = null
    private var add: Button? = null
    private var selectedTime: String? = null
    private var selectedDate: String? = null
    private var slot: String? = null
    private var clockSelection: TextView? = null
    private var timeSlotModels: MutableList<TimeSlotModel?>? = null
    private var timeSlotList: List<TimeSlot>? = null
    private var actionbar: Toolbar? = null
    private fun changeLanguage() {
        val languageToLoad = "tr" // your language
        val locale = Locale(languageToLoad)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config,
                baseContext.resources.displayMetrics)
    }

    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Randevu Takvimi Güncelle"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        changeLanguage()
        add = findViewById(R.id.add)
        timeSlotModels = ArrayList()
        clockSelection = findViewById(R.id.clockselection)
        recyclerView = findViewById(R.id.recycler_view)
        timeSlotList = ArrayList()
        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView?.setLayoutManager(manager)
        times = ArrayList()
        times = Common.convertTimeSlotToString()
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_MONTH, 9)
        cal[Calendar.HOUR_OF_DAY] = 23
        cal[Calendar.MINUTE] = 59
        cal[Calendar.SECOND] = 59
        cal[Calendar.MILLISECOND] = 999
        val maxDate = cal.timeInMillis
        val simpleCalendarView = findViewById<View>(R.id.calendari) as CalendarView
        simpleCalendarView.maxDate = maxDate
        simpleCalendarView.minDate = System.currentTimeMillis()
        simpleCalendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            selectedDate = dayOfMonth.toString() + "." + (month + 1).toString() + "." + year.toString()
            //Toast.makeText(TimeSelection.this, "gün: " + selectedDate, Toast.LENGTH_LONG).show();
            readHairdresser(selectedDate!!)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_calendar)
        LocalBroadcastManager.getInstance(this).registerReceiver(getTimes, IntentFilter("BUSY_TIMES"))
        init()
        //definedCalendar();
        add!!.setOnClickListener {
            val givenServices = HashMap<String, String?>()
            givenServices["date"] = selectedDate
            givenServices["time"] = selectedTime
            givenServices["slot"] = slot
            givenServices["acceptanceCase"] = "yes"
            if (selectedDate == null) {
                Toast.makeText(this@setCalendar, "Lütfen Gün Seçiniz.", Toast.LENGTH_LONG).show()
            }
            if (selectedTime == null) {
                Toast.makeText(this@setCalendar, "Lütfen Saat Seçiniz", Toast.LENGTH_LONG).show()
            }
            if (selectedDate != null && selectedTime != null) {
                val firebaseFirestore = FirebaseFirestore.getInstance()
                firebaseFirestore.collection("Hairdressers")
                        .document(Objects.requireNonNull(FirebaseAuth.getInstance().uid!!))
                        .collection("FullTimes").document().set(givenServices)
                Toast.makeText(this@setCalendar, "Müsait Olunmayan Zaman Başarı İle Eklendi.", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun readHairdresser(selectedDate: String) {
        val firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseFirestore.collection("Hairdressers").document(FirebaseAuth.getInstance().uid!!)
                .collection("FullTimes").get().addOnCompleteListener { task ->
                    timeSlotModels!!.clear()
                    for (document in Objects.requireNonNull(task.result)!!) {
                        val model = document.toObject<TimeSlotModel>(TimeSlotModel::class.java)
                        if (model!!.date == selectedDate) {
                            if (!timeSlotModels!!.contains(model)) timeSlotModels!!.add(model)
                        }
                    }
                    clockSelection!!.visibility = View.VISIBLE
                    timeSlotAdapter = TimeSlotAdapter(this@setCalendar,
                            times!!, FirebaseAuth.getInstance().uid!!, selectedDate, timeSlotModels!!)
                    recyclerView!!.adapter = timeSlotAdapter
                }
    }

    var getTimes: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val get = intent.getStringExtra("time")
            selectedTime = get.toString()
            slot = intent.getStringExtra("slot")
        }
    }
}