package com.example.hairdresserbookingapplication.ReservationSteps

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.ReservationSteps.LastStep
import com.example.hairdresserbookingapplication.model.CostumerModel
import com.example.hairdresserbookingapplication.users.UserPage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class LastStep : AppCompatActivity() {
    var day: TextView? = null
    var time: TextView? = null
    var hairdresser: TextView? = null
    var service: TextView? = null
    var gender: TextView? = null
    private var hairdresserId: String? = null
    private var companyId: String? = null
    private var service_list: ArrayList<String>? = null
    private var selected_services: String? = null
    private fun init() {
        gender = findViewById(R.id.gender_selection)
        hairdresser = findViewById(R.id.hairdresser_selection)
        service = findViewById(R.id.service_selection)
        day = findViewById(R.id.day_selection)
        time = findViewById(R.id.time_selection)
        service_list = ArrayList()
        getExtras(intent)
        findUser()
    }

    private fun recordSelection() {
        val givenServices = HashMap<String, String?>()
        givenServices["date"] = day!!.text.toString()
        givenServices["time"] = time!!.text.toString()
        givenServices["slot"] = intent.getStringExtra("slot")
        givenServices["acceptanceCase"] = "waiting"
        val firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseFirestore.collection("Hairdressers")
                .document(Objects.requireNonNull(hairdresserId)!!)
                .collection("FullTimes").document().set(givenServices)
        //Toast.makeText(TimeSelection.this, "Başarı İle Eklendi.", Toast.LENGTH_LONG).show();
    }

    private fun recordDatabase(email: String?, name: String?, phone: String?) {
        val mFirestore = FirebaseFirestore.getInstance()
        val user = HashMap<String, String?>()
        user["gender"] = gender!!.text.toString()
        user["hairdresser_id"] = hairdresserId
        user["companyId"] = companyId
        user["email"] = email
        user["name"] = name
        user["phone"] = phone
        user["user_id"] = FirebaseAuth.getInstance().uid
        user["hairdresser"] = hairdresser!!.text.toString()
        user["day"] = day!!.text.toString()
        user["time"] = time!!.text.toString()
        user["selected_services"] = selected_services
        user["acceptanceCase"] = "waiting"
        recordSelection()
        val ref = mFirestore.collection("ReservationRequests").document()
        user["id"] = ref.id
        ref.set(user).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val services = HashMap<String, String>()
                for (list in service_list!!) {
                    services[list] = list
                }
                ref.collection("selected_services").document().set(services)
                Toast.makeText(this@LastStep, "Rezervasyon talebi başarıyla gönderildi.", Toast.LENGTH_LONG).show()
                time!!.setOnClickListener {
                    val intent = Intent(this@LastStep, UserPage::class.java)
                    startActivity(intent)
                }
            } else {
                Toast.makeText(this@LastStep, "Reservasyon talebi yaparken problem oluştu.", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun findUser() {
        val docRef = FirebaseFirestore.getInstance()
                .collection("Costumers")
                .document(Objects.requireNonNull(FirebaseAuth.getInstance().uid)!!)
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                if (document != null && document.exists()) {
                    val costumerModel = document.toObject<CostumerModel>(CostumerModel::class.java)
                    recordDatabase(costumerModel!!.email, costumerModel.name, costumerModel.phone)
                }
            } else {
            }
        }
    }

    private fun getExtras(intent: Intent) {
        if (intent.getStringExtra("gender") != null) {
            gender!!.text = intent.getStringExtra("gender")
        }
        if (getIntent().getStringArrayListExtra("service_list") != null) {
            service!!.text = intent.getStringArrayListExtra("service_list").toString()
            selected_services = intent.getStringArrayListExtra("service_list").toString()
            service_list = intent.getStringArrayListExtra("service_list")
        }
        if (getIntent().getStringExtra("hairdresser") != null) {
            hairdresser!!.text = intent.getStringExtra("hairdresser")
        }
        if (getIntent().getStringExtra("day") != null) {
            day!!.text = intent.getStringExtra("day")
        }
        if (getIntent().getStringExtra("time") != null) {
            time!!.text = intent.getStringExtra("time")
        }
        if (getIntent().getStringExtra("hairdresser_id") != null) {
            hairdresserId = intent.getStringExtra("hairdresser_id")
        }
        if (getIntent().getStringExtra("companyId") != null) {
            companyId = intent.getStringExtra("companyId")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_last_step)
        init()
    }
}