package com.example.hairdresserbookingapplication

import java.util.*

object Common {
    fun convertTimeSlotToString(): List<String> {
        val arrayList = ArrayList<String>()
        arrayList.add("9:00 - 9:40")
        arrayList.add("9:50 - 10:30")
        arrayList.add("10:40 - 11:20")
        arrayList.add("11:30 - 12:10")
        arrayList.add("12:20 - 13:00")
        arrayList.add("13:10 - 13:50")
        arrayList.add("14:00 - 14:40")
        arrayList.add("14:50 - 15:30")
        arrayList.add("15:40 - 16:20")
        arrayList.add("16:30 - 17:10")
        arrayList.add("17:20 - 18:00")
        arrayList.add("18:10 - 18:50")
        arrayList.add("19:00 - 19:40")
        arrayList.add("19:50 - 19:30")
        arrayList.add("14:50 - 15:30")
        arrayList.add("15:40 - 16:20")
        arrayList.add("16:30 - 17:10")
        arrayList.add("17:20 - 18:00")
        arrayList.add("18:10 - 18:50")
        arrayList.add("19:00 - 19:40")
        arrayList.add("19:50 - 19:30")
        return arrayList
    }
}