package com.example.hairdresserbookingapplication.fragment

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

open class LoginFragment_Common : Fragment() {
    @get:JvmName("getView_") protected var view:  View? = null
    @get:JvmName("getContext_") protected var context: Context? = null
    protected var register: TextView? = null
    protected var email: EditText? = null
    protected var password: EditText? = null
    protected var forgotPassword: TextView? = null
    protected var loginButton: Button? = null
    protected var auth: FirebaseAuth? = null
    protected var userId: String? = null
    protected var reference: DatabaseReference? = null
    protected var mFirestore: FirebaseFirestore? = null
    protected var firstRegister: Button? = null
    protected var document: DocumentReference? = null
    protected var collectionReference: CollectionReference? = null
    protected var mFirebaseUser: FirebaseUser? = null
    protected fun resetPassword(view: View) {
        val resetMail = EditText(view.context)
        val passwordReset = AlertDialog.Builder(view.context)
        passwordReset.setTitle("Şifrenizi sıfırlamak ister misiniz?")
        passwordReset.setMessage("Şifrenizi sıfırlama linki için e-postanızı giriniz.")
        passwordReset.setView(resetMail)
        passwordReset.setPositiveButton("Evet") { dialogInterface, i ->

            //maili çıkar ve sıfırlama bağlantısı gönder.
            val mail = resetMail.text.toString()
            auth!!.sendPasswordResetEmail(mail).addOnSuccessListener { Toast.makeText(context, "Sıfırlama linki e-posta adresinize gönderildi.", Toast.LENGTH_SHORT).show() }.addOnFailureListener { e -> Toast.makeText(context, "Hata oluştu.Sıfırlama linki gönderilemedi." + e.message, Toast.LENGTH_SHORT).show() }
        }
        passwordReset.setNegativeButton("Hayır") { dialogInterface, i ->
            //basarısız.
        }
        passwordReset.create().show()
    }
}