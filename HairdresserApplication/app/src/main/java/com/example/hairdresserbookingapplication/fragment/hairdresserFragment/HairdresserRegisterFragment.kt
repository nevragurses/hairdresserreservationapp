package com.example.hairdresserbookingapplication.fragment.hairdresserFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.fragment.RegisterFragment_Common
import com.example.hairdresserbookingapplication.login.Login
import com.example.hairdresserbookingapplication.model.AdminModel
import com.example.hairdresserbookingapplication.model.HairdresserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class HairdresserRegisterFragment : RegisterFragment_Common() {
    private var companyName: String? = null
    private var companyAddr: String? = null
    private var province: String? = null
    private var street: String? = null
    private var neighborhood: String? = null
    private var district: String? = null
    private var adminId: String? = null
    var radioGroup: RadioGroup? = null
    var radioButton: RadioButton? = null
    var spinnerComName: Spinner? = null
    var spinnerProvince: Spinner? = null
    var spinnerNeigh: Spinner? = null
    var spinnerStreet: Spinner? = null
    var spinnerDistrict: Spinner? = null
    var spinnerAddr: Spinner? = null
    var streetAdapter: ArrayAdapter<String>? = null
    var provinceAdapter: ArrayAdapter<String>? = null
    var neighAdapter: ArrayAdapter<String>? = null
    var districtAdapter: ArrayAdapter<String>? = null
    var addressAdapter: ArrayAdapter<String>? = null
    var nameAdapter: ArrayAdapter<String>? = null
    var provinceList: ArrayList<String>? = null
    var streetList: ArrayList<String>? = null
    var districtList: ArrayList<String>? = null
    var neighList: ArrayList<String>? = null
    var addressList: ArrayList<String>? = null
    var nameList: ArrayList<String>? = null
    private fun init(view: View) {
        auth = FirebaseAuth.getInstance()
        val mFirebaseUser = auth?.currentUser
        if (mFirebaseUser != null) {
            userId = mFirebaseUser.uid
        }
        mFirestore = FirebaseFirestore.getInstance()
        alreadyRegister = view.findViewById(R.id.alreadyRegister)
        name = view.findViewById(R.id.nameProf)
        phone = view.findViewById(R.id.phoneProf)
        email = view.findViewById(R.id.emailProf)
        password = view.findViewById(R.id.passwordProf)
        register = view.findViewById(R.id.register_button)
        login = view.findViewById(R.id.login)
        spinnerAddr = view.findViewById(R.id.addr)
        spinnerComName = view.findViewById(R.id.compName)
        spinnerProvince = view.findViewById(R.id.spinnerProvince)
        spinnerStreet = view.findViewById(R.id.spinnerStreet)
        spinnerNeigh = view.findViewById(R.id.spinnerNeigh)
        spinnerDistrict = view.findViewById(R.id.spinnerDistrict)
        provinceList = ArrayList()
        streetList = ArrayList()
        neighList = ArrayList()
        addressList = ArrayList()
        nameList = ArrayList()
        districtList = ArrayList()
        addressAdapter = thisContext?.let {
            ArrayAdapter(it,
                android.R.layout.simple_spinner_dropdown_item, addressList!!)
        }
        nameAdapter = thisContext?.let {
            ArrayAdapter(it,
                android.R.layout.simple_spinner_dropdown_item, nameList!!)
        }
        streetAdapter = thisContext?.let {
            ArrayAdapter(it
                , android.R.layout.simple_spinner_dropdown_item, streetList!!)
        }
        provinceAdapter = thisContext?.let {
            ArrayAdapter(it,
                android.R.layout.simple_spinner_dropdown_item, provinceList!!)
        }
        districtAdapter = thisContext?.let {
            ArrayAdapter(it,
                android.R.layout.simple_spinner_dropdown_item, districtList!!)
        }
        neighAdapter = thisContext?.let {
            ArrayAdapter(it,
                android.R.layout.simple_spinner_dropdown_item, neighList!!)
        }
        spinnerComName!!.setAdapter(nameAdapter)
        spinnerAddr!!.setAdapter(addressAdapter)
        spinnerProvince!!.setAdapter(provinceAdapter)
        spinnerStreet!!.setAdapter(streetAdapter)
        spinnerNeigh!!.setAdapter(neighAdapter)
        spinnerDistrict!!.setAdapter(districtAdapter)
        results
        register?.setOnClickListener {
            val selectedId = radioGroup!!.checkedRadioButtonId
            radioButton = activity!!.findViewById(selectedId)
            createNewAccount()
        }
        alreadyRegister?.setOnClickListener {
            val intent = Intent(thisContext, Login::class.java)
            intent.putExtra("FRAGMENT_ID", "1")
            startActivity(intent)
        }
        login?.setOnClickListener {
            val intent = Intent(thisContext, Login::class.java)
            intent.putExtra("FRAGMENT_ID", "1")
            startActivity(intent)
        }
    }

    private val results: Unit
        private get() {
            spinnerAddr!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    companyAddr = parent.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            spinnerComName!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    companyName = parent.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            spinnerNeigh!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    neighborhood = parent.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            spinnerProvince!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    province = parent.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            spinnerDistrict!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    district = parent.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            spinnerStreet!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    street = parent.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
        }

    private fun searchAdminId(userName: String, e_mail: String, phoneNum: String) {
        mFirestore!!.collection("Admins").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<HairdresserModel>(HairdresserModel::class.java)
                if (model!!.street == street && model!!.neighborhood == neighborhood && model!!.district == district && model!!.province == province && model!!.companyName == companyName && model!!.companyAddress == companyAddr) {
                    adminId = document.getString("id")
                    val user = HashMap<String, String?>()
                    user["id"] = auth!!.uid
                    user["name"] = userName
                    user["email"] = e_mail
                    user["phone"] = phoneNum
                    user["companyName"] = companyName
                    user["companyAddress"] = companyAddr
                    user["province"] = province
                    user["district"] = district
                    user["street"] = street
                    user["neighborhood"] = neighborhood
                    user["acceptanceCase"] = "false"
                    user["adminId"] = adminId
                    user["type"] = radioButton!!.text.toString()
                    userId?.let {
                        mFirestore!!.collection("Hairdressers").document(it).set(user).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                val loginIntent = Intent(thisContext, Login::class.java)
                                loginIntent.putExtra("FRAGMENT_ID", "1")
                                //Toast.makeText(thisContext,"Kayıt başarı ile yapıldı.",Toast.LENGTH_LONG).show();
                                startActivity(loginIntent)
                            } else {
                                Toast.makeText(thisContext, "Kayıt yapılırken problem oluştu.", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun searchAddr() {
        mFirestore!!.collection("Admins").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<AdminModel>(AdminModel::class.java)
                model!!.street?.let { streetList!!.add(it) }
                streetAdapter!!.notifyDataSetChanged()
                model!!.province?.let { provinceList!!.add(it) }
                provinceAdapter!!.notifyDataSetChanged()
                model!!.neighborhood?.let { neighList!!.add(it) }
                neighAdapter!!.notifyDataSetChanged()
                model!!.district?.let { districtList!!.add(it) }
                districtAdapter!!.notifyDataSetChanged()
                model!!.companyName?.let { nameList!!.add(it) }
                nameAdapter!!.notifyDataSetChanged()
                model!!.companyAddress?.let { addressList!!.add(it) }
                addressAdapter!!.notifyDataSetChanged()
            }
        }
    }

    private fun createNewAccount() {
        if (!validateName() || !validateMail() || !validatePass() || !validatePhone()) {
            return
        }
        val userName = name?.text.toString()
        val e_mail = email?.text.toString()
        val pass = password?.text.toString()
        val phoneNum = phone?.text.toString()
        auth?.createUserWithEmailAndPassword(e_mail, pass)?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                // sendVerificationLink(); ONEMLİİİİİİİİİİİİ
                userId = auth?.currentUser?.uid
                searchAdminId(userName, e_mail, phoneNum)
            } else {
                val builder = thisContext?.let { AlertDialog.Builder(it) }
                builder!!.setMessage("Bu e-posta adresi ile kayıtlı kullanıcı zaten var!")
                builder!!.setPositiveButton("Anladım", null)
                builder.show()
                Toast.makeText(thisContext, "Bu e-posta adresi ile kayıtlı kullanıcı zaten var!", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        thisContext = container!!.context
        val view = inflater.inflate(R.layout.fragment_hairdresser_register, container, false)
        radioGroup = view.findViewById(R.id.radioGroup)
        init(view)
        searchAddr()
        return view
    }
}