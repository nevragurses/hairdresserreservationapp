package com.example.hairdresserbookingapplication.model

class CostumerModel {
    var id: String? = null
    var name: String? = null
    var email: String? = null
    var phone: String? = null
    var password: String? = null
    var gender: String? = null

    constructor() {}
    constructor(id: String?, name: String?, email: String?,
                phone: String?, password: String?, gender: String?) {
        this.id = id
        this.name = name
        this.email = email
        this.phone = phone
        this.password = password
        this.gender = gender
    }

}