package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.admin.AddingServices
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class AddingServices : AppCompatActivity() {
    private var service: EditText? = null
    private var price: EditText? = null
    private var userId: String? = null
    private var addButton: Button? = null
    private var firebaseFirestore: FirebaseFirestore? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Hizmet Ekleme"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        service = findViewById(R.id.service)
        firebaseFirestore = FirebaseFirestore.getInstance()
        userId = FirebaseAuth.getInstance().uid
        addButton = findViewById(R.id.button)
        price = findViewById(R.id.price)
        addButton?.setOnClickListener(View.OnClickListener {
            val selected = service?.getText().toString()
            val selectedPrice = price?.getText().toString()
            val givenServices = HashMap<String, String>()
            givenServices["name"] = selected
            givenServices["price"] = selectedPrice
            firebaseFirestore!!.collection("Admins").document(userId!!)
                    .collection("Services").document(selected).set(givenServices)
            Toast.makeText(this@AddingServices, "Hizmet Başarı İle Eklendi.", Toast.LENGTH_LONG).show()
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adding_services)
        init()
    }
}