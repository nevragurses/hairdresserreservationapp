package com.example.hairdresserbookingapplication.fragment.hairdresserFragment

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.fragment.LoginFragment_Common
import com.example.hairdresserbookingapplication.hairdresser.HairdresserPage
import com.example.hairdresserbookingapplication.register.Register
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class HairdresserLoginFragment : LoginFragment_Common() {
    private fun init(view: View) {
        auth = FirebaseAuth.getInstance()
        mFirestore = FirebaseFirestore.getInstance()
        mFirebaseUser = auth!!.currentUser
        if (mFirebaseUser != null) {
            userId = mFirebaseUser!!.uid
        }
        register = view.findViewById(R.id.noRegister)
        email = view.findViewById(R.id.email)
        password = view.findViewById(R.id.editTextPassword)
        forgotPassword = view.findViewById(R.id.forgotPassword)
        loginButton = view.findViewById(R.id.button)
        firstRegister = view.findViewById(R.id.register)
        register?.setOnClickListener {
            val intent = Intent(context, Register::class.java)
            intent.putExtra("FRAGMENT_REGISTER", "1")
            startActivity(intent)
        }
        firstRegister?.setOnClickListener {
            val intent = Intent(context, Register::class.java)
            intent.putExtra("FRAGMENT_REGISTER", "1")
            startActivity(intent)
        }
        loginButton?.setOnClickListener {
            collectionReference = mFirestore?.collection("Hairdressers")
            loginHairdresser()
        }
        forgotPassword?.setOnClickListener { view -> resetPassword(view) }
    }

    private fun controlConfirmation() {
        val docRef = userId?.let { mFirestore?.collection("Hairdressers")!!.document(it) }
        docRef!!.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                if (document != null) {
                    if (document.getString("acceptanceCase") == "true") {
                        val mainIntent = Intent(context, HairdresserPage::class.java)
                        startActivity(mainIntent)
                    } else if (document.getString("acceptanceCase") == "rejected") {
                        val builder = context?.let { AlertDialog.Builder(it) }
                        builder!!.setMessage("Kayıt talebiniz seçtiğiniz işletme tarafından reddedildi.")
                        builder.setPositiveButton("Anladım", null)
                        builder.show()
                    } else if (document.getString("acceptanceCase") == "false") {
                        val builder = context?.let { AlertDialog.Builder(it) }
                        builder!!.setMessage("İşletmeye kayıt talebiniz değerlendirme sürecindedir.Beklemede kalın.")
                        builder.setPositiveButton("Tamam", null)
                        builder.show()
                    }
                }
            }
        }
    }

    private fun checkUserAccessLevel() {
        userId?.let {
            collectionReference!!.document(it).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                if (Objects.requireNonNull(task.result)!!.getString("email") != null) {
                    controlConfirmation()
                } else {
                    Toast.makeText(context, "Giriş Başarısız-2!", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(context, "Giriş Başarısız!", Toast.LENGTH_LONG).show()
            }
        }
        }
    }

    private fun loginHairdresser() {
        val e_mail = email!!.text.toString()
        val pass = password!!.text.toString()
        if (TextUtils.isEmpty(e_mail)) {
            Toast.makeText(context, "E-mail alanı boş olamaz", Toast.LENGTH_LONG).show()
        } else if (TextUtils.isEmpty(pass)) {
            Toast.makeText(context, "Şifre alanı boş olamaz", Toast.LENGTH_LONG).show()
        } else {
            auth!!.signInWithEmailAndPassword(e_mail, pass).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    userId = auth?.currentUser?.uid
                    val userVerification = auth!!.currentUser
                    //if(userVerification.isEmailVerified()) {
                    checkUserAccessLevel()
                    //}
                    /*else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Lütfen e-postanıza gönderilen doğrulama kodunu onaylayınız!");
                                builder.setPositiveButton("Anladım", null);
                                builder.show();
                            }*/
                } else {
                    Toast.makeText(context, "Giriş Başarısız!" + task.exception!!.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        context = container!!.context
        val view = inflater.inflate(R.layout.fragment_hairdresser_login, container, false)
        init(view)
        return view
    }
}