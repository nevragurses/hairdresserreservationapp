package com.example.hairdresserbookingapplication.adapter.adminAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.model.UploadModel
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso

class ImageAdapter(private val mContext: Context, private val mUploads: List<UploadModel>) : RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.image_item, parent, false)
        return ImageViewHolder(v)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val uploadCurrent = mUploads[position]
        holder.textViewName.text = uploadCurrent.name
        Picasso.with(mContext)
                .load(uploadCurrent.imageUrl)
                .placeholder(R.drawable.scissoricon)
                .fit()
                .centerCrop()
                .into(holder.imageView)
    }

    override fun getItemCount(): Int {
        return mUploads.size
    }

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewName: TextView
        var imageView: ImageView
        var trash: ImageView

        init {
            textViewName = itemView.findViewById(R.id.text_view_name)
            imageView = itemView.findViewById(R.id.image_view_upload)
            trash = itemView.findViewById(R.id.trash)
            trash.setOnClickListener {
                val mStorage = FirebaseStorage.getInstance()
                val mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads")
                val selectedItem = mUploads[adapterPosition]
                val selectedKey = selectedItem.key
                val imageRef = selectedItem.imageUrl?.let { it1 -> mStorage.getReferenceFromUrl(it1) }
                if (imageRef != null) {
                    imageRef.delete().addOnSuccessListener {
                        if (selectedKey != null) {
                            mDatabaseRef.child(selectedKey).removeValue()
                        }
                        Toast.makeText(mContext, "Resim Silindi", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

}