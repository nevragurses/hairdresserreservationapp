package com.example.hairdresserbookingapplication.admin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hairdresserbookingapplication.R
import com.example.hairdresserbookingapplication.adapter.adminAdapters.HairdresserConfirmation_Adapter
import com.example.hairdresserbookingapplication.model.HairdresserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class WaitingHairdresser_Requests : AppCompatActivity() {
    private var recycler: RecyclerView? = null
    private var mFirestore: FirebaseFirestore? = null
    private var hairdresserRequests: MutableList<HairdresserModel?>? = null
    private var hairdresser_requests: HairdresserConfirmation_Adapter? = null
    private var actionbar: Toolbar? = null
    private fun init() {
        actionbar = findViewById(R.id.actionbar)
        setSupportActionBar(actionbar) //action bar ı ekrana set et.
        supportActionBar!!.title = "Kuaför Talepleri"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //buton aktif hale geliyor.
        recycler = findViewById(R.id.recycler)
        recycler?.setHasFixedSize(true)
        mFirestore = FirebaseFirestore.getInstance()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler?.setLayoutManager(linearLayoutManager)
        hairdresserRequests = ArrayList()
        readRequests()
    }

    private fun readRequests() {
        mFirestore!!.collection("Hairdressers").get().addOnCompleteListener { task ->
            for (document in Objects.requireNonNull(task.result)!!) {
                val model = document.toObject<HairdresserModel>(HairdresserModel::class.java)
                if (model!!.adminId == FirebaseAuth.getInstance().uid && model!!.acceptanceCase == "false") {
                    hairdresserRequests!!.add(model)
                }
                hairdresser_requests = HairdresserConfirmation_Adapter(this@WaitingHairdresser_Requests, hairdresserRequests)
                recycler!!.adapter = hairdresser_requests
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waiting_hairdresser__requests)
        init()
    }
}