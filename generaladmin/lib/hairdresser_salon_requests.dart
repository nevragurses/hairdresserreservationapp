import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:generaladmin/model/AdminModel.dart';
import 'package:fluttertoast/fluttertoast.dart';
class HairdresserRequests extends StatefulWidget {
  @override
  _HairdresserRequestsState createState() => _HairdresserRequestsState();
}
class _HairdresserRequestsState extends State<HairdresserRequests> {
  List<Card> cardList = new List.filled(0, new Card(),growable: true);
  bool setStateRan=false;
  _confirm(String id){
    DocumentReference users = FirebaseFirestore.instance.collection('Admins').doc(id);
    users.get().then((documentSnapshot) {
      final Map<String, String> someMap = {
        "acceptanceCase": "true",
      };
      documentSnapshot.reference.update(someMap);
      setState(() {});
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Fluttertoast.showToast(
        msg: "Kayıt Onaylandı!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );
  }
  _reject(String id){
    DocumentReference users = FirebaseFirestore.instance.collection('Admins').doc(id);
    users.get().then((documentSnapshot) {
      final Map<String, String> someMap = {
        "acceptanceCase": "false",
      };
      documentSnapshot.reference.update(someMap);
      setState(() {});
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Fluttertoast.showToast(
        msg: "Kayıt Reddedildi!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 4
    );
  }

  Card newsCard(AdminModel news){
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
        //borderRadius: BorderRadius.circular(35.0),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),

        title: Text("İşletme İsim: " +news.companyName.toString()),
        subtitle: Text("İşletme Yöneticisi İsim: " + news.name.toString() +
            "\n" + "İşletme Telefon: " +  news.phone.toString() +
            "\n" + "İşletme Email: " +  news.email.toString() +
            "\n" + "İşletme İl: " +  news.province.toString()+
            "\n" + "İşletme İlçe: " +  news.district.toString()+
            "\n" + "İşletme Cadde: " +  news.street.toString()+
            "\n" + "İşletme Mahalle: " +  news.neighborhood.toString()+
            "\n" + "İşletme Adres: " +  news.address.toString()

        ),
        isThreeLine: true,
        trailing: Wrap(
          spacing: 12, // space between two icons
          children: <Widget>[
            new IconButton(
              icon: new Icon(Icons.assignment_turned_in),
              onPressed: (){
                _confirm(news.admin_id) ;
              },
              highlightColor: Colors.green,
            ),
            new IconButton(
              icon: new Icon(Icons.clear),
              onPressed: (){
                _reject(news.admin_id) ;
              },
              highlightColor: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
  Future<List<AdminModel>> getNewsList()async{
    final firestoreInstance = FirebaseFirestore.instance;
    List<AdminModel> _needs = [];
    await firestoreInstance.collection("Admins").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("acceptanceCase").toString()=="waiting" ) {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(AdminModel.fromMap(values));
        }
        //print(value.data());
      });
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });

    return await _needs;
  }

  createCardList()async{
    List<AdminModel> newsList =await getNewsList();
    for(int i=0;i< newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
    }
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
class HairdresserRequestPage extends StatelessWidget { //BUNU ÇAĞIR
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          /*decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/back.png"),fit: BoxFit.fill
              )
          ),*/
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child:HairdresserRequests()
            ),
          ),
        ),
      ),
    );
  }
}