import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:generaladmin/model/AdminModel.dart';
class RejectedHairdressers extends StatefulWidget {
  @override
  _RejectedRequestsState createState() => _RejectedRequestsState();
}
class _RejectedRequestsState extends State<RejectedHairdressers> {
  List<Card> cardList = new List.filled(0, new Card(),growable: true);
  bool setStateRan=false;

  Card newsCard(AdminModel news){
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color:  Color(0xFF6B8BC8), width: 0.5),
        //borderRadius: BorderRadius.circular(35.0),
      ),
      child: ListTile(
        leading:Image.asset(
          'assets/scissor.jpg',
          fit: BoxFit.cover,
        ),

        title: Text("İşletme İsmi: " +news.name.toString()),
        subtitle: Text("İşletme Yöneticisi İsim: " + news.name.toString() +
            "\n" + "İşletme Telefon: " +  news.phone.toString() +
            "\n" + "İşletme Email: " +  news.email.toString() +
            "\n" + "İşletme İl: " +  news.province.toString()+
            "\n" + "İşletme İlçe: " +  news.district.toString()+
            "\n" + "İşletme Cadde: " +  news.street.toString()+
            "\n" + "İşletme Mahalle: " +  news.neighborhood.toString()+
            "\n" + "İşletme Adres: " +  news.address.toString()

        ),
        isThreeLine: true,
        trailing: Wrap(
          spacing: 12, // space between two icons
        ),
      ),
    );
  }
  Future<List<AdminModel>> getNewsList()async{
    var user = await FirebaseAuth.instance.currentUser;
    final firestoreInstance = FirebaseFirestore.instance;
    List<AdminModel> _needs = [];
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
          title: const Text(''),
          content: Container(
              child:Column(
                  children:[
                    CircularProgressIndicator(
                      semanticsLabel: 'Linear progress indicator',
                    )
                  ]
              ),
              height:75,
              width:75
          )
      ),
    );
    await firestoreInstance.collection("Admins").get().then((querySnapshot) {
      _needs.clear();
      querySnapshot.docs.forEach((value) {
        if (value.get("acceptanceCase").toString()=="false" ) {
          Map<dynamic, dynamic> values = value.data();
          _needs.add(AdminModel.fromMap(values));
        }
        //print(value.data());
      });
      // Navigator.pop(context);
    }).catchError((onError) {
      print("getCloudFirestoreUsers: ERROR");
      print(onError);
    });
    Navigator.pop(context);
    return await _needs;
  }

  createCardList()async{
    List<AdminModel> newsList =await getNewsList();
    for(int i=0;i< newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
    }

    setState(() {});

  }
  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }
}
class RejectedHairdressersPage extends StatelessWidget { //BUNU ÇAĞIR
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(vertical:10),
          child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child:RejectedHairdressers()
            ),
          ),
        ),
      ),
    );
  }
}